<?php
	$lesson = array(
		'lesson'=>'p3-t06'
	);
	if(!isset($_GET['lesson'])){
		//set lesson parametor on URL if parametor is not set
		header('Location:'.$_SERVER['REQUEST_URI'].'?'.http_build_query($lesson));
	}
	else if($lesson['lesson']!=$_GET['lesson']){ 
		//remove query parameter from URL and add correct parameter if URL contains wrong lesson value 
		$url=strtok($_SERVER["REQUEST_URI"],'?');
		header('Location:'.$url.'?'.http_build_query($lesson));
	}	
?>


<?php
Header('Vary: User-Agent, Accept');
$home_url = 'https://'.$_SERVER['SERVER_NAME'];
$home_url = 'http://192.168.70.123:9000';
?>
<!doctype html>
<html lang='en'>
<head>
	<meta charset='utf-8' />
	<!-- <script src="../js/confirmDialog.jquery.js"></script> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
    <script src="../libraries/codemirror/lib/codemirror.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:400,600,700&display=swap" rel="stylesheet">
    <link rel='apple-touch-icon' sizes='76x76' href='assets/img/apple-icon.png'>
    <link rel='icon' type='image/png' href='assets/img/favicon.png'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
    <title>Learn to code HTML, CSS, and JavaScript with CodeJIKA</title>
  <meta name="description" content="CodeJIKA is a fun and free online course that teaches you how to make websites through simple projects you can do right in your browser. Get started today!">	
    <meta name="keywords" content="code, jika, school, free, coding, learn online">
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	
	<script src="../js/fontawesome.min.js" type="text/javascript"></script>
	<!-- fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
      <!-- <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js" id="js-firebase" async></script>		 -->
        <script type="text/javascript">
    //<![CDATA[
    function randText() {
			var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for (var i = 0; i < 5; i++)
					text += possible.charAt(Math.floor(Math.random() * possible.length));
			return text;
		}
		
    // document.write("<script type='text/javascript' src='../js/lesson.js?" + randText() + "'><\/script>");
    //]]>
  </script>	
    <!--     Fonts and icons     -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700,200' rel='stylesheet' />
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' />
    <!-- CSS Files -->
    <link href='../assets/css/bootstrap.min.css' rel='stylesheet' />
    <link href='../css/now-ui-kit.css?v=1.1.0' rel='stylesheet' />
	<!-- <link rel='stylesheet' href='../doc/docs.css'> -->
	<link rel='stylesheet' href='../css/codemirror.css'>
	<link rel="stylesheet" href="../addon/hint/show-hint.css">
	<link rel="stylesheet" href="../addon/lint/lint.css">
	<link rel='stylesheet' href='../css/theme/base16-dark.css'>
    <link href='../css/custom-lesson.css' rel='stylesheet' />
	<style>
		.pagination {
			background: black;
		}
	</style>

</head>

<body class='template-page sidebar-collapse h-100 lesson'>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg nav-header fixed-top " color-on-scroll="400">
	<div class="container-fluid" >
		<div class="navbar-translate">
			<a class="navbar-brand" href="https://localhost" rel="tooltip" title="" data-placement="bottom" data-original-title="Return to Code JIKA homepage">
				<img src="../img/logo-beta-1.png">
				</a>
				<button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="true" aria-label="Toggle navigation">
					<span class="navbar-toggler-bar bar1"></span>
					<span class="navbar-toggler-bar bar2"></span>
					<span class="navbar-toggler-bar bar3"></span>
				</button>
			</div>
			<div class="navbar-collapse justify-content-end collapse show" id="navigation" style="" data-nav-image="../assets/img/blurred-image-1.jpg">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" onclick="return homePage()">
							Home
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" onclick="galleryPage()">
						Gallery
						</a>
					</li>
					<li class="nav-item" >
						<a class="nav-link" onclick="menuPage()">
							Menu
						</a>
					</li>
					<li class="nav-item"></li>
				</ul>
				<span style="color: #fff;">Tell your friends:&nbsp;&nbsp;</span>
				<ul class="navbar-nav">
					<li class="nav-item" style="display: none;">
						<a class="nav-link" href="#">
							<p>Lesson</p>
						</a>
					</li>
					<li class="nav-item" style="display: none;">
						<a class="nav-link" href="#">
							<p>Q&amp;A Forum</p>
						</a>
					</li>
					<li class="nav-item" style="display: none;">
						<a class="nav-link" href="#">
							<p>Skills</p>
						</a>
					</li>
					<li class="nav-item">
						<div class="sharethis-inline-share-buttons st-right  st-inline-share-buttons st-animated" id="st-1">
							<div class="st-btn st-first    st-remove-label" data-network="facebook" style="display: inline-block;">
								<img src="https://platform-cdn.sharethis.com/img/facebook.svg">
								</div>
								<div class="st-btn    st-remove-label" data-network="twitter" style="display: none;">
									<img src="https://platform-cdn.sharethis.com/img/twitter.svg">
									</div>
									<div class="st-btn st-last    st-remove-label" data-network="sharethis" style="display: none;">
										<img src="https://platform-cdn.sharethis.com/img/sharethis.svg">
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</nav>
    <!-- End Navbar -->
    <div class='wrapper h-100' id="main_page">

		<div class='container-fluid' style='height: 100%; min-height: 600px;'>
		  <div class='row h-100'>
				<div class='col-md-4 col-sm-6 h-100 no-gutter left-col ' style='border-right: 1px solid #333; '>

						<div class='col-sm-12 no-gutter lesson-skills' >

						
						<div class="card">
							<ul id="submenu" class="nav nav-tabs nav-tabs-neutral justify-content-center" role="tablist" >
								<li role="submenu" class="nav-item">
									<a class="nav-link active " data-toggle="tab" href="#lesson_tab" role="tab_menu">Lesson</a>
								</li>
								<li role="submenu" class="nav-item">
									<a class="nav-link " data-toggle="tab" href="#forum_tab" role="tab_menu" >Q&A Forum</a>
								</li>
								<li role="submenu" class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#skills_tab" role="tab_menu">Skills</a>
								</li>
								<li role="submenu" class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#resources_tab" role="tab_menu">Hints</a>
								</li>
								<li role="submenu" class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#credits_tab" role="tab_menu">Credits</a>
								</li>
							</ul>
							
							<div class="card-body">
								<!-- Tab panes -->
								<div class="tab-content">
									<div class="tab-pane active " id="lesson_tab" role="tabpanel">
									
									
									
								
            




<?php
    $slideNumber = 1;//starting slide number for lesson1 of project1
    
    $totalSlides = count(glob('../img/201909-project-1/slides1/*.PNG'));

    echo"<!-- Nav tabs -->";
    $navTabCounter = $slideNumber;
    if( $totalSlides >= 1){
        if( $navTabCounter == 1 ){
            echo "<ul id='tab-slides' class='nav nav-tabs justify-content-center' role='tablist' >
                <li role='slides' class='nav-item'><a class='nav-link active' data-toggle='tab' href='#slide".$navTabCounter."' data-step='".$navTabCounter."' role='tab-slides'></a></li>
            ";
            $navTabCounter++;
        }
        while( $navTabCounter <= $totalSlides){
            echo "<li role='slides' class='nav-item'><a class='nav-link' data-toggle='tab' href='#slide".$navTabCounter."' data-step='".$navTabCounter."' role='tab-slides'></a></li>";
            $navTabCounter++;
        }
        echo "</ul>";
    }

    echo"<!-- Nav tabs -->";
    echo '<div class="tab-content text-center">';
    while( $slideNumber <= $totalSlides ){
        if( is_file('../img/201909-project-1/slides1/Slide'.$slideNumber.'.PNG') ){
            //first slide
            if($slideNumber == 1){
                echo "<div class='tab-pane tab-pane-slide active' id='slide".$slideNumber."' role='tabpanel'>
                    <img src='../img/201909-project-1/slides1/Slide".$slideNumber.".PNG'>
                    <a class='btn btn-primary next' style='top:65%;'>Start Slideshow →</a>
                </div>";
            }
            

            else if($slideNumber == 16){
                echo "
                <div class='tab-pane tab-pane-slide' id='slide".$slideNumber."' role='tabpanel'>
                         <img class='lazy' src='../img/201909-project-1/slides1/Slide".$slideNumber.".PNG'>
                        
                        <div class='checkpoint'>
                            <h2>
                                Challenge <br>
                                <span class='challange-step'> Step 1</span>
                            </h2>

                            <ol class='actions'>
                                <li data-index='0'>
                                    <span class='check-icon'>/</span>
                                    Write this in the editor below:<br>
                                    <span class='html-code'>
                                        &lt;head&gt;<br>&lt;/head&gt
                                    </span>
                                </li>
                            </ol>
                            <a class='btn btn-primary next check' style='toXp:65%;'>Skip this →</a>
                        </div>

                    </div>
                    
                    ";
            }
            
            
            else if($slideNumber == 43){
                echo "<div class='tab-pane tab-pane-slide' id='slide".$slideNumber."' role='tabpanel'>
                    <img src='../img/201909-project-1/slides1/Slide".$slideNumber.".PNG'>
                    <a href='/GitHub/codejika/learn/coding1-t2.php' class='btn btn-primary' style='top:65%;'>Start next training →</a>
                </div>";
            }
            else{
                    echo "<div class='tab-pane tab-pane-slide' id='slide".$slideNumber."' role='tabpanel'>
                            <img class='lazy' src='../img/201909-project-1/slides1/Slide".$slideNumber.".PNG'>
                        </div>";
            }
            $slideNumber++;
        }
        else{
            $slideNumber++;
        }
    }
?>

</div>




<div class="row pagination">
								<div class="col-sm-12 no-gutter">
									<div class="progress"><div id="lessonProgressBar" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="29" data-valuemax="29" style="">
									</div></div>	
								</div>	
								<div class="col-sm-12 nXo-gutter">
								<div class="row remove-all-margin">
								<div class="col-sm-4 text-left no-gutter">
									<a class="prev disabled" href="#" aria-label="Previous Slide">
										< Previous</span>
									</a>
								</div>	
								<div class="col-sm-4 text-center no-gutter">
									<div class="lessonProgressText"></div>
								</div>	
								<div class="col-sm-4 text-right no-gutter">									
									<a class="next" href="#" aria-label="Next Slide">Start Slideshow
									</a>
								</div>	
								</div>	
								</div>	
							</div>	
								
																	
									</div>
									<div class="tab-pane text-center" id="forum_tab" role="tabpanel">
										<h2 style="user-select: text;">Stuck?</h2>
										<p style="user-select: text;">Ask the community for help in our Facebook group!</p>
										<a href="https://www.facebook.com/groups/626270557761252/" target="_blank" class="button">Launch the Facebook Group</a>
									</div>
									<div class="tab-pane" id="skills_tab" role="tabpanel">
									<div class='skills'>
										<span id="h1-h6-skill" class='skill has-skill-false skill-type-html data-name='h1-h6' rel='tooltip' title='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">Locked Skill</h2>
										<p class="skill-description">Your mind is not yet prepared to comprehend this skill</p>
										<p class="skill-description">Keep learning to unlock it :)</p>'  data-title-text='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">h1-h6</h2>
										<p class="skill-description">Document headings and sub-headings. The bigger the number, the smaller the size.</p>
										<h3>Example</h3>
										</div>
										</div></div>
										' data-placement='right' target='_blank'>
										h1-h6
											<div class="skills-example">
											<div class="CodeTooltip">
											  <div class="code"><code><span class="tag">&lt;h1&gt;</span>I’m a main heading<span class="tag">&lt;/h1&gt;</span><br><span class="tag">&lt;h2&gt;</span>I’m a subheading<span class="tag">&lt;/h2&gt;</span><br><span class="tag">&lt;h6&gt;</span>I’m the smallest<span class="tag">&lt;/h6&gt;</span></code></div>
											</div></div>
										</span>
										<span id="email-input-skill" class='skill has-skill-false skill-type-html  data-name='email-input' rel='tooltip' title='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">Locked Skill</h2>
										<p class="skill-description">Your mind is not yet prepared to comprehend this skill</p>
										<p class="skill-description">Keep learning to unlock it :)</p>'  data-title-text='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">email input</h2>
										<p class="skill-description">A type of input that takes user email addresses. New in HTML5.</p>
										<h3>Example</h3>
										</div>
										</div></div>
										' data-placement='right' target='_blank'>
										email input
											<div class="skills-example">
											<div class="CodeTooltip">
											  <div class="code"><pre><span class="tag">&lt;input</span> <span class="attribute-name">type</span>=<span class="string"><span class="delimiter">"</span><span class="content">email</span><span class="delimiter">"</span></span> <span class="attribute-name">placeholder</span>=<span class="string"><span class="delimiter">"</span><span class="content">Your email</span><span class="delimiter">"</span></span><span class="tag">&gt;</span></pre></div>
											</div></div>
										</span>
										<span id="submit-input-skill" class='skill has-skill-false skill-type-html' data-name='submit-input' rel='tooltip' title='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">Locked Skill</h2>
										<p class="skill-description">Your mind is not yet prepared to comprehend this skill</p>
										<p class="skill-description">Keep learning to unlock it :)</p>'  data-title-text='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">submit input</h2>
										<p class="skill-description">Makes a button to submit information from a form to a server.</p>
										<h3>Example</h3>
										</div>
										</div></div>
										' data-placement='right' target='_blank'>
										submit input
											<div class="skills-example">
											<div class="CodeTooltip">
											  <div class="code"><pre><span class="tag">&lt;input</span> <span class="attribute-name">type</span>=<span class="string"><span class="delimiter">"</span><span class="content">submit</span><span class="delimiter">"</span></span><span class="tag">&gt;</span></pre></div>
											</div></div>
										</span>	
										<span id="p-skill" class='skill has-skill-false skill-type-html' data-name='hide()' rel='tooltip' title='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">Locked Skill</h2>
										<p class="skill-description">Your mind is not yet prepared to comprehend this skill</p>
										<p class="skill-description">Keep learning to unlock it :)</p>'  data-title-text='
										<div class="skill-details">
										<div class="code">
										<div class="skill-details-content"><h2 class="skill-name">P</h2>
										<p class="skill-description">The p tag makes paragraph elements</p>
										<h3>Example</h3>
										</div>
										</div></div>' data-placement='right' target='_blank'>
										p
											<div class="skills-example">
											<div class="CodeTooltip">
											  <div class="code"><pre><span class="tag">&lt;p&gt;</span>I am a cool paragraph, full of text<span class="tag">&lt;/p&gt;</span></pre></div>
											</div></div>
										</span>								
									</div>	
									</div>
									<div class="tab-pane" id="resources_tab" role="tabpanel" style="text-align: left;">
										<p style="margin-bottom:0px;">Slide: 12</p>
										<pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
										<p style="margin-bottom:0px;">Slide: 14</p>
										<pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, an actress. Say hello!&lt;/p&gt;</pre>
										<p style="margin-bottom:0px;">Slide: 19</p>
										<pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
										<p style="margin-bottom:0px;">Slide: 23</p>	  
										<pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
										<p style="margin-bottom:0px;">Slide: 27</p>
										<pre>placeholder="Your email"</pre>
									</div>	
									<div class="tab-pane" id="credits_tab" role="tabpanel">
										Thanks and recognition for some concepts and the lesson plan that were taken from dash.generalassemb.ly
									</div>	
								</div>
							</div>
						</div>													
						
						

				    
						</div>
						<div class='col-sm-12 no-gutter code-editor' style=''>
<textarea id='myeditor' name='myeditor' style="diXsplay: none;">
      
</textarea>
						</div>
				
				</div>
				<div class='col-md-8 col-sm-6 h-100 no-gutter right-col' style=''>
					<iframe id='preview' class="" "></iframe>
 				</div>
			</div>
		</div>
    </div>

	<!-- ************** gallery page start -->
	
<div id="gallery_section">
			<div class="loading" id="loadingIcon">
              <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
              </div>
            </div>
	<div class="container">
		<h2 class="page_title">Projects</h2>
		
		 <div class="dropdown">
			<!--<button class="btn btn-secondary dropdown-toggle primary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Search By
			</button> -->
			<select   id="selectOptions" onchange="FilterData(this.event)" >
				<option class="dropdown-item"  id="projects">All Projects</option>
				<option class="dropdown-item"  id="recent">Recent</option>
				<option class="dropdown-item"  id="likes" value="MostLiked">Most Liked</option>
			</select>
		</div>
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<li class="page-item " id="previous">
				<a class="page-link" tabindex="-1" onclick="go_previous()">Previous</a>
				</li>
				<li class="page-item"><a class="page-link" href="#" id="pageNumber">1</a></li>
				
				<li class="page-item" id="next2">
				<a class="page-link"  onclick="go_next()">Next</a>
				</li>
			</ul>
		</nav>
		<div>
			<div class="gallery-container" id="galleryContainer">
			<div class="" id="gallery_page">
        <div class="container" style="height:100%; padding: 23px;overflow-y: scroll;overscroll-behavior-y: contain;">
          <!-- <h2 class="">Projects</h2>
          <div class="search-continer">
            <div class="search-list">
              <div class="">
                <h6>Search By:</h6>
              </div>
              <ul>
                <li>
                  <select name="" id="selectOptions" onchange="FilterData(this.event)">
                    <option value="Projects" id="projects">All Projects</option>
                    <option value="Recent" id="recent"> Recent</option>
                    <option value="MostLiked" id="likes">Most Liked</option>
                  </select>
                </li>
              </ul>
            </div>
          </div> 
          <div class="pagination-section">
            <ul class="pagination">
              <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
              <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
              <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
            </ul>
          </div> -->
          <div class="loading" id="loadingIcon">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
        </div>
      </div>
			</div>
			<div class="gallery-container" id="galleryLikes">

			</div>
			<div class="gallery-container" id="recentProjects">
			</div>
		</div>
	</div>
	
</div>
<!-- ************* gallery page end -->
<div class="modalPlaceholder"></div>
<!-- ************** menu page start -->
	
<div id="menu_page">
<div id="profile_page">
	<div class="container">
	<i class="fas fa-user-circle"></i>
		<h2 class="profile_name">Hello</h2>
		<div class="menu">
			<ul>
				<li class="">
					<a class="divLink" href="https://<?php echo $_SERVER['SERVER_NAME'] ?>"></a>
					<i class="fas fa-home 3x"></i> Homepage
				</li>
				<li class="reset-lesson" data-toggle="modal" data-target="#reset-lesson">
					<i class="fas fa-undo"></i> Reset Current Lesson
				</li>
				<li class="">
					<i class="fas fa-chalkboard-teacher"></i> Instructor 
					<div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
					<div class="option_unselected select_lego">Lego</div>
				</li>
				<li class="">
					<a onclick="registerModal()">
						<i class="fas fa-sign-in-alt"></i>
						<div class="option_selected" style="margin-left: 5px;">LogIn / Sign up</div>
					</a>
				</li>
				<li id="share-link" class="hide" style="display:none;">
					<i class="icon-assignment fs2"></i> Share on: 
					<div class="option_unselected facXebook-share" data-js="facebook-share" style="margin-left: 5px;">FB</div>
					<div class="option_unselected twitXter-share" style="margin-left: 5px;" data-js="twitter-share">TW</div>
					<a id="whatsapp" href="">
						<div class="option_unselected ">WA</div>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<!-- ************* menu page end -->
	
<!-- modal jquery file -->

</body>
<!--   Core JS Files   -->


<script src='../js/core/jquery.3.2.1.min.js' type='text/javascript'></script>
<script src='../assets/js/vendor/popper.min.js' type='text/javascript'></script>
<script src='../assets/js/bootstrap.min.js' type='text/javascript'></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src='../js/plugins/bootstrap-switch.js'></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<!-- <script src='../js/now-ui-kit.js?v=1.1.0' type='text/javascript'></script> -->

<!-- firebase -->
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-database.js"></script>


    
    <script src="../libraries/codemirror/mode/javascript/javascript.js"></script>
    <script src="../libraries/codemirror/mode/xml/xml.js"></script>
    <script src="../libraries/codemirror/mode/css/css.js"></script>
    <script src="../libraries/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="../libraries/codemirror/addon/edit/closetag.js"></script>
    <script src="../libraries/codemirror/addon/edit/matchbrackets.js"></script>
    <script src="../libraries/codemirror/addon/search/match-highlighter.js"></script>
    <script src="../libraries/codemirror/htmlhint.js"></script>
    <script src="../libraries/codemirror/csslint.js"></script>
    <script src="../libraries/codemirror/jshint.js"></script>
    <script src="../libraries/codemirror/addon/lint/lint.js"></script>
    <script src="../libraries/codemirror/addon/lint/html-lint.js"></script>
    <script src="../libraries/codemirror/addon/lint/css-lint.js"></script>
    <script src="../libraries/codemirror/addon/lint/javascript-lint.js"></script>
    <script src="../libraries/codemirror/markdown.js"></script>

  <script type="text/javascript">
    //<![CDATA[
    function randText() {
			var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for (var i = 0; i < 5; i++)
					text += possible.charAt(Math.floor(Math.random() * possible.length));
			return text;
		}
		
    document.write("<script type='text/javascript' src='../js/lesson.js?" + randText() + "'><\/script>");
    //]]>
  </script>	
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59d266dad184b0001230f752&product=inline-share-buttons"></script>	
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63106610-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-63106610-3');
</script>

<!-- jquery modal file -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

	</html>