<?php
Header('Vary: User-Agent, Accept');
$home_url = 'https://' . $_SERVER['SERVER_NAME'];
 $home_url = 'http://localhost:9000';
//$home_url = 'http://192.168.70.125:9000';
// $home_url = 'http://192.168.0.12:9000';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <script type="text/javascript">
    var start = Date.now();
  </script>
  <meta charset="utf-8">
  <title>Code JIKA - Learn HTML, CSS and Javascript</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <meta id="description" name="description" content="CodeJIKA Learn">
  <meta id="keywords" name="keywords" content="coding, learn to code, codejika, africa, south africa">
  <link href="<?php echo $home_url; ?>/css/merged.min.css" rel="stylesheet" type="text/css" />


 
  <script src="../js/merged_single_cdns.js"></script>

  <script type="text/javascript">
    //<![CDATA[
    function randText() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
    }

    document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url; ?>/css/mobile-lessons-m.css?" + randText() + "'\/>");
  </script>
  <style>

  </style>
</head>

<body>

  <div id="header" class="embed-nav">
    <a href="https://<?php echo $_SERVER['SERVER_NAME'] ?>" style="padding: 0!important;"><img src="<?php echo $home_url; ?>/img/logo.png" alt="logo" id="logo" style="" /></a>

    <ul id="lesson-navbar" class="hXide2" style="position: absolute; right: 0; top: 0; border: 0;">
      <li id="menu-lesson" class="active"><a><span>LESSON</span><i class="icon-assignment"></i></a></li>

      <li id="menu-code"><a><span>CODE</span><i class="icon-code1"></i></a></li>

      <li id="menu-preview"><a><span>PREVIEW</span><i class="icon-personal_video"></i></a></li>

      <!-- <li id="menu-skills" class="hide"><a><span>SKILLS</span><i class="icon-school"></i></a></li> -->
      <!-- <li id="menu-gallery"><a><span>GALLERY</span><i class="icon-images new-icon" style="font-family: 'icomoon' !important;"></i></a></li> -->
      <li id="menu-profile"><a><span>MENU</span><i class="icon-dehaze"></i></a></li>
    </ul>
  </div>
  <div class="swiper-container swiper-container-h">
    <div class="swiper-wrapper">
      <div class="swiper-slide" id="lesson-page">
        <div class="swiper-container swiper-container-v">
          <div class="swiper-wrapper cm-s-base16-dark">

            <div class="loading">
              <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination swiper-pagination-v"></div>
        </div>
      </div>
      <div class="swiper-slide" id="editor_page">

        <!-- <textarea id='code-editor' name='editor' style="display: none;visibility:hidden;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">

				</textarea> -->
        <!-- <div id="editor" contenteditable="true" class="banana-cake" style="width: 100%; height:100%"></div> -->
        <!-- <div id="lineNo"></div> -->
        <textarea onchange="editorChange()" id="editor" style="width: 100%; height:100%; display:inherit" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
        <!-- onkeyup="getLineNumber(this, document.getElementById('lineNo'));" -->

        <div class="row bottom_keys hide">
          <button class="button_design" onclick="btclicked('<')">
            < </button> <button class="button_design" onclick="btclicked('>')"> >
          </button>
          <button class="button_design" onclick='btclicked("\"")'> " </button>
          <button class="button_design" onclick="btclicked('/')"> / </button>
	  <button class="button_design" onclick="btclicked('=')"> = </button>
        </div>
      </div>
      <div class="swiper-slide" id="preview_page">
        <iframe class="" id='preview' name="preview"></iframe>

        <div class="swipe-overlay"></div>
        <div id="disallow-interact" class="icon-locked">
          <i class=" icon-uniE011 fs2"></i>
          <div>Swipe Mode</div>
        </div>
        <div id="allow-interact" class="icon-unlocked hide">
          <i class="icon-uniE010 fs2"></i>
          <div>Input Mode</div>
        </div>
      </div>
      <!-- ****************** gallery page start ***********************  -->
      
      <!-- <div class="swiper-slide" id="gallery_page">
        <div class="container" style="height:100%; padding: 23px;overflow-y: scroll;
        overscroll-behavior-y: contain;">
          <h2 class="">Projects</h2>
          <div class="search-continer">
            <div class="search-list">
              <div class="">
                <h6>Search By:</h6>
              </div>
              <ul>
                <li><a onclick="" rel="noopener noreferrer">Under 15</a></li>
              <li><a href="http://" rel="noopener noreferrer">Under 17</a></li>
              <li><a href="http://" rel="noopener noreferrer">Project 2</a></li>
              <li><a href="http://" rel="noopener noreferrer">Project 3</a></li>
              <li><a href="http://" rel="noopener noreferrer">Adventures</a></li>
                <li>
                  <select name="" id="selectOptions" onchange="FilterData(this.event)">
                    <option value="Projects" id="projects">All Projects</option>
                    <option value="Recent" id="recent"> Recent</option>
                    <option value="MostLiked" id="likes">Most Liked</option>
                  </select>
                </li>
              </ul>
            </div>
          </div>
          <div class="pagination-section">
            <ul class="pagination">
              <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
              <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
              <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
            </ul>
          </div>
          <div class="loading" id="loadingIcon">
            <div class="spinner">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
          <div class="gallery-container" id="galleryContainer">

          </div>
          <div class="gallery-container" id="galleryLikes">

          </div>
          <div class="gallery-container" id="recentProjects">
          </div>
          <div class="pagination-section" id="bottom-pagination">
            <ul class="pagination">
              <li class="page-item previous" id="previous2"><a onclick="go_previous()" class="page-link">Previous</a></li>
              <li class="page-item"><a class="page-link" id="pageNumber2"></a></li>
              <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
            </ul>
          </div>
        </div>
      </div> -->

      <!-- ////////////////////   gallery page ends /////////////////////////// -->

      <div class="swiper-slide" id="profile_page">
        <div class="container">
          <i class="icon-account_circle fs5" style="padding: 30px 0px 10px 0; display: block;"></i>
          <h2 class="profile_name">Hello</h2>
          <div class="menu">
            <ul>
              <li class=""><a class="divLink" href="https://<?php echo $_SERVER['SERVER_NAME'] ?>"></a><i class="icon-home fs2"></i> Homepage</li>
              <a href="m-projects.htm" class="project-pageLink"><li class="projects-page"><i class="icon-local_library"></i> Explore Projects </li></a>
              <li class="reset-lesson" data-toggle="modal" data-target="#reset-lesson"><i class="icon-replay fs2"></i> Reset Current Lesson</li>
              <li class="create-GalleryPage" data-toggle="modal" data-target="#createGalleryPage"><i class="icon-images icomoon"></i> Gallery</li>
              <li class="create-profilePage" data-toggle="modal" data-target="#createProfilePage"><i class="icon-account_circle fs2"></i> Profile</li>
              <li class=""><i class="icon-face fs2"></i> Instructor <div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
                <div class="option_unselected select_lego">Lego</div>
              </li>
              <li class=""><a onclick="registerModal()"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i>
                  <div class="option_selected" style="margin-left: 5px;">LogIn / Sign up</div>
                </a></li>

              <li id="share-link" class="hide"><i class="icon-assignment fs2"></i> Share on: <div class="option_unselected facXebook-share" data-js="facebook-share" style="margin-left: 5px;">FB</div>
                <div class="option_unselected twitXter-share" style="margin-left: 5px;" data-js="twitter-share">TW</div> <a id="whatsapp" href="">
                  <div class="option_unselected ">WA</div>
                </a>
              </li>
            </ul>

          </div>
        </div>
      </div>
    </div>

    <div class="swiper-pagination swiper-pagination-h"></div>
  </div>

  </div>


  <div class="modalPlaceholder"></div>

  <div class="success-animation hide" style="">

    <span class="success-layers">
      <svg class="success-svg-icon" style="color: #fff;" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
        <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
      </svg><!-- <i class="fas fa-circle" style="color:#fff"></i> -->
      <svg class="success-svg-icon" style="color: #00A921;" aria-hidden="true" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
        <path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path>
      </svg><!-- <i class="fas fa-inverse fa-check-circle" style="color:#00A921"></i> -->
    </span>

    </span>
  </div>

  <!--   Core JS Files   -->
  <!-- <script type="text/javascript" src="js/script.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript" ></script> -->
  <!-- <script src="https://unpkg.com/swiper/js/swiper.min.js"></script> -->

  <!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->


  <!-- <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-app.js"></script> -->
  <!-- <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase-database.js"></script> -->
  <!-- <script src='https://idangero.us/swiper/js/vendor/jquery-1.11.0.min.js' type='text/javascript'></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://idangero.us/swiper/dist/js/swiper.min.js"></script> -->
  <script src="../js/confirmDialog.jquery.js"></script>
  <!-- <script src='<?php echo $home_url; ?>/js/merged.js' type='text/javascript' id="js-jquery"></script> -->
  <!-- Global site tag (gtag.js) - Google Analytics -->



  <script>
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-63106610-3', 'auto');
    ga('send', 'pageview');
  </script>


  <script>
    var lessons_data = {
      "5-minute-website-dev": {
        lesson_id: "P001-L00-M-V003",
        save_id: "5-minute-website-dev"
      },
      "5-minute-website": {
        lesson_id: "P001-L00-M-V002",
        save_id: "5-minute-website"
      },
      "5-minute-website": {
        lesson_id: "P001-L00-M-V004-B",
        save_id: "5-minute-website"
      },
      "10-minute-website": {
        lesson_id: "P001-L00-M-V001",
        save_id: "10-minute-website"
      },
      "javascript-intro-the-party": {
        lesson_id: "P000-J01-M-V001",
        save_id: "javascript-intro-the-party"
      },
      "my-first-landing-page-1": {
        lesson_id: "P001-T01-M-V001",
        save_id: "my-first-landing-page-1"
      },
      "P1Training1" : {
        lesson_id : 'P001-T01-M-V007',
        save_id: "P1Training1"
      },
      "P1Training2" : {
        lesson_id : 'P001-T02-M-V008',
        save_id: "P1Training2"
      }
    };



    var lessson_url = "<?php echo htmlspecialchars($_GET["lesson_id"]); ?>";
    var set_user_id = "<?php echo htmlspecialchars($_GET["user_id"]); ?>";

    // var lesson_id = lessons_data[lessson_url]["lesson_id"];
    var lesson_id = 'P001-L00-M-V004-B'
    // var save_lesson_id = lessons_data[lessson_url]["save_id"];
    var save_lesson_id = "5-minute-website"

    // P1T2 lesson id
    // var lesson_id = 'P001-T01-M-V007'
    // var save_lesson_id = "project-1"

    console.log("lesson_url: " + lessson_url);
    console.log("lesson_id: " + lesson_id);
    console.log("set_user_id: " + set_user_id);
    console.log("save_lesson_id: " + save_lesson_id);

    // var timeout;
    // history.pushState(null, null, location.href);
    // window.onpopstate = function() {
    //   history.go(1);

    //   clearTimeout(timeout);
    //   timeout = setTimeout(function() {
    //     if (swiperTabs.activeIndex == 0) {
    //       swiperLesson.slideTo(swiperLesson.activeIndex - 1);
    //       console.log("on lesson tab" + swiperLesson.activeIndex);
    //     }
    //     if (swiperTabs.activeIndex == 4) {
    //       // swiperLesson.slideTo(swiperLesson.activeIndex - 1);
    //       alert()
    //       console.log("on lesson tab" + swiperLesson.activeIndex);
    //     } else {
    //       swiperTabs.slideTo(swiperTabs.activeIndex - 1);
    //       console.log("on other tab" + swiperTabs.activeIndex);
    //     }

    //   }, 50);


    // };
    document.write("<script type='text/javascript' src='<?php echo $home_url; ?>/js/mobile-lesson-P00v3.js?" + randText() + "'><\/script>");



    var is_keyboard = false;
    var initial_screen_size = window.innerHeight;
    window.addEventListener("resize", function() {
      is_keyboard = (window.innerHeight < initial_screen_size);

      console.log(is_keyboard, 'Here');
      if (!is_keyboard) {
        $('textarea').blur();
      }
    }, false);


    function btclicked(symb) {
      insertAtCaret(document.getElementById('editor'), symb);
      console.log("Hello");
    }

    function insertAtCaret(element, text) {
      if (document.selection) {
        console.log('Hello');
        element.focus();
        var sel = document.selection.createRange();
        sel.text = text;
        element.focus();
      } else if (element.selectionStart || element.selectionStart === 0) {
        var startPos = element.selectionStart;
        var endPos = element.selectionEnd;
        var scrollTop = element.scrollTop;
        element.value =
          element.value.substring(0, startPos) +
          text +
          element.value.substring(endPos, element.value.length);
        element.focus();
        element.selectionStart = startPos + text.length;
        element.selectionEnd = startPos + text.length;
        element.scrollTop = scrollTop;
      } else {
        element.value += text;
        element.focus();
      }
    }

    $('textarea').on('keydown', function(e) {
      var keyCode = e.keyCode || e.which;

      if (keyCode === 9) {
        e.preventDefault();
        var start = this.selectionStart;
        var end = this.selectionEnd;
        var val = this.value;
        var selected = val.substring(start, end);
        var re = /^/gm;
        var count = selected.match(re).length;


        this.value = val.substring(0, start) + selected.replace(re, '\t') + val.substring(end);
        this.selectionStart = start;
        this.selectionEnd = end + count;
      }
    });

    var myElement = document.getElementById('editor');

    myElement.addEventListener("touchstart", startTouch, false);
    myElement.addEventListener("touchmove", moveTouch, false);

    // Swipe Up / Down / Left / Right
    var initialX = null;
    var initialY = null;

    function startTouch(e) {
      initialX = e.touches[0].clientX;
      initialY = e.touches[0].clientY;
    };

    function moveTouch(e) {
      if (initialX === null) {
        return;
      }

      if (initialY === null) {
        return;
      }

      var currentX = e.touches[0].clientX;
      var currentY = e.touches[0].clientY;

      var diffX = initialX - currentX;
      var diffY = initialY - currentY;

      if (Math.abs(diffX) > Math.abs(diffY)) {
        // sliding horizontally
        if (diffX > 0) {
          // swiped left
          $('textarea').blur();
        } else {
          // swiped right
          // alert('Swipe Right');
          $('textarea').blur();
        }
      } else {
        // sliding vertically
        if (diffY > 0) {
          // swiped up
          console.log("swiped up");
        } else {
          // swiped down
          console.log("swiped down");
        }
      }

      initialX = null;
      initialY = null;

      e.preventDefault();
    };

    // TLN line number code below
    const TLN = {
      eventList: {},
      update_line_numbers: function(ta, el) {
        let lines = ta.value.split("\n").length;
        let child_count = el.children.length;
        let difference = lines - child_count;

        if (difference > 0) {
          let frag = document.createDocumentFragment();
          while (difference > 0) {
            let line_number = document.createElement("span");
            line_number.className = "tln-line";
            frag.appendChild(line_number);
            difference--;
          }
          el.appendChild(frag);
        }
        while (difference < 0) {
          el.removeChild(el.firstChild);
          difference++;
        }
      },
      append_line_numbers: function(id) {
        let ta = document.getElementById(id);
        if (ta == null) {
          return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
        }
        if (ta.className.indexOf("tln-active") != -1) {
          return console.warn("[tln.js] textarea of id '" + id + "' is already numbered");
        }
        ta.classList.add("tln-active");
        ta.style = {};

        let el = document.createElement("div");
        ta.parentNode.insertBefore(el, ta);
        el.className = "tln-wrapper";
        TLN.update_line_numbers(ta, el);
        TLN.eventList[id] = [];

        const __change_evts = [
          "propertychange", "input", "keydown", "keyup", 'focus'
        ];
        const __change_hdlr = function(ta, el) {
          return function(e) {
            if ((+ta.scrollLeft == 10 && (e.keyCode == 37 || e.which == 37 ||
                e.code == "ArrowLeft" || e.key == "ArrowLeft")) ||
              e.keyCode == 36 || e.which == 36 || e.code == "Home" || e.key == "Home" ||
              e.keyCode == 13 || e.which == 13 || e.code == "Enter" || e.key == "Enter" ||
              e.code == "NumpadEnter")
              ta.scrollLeft = 0;
            TLN.update_line_numbers(ta, el);
          }
        }(ta, el);
        for (let i = __change_evts.length - 1; i >= 0; i--) {
          ta.addEventListener(__change_evts[i], __change_hdlr);
          TLN.eventList[id].push({
            evt: __change_evts[i],
            hdlr: __change_hdlr
          });
        }

        const __scroll_evts = ["change", "mousewheel", "scroll", "focus"];
        const __scroll_hdlr = function(ta, el) {
          return function() {
            el.scrollTop = ta.scrollTop;
          }
        }(ta, el);
        for (let i = __scroll_evts.length - 1; i >= 0; i--) {
          ta.addEventListener(__scroll_evts[i], __scroll_hdlr);
          TLN.eventList[id].push({
            evt: __scroll_evts[i],
            hdlr: __scroll_hdlr
          });
        }
      },
      remove_line_numbers: function(id) {
        let ta = document.getElementById(id);
        if (ta == null) {
          return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
        }
        if (ta.className.indexOf("tln-active") == -1) {
          return console.warn("[tln.js] textarea of id '" + id + "' isn't numbered");
        }
        ta.classList.remove("tln-active");

        ta.previousSibling.remove();

        if (!TLN.eventList[id]) return;
        for (let i = TLN.eventList[id].length - 1; i >= 0; i--) {
          const evt = TLN.eventList[id][i];
          ta.removeEventListener(evt.evt, evt.hdlr);
        }
        delete TLN.eventList[id];
      }
    }

    // TLN line No. code ends here
  </script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

  <!-- PDF MAke scripts -->
  <script src="../js/pdfmake.min.js"></script>
  <script src="../js/pdfmakeFonts.js"></script>
</body>

</html>
