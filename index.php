<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */
header("X-Robots-Tag: noindex, nofollow", true);
header("Access-Control-Allow-Origin: *");
 
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$uri_segments = explode('/', $uri_path);

$lang_path = rtrim($uri_path, '/');
/*
  foreach ($_SERVER as $name => $value) {
    echo "$name: $value\n";
} 
*/

$cookie_name = "geo_redirect_once_".str_replace('.', '_', $_SERVER['SERVER_NAME']);
/*
if ( !isset( $_COOKIE[$cookie_name] ) || !isset( $_COOKIE['pll_language'] )  ) {
  
require_once("geoip.inc");

$gi = geoip_open("GeoIP.dat",GEOIP_STANDARD);

$country_code = geoip_country_code_by_addr($gi, $_SERVER['REMOTE_ADDR']);

geoip_close($gi);
*/


  /*      Disabled show country homepage on first visit based on GeoIP - 19 Oct 2018
$country_code = $_SERVER['HTTP_CF_IPCOUNTRY'];

    
  if ($lang_path == "" || $lang_path == "/" || $lang_path == "/index.php") { //&& $cookie_name != false) {
  setcookie( $cookie_name, $country_code."_".$_SERVER['SERVER_NAME'], time() + 60 * 60 * 24 * 1, '/' ); // 1 day(s)
 //  echo '<script>console.log("$country_code: '. $country_code .'")</script>';    
  if($country_code == 'ZA') {
          header('Location: https://'.$_SERVER['SERVER_NAME'].'/southafrica/');
  } elseif($country_code == 'MU') {
          header('Location: https://'.$_SERVER['SERVER_NAME'].'/zambia/');
  } elseif($country_code == 'NA') {
          header('Location: https://'.$_SERVER['SERVER_NAME'].'/namibia/');
  }  elseif($country_code == 'BW') {
          header('Location: https://'.$_SERVER['SERVER_NAME'].'/botswana/');
  }  elseif($country_code == 'MZ') {
          header('Location: https://'.$_SERVER['SERVER_NAME'].'/mozambique/');
  } 

  }
  
} */

/*
if($lang_path == '/zambia' || $lang_path == '/zm') {
 echo '<script>console.log("'. $lang_path .'")</script>';
 include('index-zambia.php'); 

} else if($lang_path == '/namibia' || $lang_path == '/na') {
   echo '<script>console.log("'. $lang_path .'")</script>';
 include('index-default.php'); 
 exit;
} else if($lang_path == '/botswana' || $lang_path == '/bw') {
   echo '<script>console.log("'. $lang_path .'")</script>';
 include('index-default.php');
 exit;
} else if ($lang_path == "" || $lang_path == "/" || $lang_path == "/index.php"){
   include('index-default.php'); 
   $lang_path = "";
   
} 
else {

//echo '<script>console.log("'. $uri_path .'")</script>';
}
*/

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
//define( 'WP_SITEURL', 'https://staging.codejika.com' );
//define( 'WP_HOME', 'https://staging.codejika.com' );


/** Loads the WordPress Environment and Template */
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
//echo '<script>console.log("WP_SITEURL: '. constant("WP_SITEURL") .'")</script>';
//echo '<script>console.log("WP_DEBUG_LOG: '. constant("WP_DEBUG_LOG") .'")</script>';
//echo '<script>console.log("pll_home_url: '. pll_home_url() .'")</script>';
//}
