<?php $home_url = 'https://'.$_SERVER['SERVER_NAME']; ?>

 <div class="modal-body">
<link rel='stylesheet' id='contact-form-7-css'  href='https://www.codejika.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.2' type='text/css' media='all' />

<script type='text/javascript'>
/* <![CDATA[ */
//var wpcf7 = {"apiSettings":{"root":"https:\/\/<?php echo $_SERVER['SERVER_NAME'];?>\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo $home_url;?>/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.2'></script>
<script type='text/javascript' src='<?php echo $home_url;?>/static/scripts.js'></script> 
<form  class="wpcf7-form floraforms" class="container" novalidate="" id="myForm">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="1610" />
<input type="hidden" name="_wpcf7_version" value="5.0.2" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1610-p1611-o1" />
<input type="hidden" name="_wpcf7_container_post" value="1611" />
</div>
<div id="formMultiSteps" class="carousel slide" data-ride="false" data-wrap="false">

  <div class="carousel-inner">
  <div class="carousel-item active">
    <div class="form-row">
      <div class="col-md-24 mb-3">
      <h2 class="mb-1 text-center" >Whatcha think?</h2>
      <p class="text-center"> Something you didn't like, didn't work? Did work, did like? </p>
        <label class="form-control-label mb-1" for="inputSuccess2">Feedback Topic</label>
        <label class="form-control-label flo-select" for="inputSuccess2">
           <select name="your-topic">
            <option value="Glitch">Glitch</option>
            <option value="I like it">I like it</option>
            <option value="I don't like it">I don't like it</option>
            <option value="New feature suggestion">New feature suggestion</option>
            <option value="Other">Other</option>
          </select><i class="arrow double"></i></label>
      </div>        
      <div class="col-md-24 mb-3">     
        <label class="form-control-label mb-1" for="inputSuccess2">the  ... is broken because ... </label>
         <textarea rows="4" cols="50" class="form-control flo-textarea" name="your-feedback" required id="your-feedback" placeholder="Type your feedback here" autocomplete="off"></textarea>
      </div>
      <div class="col-md-24 mb-3">      
        <div  class="btn btn-lg text-center btn-secondary carousel-next" id="">Next</div>
      </div>
    </div>

  </div>
  <div class="carousel-item">     
    <div class="form-row">
      <div class="col-md-24 mb-3">
      <h2 class="mb-1 text-center" >One last step</h2> 
      <p class="text-center"> Please give us your contact details so we can let know how we improved.</p>     
        <label class="form-control-label mb-1" for="inputSuccess2">Enter your Name (Optional)</label>     
        <input type="text" class="form-control flo-input" name="your-name" id="your-name" required data-valrule="min:2:Minimum 2 characters|required:Please enter your name" placeholder="First name" autocomplete="off"  >     </div>        
      <div class="col-md-24 mb-3">     
        <label class="form-control-label mb-1" for="inputSuccess2">Enter your Email Address (Optional)</label>
        <input type="email" class="form-control flo-input" name="your-email" required id="your-email" data-valrule="email:Enter a valid E-Mail Address!|required:Please enter your name" placeholder="Email Adddress" autocomplete="off">
      </div>
    <div>
        <button  class="btn btn-lg text-center btn-secondary" id="btnSubmit">Submit</button>
    </div>      
    </div>

  </div>
  </div>    

    <div class="wpcf7-response-output wpcf7-display-none"></div>

   
  <ol class="carousel-indicators">
    <li data-target="#formMultiSteps" data-slide-to="0" class="active"></li>
    <li data-target="#formMultiSteps" data-slide-to="1"></li>
  </ol>
</div>
</form>
<div class="success" style="display:none;">
      <div class="col-md-24 mb-3 text-center">
        <h2 class="mt-5 mb-2" >We love hearing from you!</h2> 
        <img src="https://www.codejika.com/static/high-five.jpg"/>
        <p class="text-center mt-2"> Thanks for your feedback, we appreciate it.</p>      
      </div>
</div>
</div>
       <div class="modal-footer">We need your feeback. Thanks for coming here!
        


  </div>

<script>



$( document ).ready(function() {

carouselNormalization('#formMultiSteps .carousel-item');

submitButton("#btnSubmit", "#myForm", "1610" );

$(".carousel-prev").click(function(){
  $("#formMultiSteps").carousel("prev");
});

$(".carousel-next").click(function(){
$("#formMultiSteps").carousel("next");
});

});
</script>