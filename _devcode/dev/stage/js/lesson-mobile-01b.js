/*
// CodeMirror HTMLHint Integration
(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "html", function(text) {
    var found = [], message;
    if (!window.HTMLHint) return found;
    var messages = HTMLHint.verify(text, ruleSets);
    for ( var i = 0; i < messages.length; i++) {
      message = messages[i];
      var startLine = message.line -1, endLine = message.line -1, startCol = message.col -1, endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity : message.type
      });
    }
    return found;
  }); 
});


// ruleSets for HTMLLint
var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};
*/
var delay;



	
// Initialize CodeMirror editor
var editor = CodeMirror.fromTextArea(document.getElementById("code-editor"), {
 mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',  
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop : true,
  
  lint: true,
	onKeyEvent: function (e, s) {
    if (s.type == "keyup") {
        CodeMirror.showHint(e);
    }
},
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
}),
    inf = editor.getInputField();

	$.keyboard.keyaction.undo = function (base) {
		base.execCommand('undo');
		return false;
	};
	$.keyboard.keyaction.redo = function (base) {
		base.execCommand('redo');
		return false;
	};
	
 $(inf).keyboard({
    keyBinding: "pointerdown",
		usePreview: false,
		useCombos: false,
		autoAccept: true,
	layout: 'custom',
	customLayout: {
		'normal': [
			'< > / " \' = : ; {left} {right}'
		]
	},
	display: {
		redo: '↻',
		undo: '↺'
	},
    position: false,
    initialized: function(evnt, keyboard, elem, txt) {
   //   $(".ui-keyboard").detach().appendTo('#virt-keyboard');
	//  console.log("b4 vis");
    },
    beforeInsert: function(evnt, keyboard, elem, txt) {
      var position = editor.getCursor();
      if (txt === "\b") {
        editor.execCommand("delCharBefore");
      }
      if (txt === "\b" && position.ch === 0 && position.line !== 0) {
        elem.value = editor.getLine(position.line) || "";
        txt = "";
      }
      return txt;
    }
  });



function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();

	validateCheckPoint();
}

function successAnimation() {
	$(".success-animation").removeClass('hide');
		console.log('animate1');
setTimeout(function(){
    $(".success-animation").addClass('hide');
	console.log('animate2');
}, 2500);	

	
}	
	

function validateCheckPoint() {
	 currentStep = swiperV.activeIndex+1;
		console.log('Trigger validation'+checkPoints[currentStep]);

	var unlockSkills = {
		12: "h1-h6",
		14: "p",
		19: "email-input",
		23: "submit-input"
	}
	
	if( checkPoints[currentStep] !== undefined ) {
			console.log('Trigger check');
    // do something
		console.log('Run validation only on '+currentStep);
		var re = checkPoints[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
			$(".check-info").removeClass('d-none');			
		if (reResults) {
			$("#slide"+currentStep+" .check-icon").html('/');
			$("#slide"+currentStep+" .actions li").addClass('correct');
			$(".check-info").addClass('correct');			
			$("#slide"+currentStep).addClass('cp-unlocked');	
			successAnimation();
			
			$(".check-info .check-icon").html('/');			
						$(".pagination .next").addClass('disabled');
			//$(".pagination .next").removeClass('disabled');
					console.log('Run validation using '+checkPoints[currentStep]);
								$(".success-animation").show();	
		
			if( unlockSkills[currentStep] !== undefined ) {
						$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
						$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
				console.log('Unlocked skill '+unlockSkills[currentStep]);
				console.log("#"+currentStep+"-skill");

			  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
			 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
			  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
			  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
			}
		
		
		} else {
			$("#slide"+currentStep+" .check-icon").html('\\');
			$("#slide"+currentStep+" .actions li").removeClass('correct');
			$(".check-info .check-icon").html('\\');
			$(".check-info").removeClass('correct');
			$(".pagination .next").addClass('disabled');
		}
	} else {
		$(".check-info").addClass('d-none');
	}
	

	
}

//var activeTab = $(".tab-content .tab-content").find(".active");
//var id = activeTab.attr('id');
//console.log('Run validation only on '+currentStep);









$(document).ready(function () {

	//Initialize tooltips
    	$('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

   /*# $("#lesson_tab .next").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        $active.parent().next().removeClass('disabled');
        nextTab($active);

    });
    $("#lesson_tab .prev").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        prevTab($active);

    });*/
	
	    //  Activate the Tooltips
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
		html: true,
		trigger: 'manual'
}).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function(){
	   $('#'+$(this).attr('id')+'.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
    });

	
setTimeout(updatePreview, 300);

// Live preview
editor.on('change', function() {
  clearTimeout(delay);
  	console.log('preview');
  delay = setTimeout(updatePreview, 300);
});
 var lessonLoaded = false;
      
//initFullPage();

	$("#fp-nav ul li:nth-child(2) a span").addClass('cp-green');
	$("#fp-nav ul li:nth-child(12) a span").addClass('cp-red');
	
	swiperH = new Swiper('.swiper-container-h', {
      spaceBetween: 0,
	  preventClicks: false,
	  preventClicksPropagation: false,
	  allowToXuchMove: false,
      pagiXnation: {
        el: '.swiper-pagination-h',
        clickable: true,
      },
		shortSwipes: true,
		
	  loop : false,
	  on: {
        init: function () {

        },
        slideChange: function () {
			 var slider = this;
			$('#lesson-navbar li.active').removeClass('active');
			var mindex = slider.activeIndex+1;
            $('#lesson-navbar li:nth-child('+mindex+')').addClass('active');
			//console.log('#lesson-navbar:nth-child('+mindex+')');
			if (slider.activeIndex === 1) {
				$('.CodeMirror-code').focus();	
				editor.focus();	
				$('.ui-keyboard').show();
			  } else {
				$('.CodeMirror-code').blur();
				$('.ui-keyboard').hide();
				
			  }
			 if (slider.activeIndex <= 2) {
				slider.allowSlideNext = true;
			  }	 else {
				 slider.allowSlideNext = false;
			  }	
		//  console.log(slider.realIndex);			  
        },
	   }
  
    });
    swiperV = new Swiper('.swiper-container-v', {
      direction: 'vertical',
      spaceBetween: 50,
	  		shortSwipes: false,
	  mousewheel: true,
	  	  preventClicks: false,
	  preventClicksPropagation: false,
	  allowTouXchMove: false,
	      // Disable preloading of all images
    preloadImages: false,
    // Enable lazy loading
    lazy: true,
      pagination: {
        el: '.swiper-pagination-v',
        clickable: false,
      },
	  observer: true,
	  observeParents: true,
	  
	  on: {
        init: function () {

        },
        slideChange: function () {
          var slider = this;
		  var mindex = slider.activeIndex+1;
		var unlocked = $("#slide"+mindex).hasClass("cp-unlocked");
         if (!unlocked && checkPoints[mindex] !== undefined) {
            slider.allowSlideNext = false;
					  console.log('lock'+unlocked);
          } else {
		     slider.allowSlideNext = true;
			 console.log('unlock');
          }
		  console.log(mindex);

		  $('.swiper-pagination-v span:nth-child('+mindex+')').prevAll().addClass('bullet-green');
		  $('.swiper-pagination-v span:nth-child('+mindex+')').next().removeClass('bullet-green');
  
        },
	   }
    });

	var swiperTabs = document.querySelector('.swiper-container-h').swiper;
	var swiperLesson = document.querySelector('.swiper-container-v').swiper;
	
	$('#lesson-navbar li').click(function(){
		if ($(this).index() === 4) {
		console.log(swiperH.activeIndex);
					  console.log($(this).index());
		swiperH.allowSlideNext = true;
			swiperH.slideTo($(this).index());			
			swiperH.allowSlideNext = false;
					  
		}	 else {	
		console.log("dfd"+swiperH.activeIndex);
			 console.log("dfd"+$(this).index());
			swiperH.slideTo($(this).index());
			$('#lesson-navbar li.active').removeClass('active');
			$(this).addClass('active')
	
		}
    })	
	$('.swiper-pagination-switch').click(function(){
        swiperH.slideTo($(this).index()+1);
        $('.swiper-pagination-switch').removeClass('active');
		$(this).addClass('active')
    })	
	$('.swiper-next').click(function(){
        swiperLesson.allowSlideNext = true;
        swiperLesson.slideNext();
        //swiperLesson.allowSlideNext = false;
		console.log("skip");
    })
	$('.swiper-editor').click(function(){
        swiperTabs.allowSlideNext = true;
        swiperTabs.slideNext();
        //swiperTabs.allowSlideNext = false;
		console.log("skip");
    })
	

//# var lessonProgressBar = document.getElementById('lessonProgressBar');
//# var maxNum = lessonProgressBar.dataset.valuemax;
 currentStep = 0;
 
 checkPoints = {
		12: "(<h1>|<h1 [^>]*>)\s*(.*?)\s*<\/h1>",
		14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
		19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
		23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
		27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))"
	}
/*
// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
	if (currentStep = parseInt(url.split('#slide')[1])) {
		$('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]').tab('show') ;
		//console.log('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]');
	} else {
		$('#submenu.nav-tabs a[href="#'+url.split('#')[1]+'"]').tab('show') ;
	}
	
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})

*/



});





