var lesson_id = 1;

function initFirebase() {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
  }

  firebase.initializeApp(config);
}

function getLessonDataPromise() {

  return firebase.database().ref('/lessons/' + lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonDataPromise");
}

function getUserProfilePromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/profile').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserProfilePromise");
}

function getUserSkillsPromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/skills').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserSkillsPromise");
}

function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonProgressPromise");
}

function saveUserData(slide_id) {
    console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);

   return firebase.database().ref('/lessons/' + lesson_id +'/slides/'+ slide_id).update({
		action: true
		checkpoint: false
		created_at: "2017-08-23T00:55:40.386Z"
		html_content: "<img data-src='../img/slides-01/HTML-Basics-Sli..."
		js_function: "console.log('I am a DB loaded function')"
		sort_order: 1
		updated_at: timeNow()

    });
   str = JSON.stringify(all_slides[slide_id], null, 4);
   console.log("slide_data saved as:\n" + str);

  } else {
    //console.log("Nothing to save: "+timeNow());
  }
}

