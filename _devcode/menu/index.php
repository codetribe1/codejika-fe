
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jQuery Simple MobileMenu Plugin Example</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="jquery-simple-mobilemenu.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="jquery-simple-mobilemenu.min.js"></script>
    <style>
 .sm_menu_outer.active {
    max-width: 500px;
    overflow-y: hidden;
}

.sm_menu_outer_overlay:after {
      -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    content: "";
    left: 0;
    opacity: 0;
    position: absolute;
    width: 100%;
    -o-transition: opacity .15s linear,visibility 0s linear .15s;
    -webkit-transition: opacity .15s linear,visibility 0s linear .15s;
    background-color: rgba(0,0,0,.6);
    display: block;
    height: calc(100vh + 2.1875rem);
    top: -2.1875rem;
    transition: opacity .15s linear,visibility 0s linear .15s;
    visibility: hidden;
    z-index: 1;
}
.mmactive .sm_menu_outer_overlay:after {
    -o-transition-delay: 0s;
    -webkit-transition-delay: 0s;
    opacity: 1;
    transition-delay: 0s;
    visibility: visible;
}

.sm_menu_outer {
    height: 100vh;
}
.sm_menu_outer .mobile_menu a {

    padding: 17px 15px;

}
.sm_menu_outer .mobile_menu li.back a {
    padding: 17px 65px;
}

.sm_menu_outer .mobile_menu li:not(:first-child) {

    border-bottom: 1px solid rgb(152, 152, 152);
   bordeXr-image: none;
    color: #ffffff;
    margin: 0px 30px;
}

.sm_menu_outer .mobile_menu li.back {
      border-bottom: 1px solid rgb(152, 152, 152);
       border-image: none;
}

.mmactive #sm_menu_ham span {
      background-color: #333;
}

.ctaPrimary, .ctaSecondary, .formFileUpload input[type=file]+label, .tagLink {
    -moz-appearance: none;
    -webkit-appearance: none;
    -webkit-box-sizing: border-box;
    background: none;
    border: none;
    box-sizing: border-box;
    cursor: pointer;
    display: inline-block;
    text-align: center;
}

.ctaPrimary {
    -o-transition: all .15s linear;
    -webkit-box-shadow: 0 0 0 0.0625rem #007c89 inset;
    -webkit-transition: all .15s linear;
    background-color: #007c89;
    box-shadow: inset 0 0 0 0.0625rem #007c89;
    color: #fff!important;
    font-family: Graphik,Helvetica Neue,Helvetica,Arial,Verdana,sans-serif;
    font-size: .9375rem;
    font-weight: 600;
    padding: 1.25rem 2.5rem;
    transition: all .15s linear;
    width: 100%
}

    </style>
</head>

<body>
    <header>
        <div class="logo-port"><img src="https://www.codejika.com/img/logo-jika_150x74.png" style=""></div>
 
    <ul class="mobile_menu">
  <li><a href="" style="color: #fff;    background: #fff;border-bottom: 1px solid #333;">Home</a></li>
  <li>
    <a href="#">Category 1</a>
      <ul class="submenu">
          <li><a href="#">Category 1.1</a></li>
          <li>
            <a href="#">Category 1.2</a>
              <ul class="submenu">
                  <li><a href="#">Category 1.2.1</a> </li>
                  <li>
                    <a href="#">Category 1.2.2</a>
                      <ul class="submenu">
                          <li><a href="#">Category 1.2.2.1</a></li>
                          <li><a href="#">Category 1.2.2.2</a></li>
                          <li><a href="#">Category 1.2.2.3</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Category 1.2.3</a></li>
              </ul>
          </li>
          <li>
            <a href="#">Category 1.3</a>
              <ul class="submenu">
                  <li> <a href="#">Category 1.3.1</a> </li>
                  <li> <a href="#">Category 1.3.2</a> </li>
                  <li> <a href="#">Category 1.3.3</a> </li>
              </ul>
          </li>
      </ul>
  </li>
  <li>
    <a href="#">Category 2</a>
      <ul class="submenu">
          <li><a href="">Category 2.1</a></li>
          <li><a href="">Category 2.2</a></li>
          <li><a href="">Category 2.3</a></li>
      </ul>
  </li>
  <li> <a href="#">Category 3</a> </li>
  <li> <a href="#">Category 4</a> </li>
  <li> <a href="#">Category 5</a> </li>
  <div style="text-align: center; padding: 40px 30px 0px 30px;">
  <a href="#" class="ctaPrimary ctaPrimary--nav ctaPrimary--mobile"  tabindex="0">Start Free Coding Lesson!</a>
      </div>  <div style="text-align: center;">
  <a href="#" class=""  style=" font-size: 85%;"  tabindex="0">Start a Coding Club</a>
                </div>
</ul>   </header>
    <div class="lorem-content">
        <h1>New Menu Demo</h1>
        <p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Risus nec feugiat in fermentum posuere urna nec tincidunt. Vel elit scelerisque mauris pellentesque. Mauris ultrices eros in cursus turpis massa tincidunt. Odio ut enim blandit volutpat. Vitae suscipit tellus mauris a diam maecenas sed enim ut. Risus commodo viverra maecenas accumsan lacus vel facilisis. Sagittis purus sit amet volutpat consequat mauris nunc congue. Felis imperdiet proin fermentum leo vel orci. Elit ut aliquam purus sit. Nec ultrices dui sapien eget mi. Quam id leo in vitae turpis massa. Et ligula ullamcorper malesuada proin libero. Nec feugiat nisl pretium fusce id velit ut tortor. Velit euismod in pellentesque massa. Non nisi est sit amet facilisis. Urna neque viverra justo nec. A condimentum vitae sapien pellentesque habitant morbi tristique senectus. Vivamus at augue eget arcu dictum. Feugiat nibh sed pulvinar proin.</p>
        <p>Ultricies tristique nulla aliquet enim tortor at. Faucibus a pellentesque sit amet porttitor eget dolor morbi. Sed vulputate mi sit amet. In ante metus dictum at tempor. Non blandit massa enim nec dui nunc mattis enim ut. Viverra tellus in hac habitasse platea dictumst vestibulum. Venenatis lectus magna fringilla urna porttitor. Faucibus pulvinar elementum integer enim. Massa eget egestas purus viverra accumsan in nisl nisi. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus. Augue mauris augue neque gravida. Rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi. Elit at imperdiet dui accumsan sit amet nulla facilisi. Duis ultricies lacus sed turpis tincidunt id aliquet.</p>
        <p>Venenatis a condimentum vitae sapien pellentesque habitant morbi. Sem et tortor consequat id porta nibh. At quis risus sed vulputate odio. Eget lorem dolor sed viverra ipsum nunc. Magna fermentum iaculis eu non diam phasellus vestibulum lorem. Mi quis hendrerit dolor magna eget. Faucibus vitae aliquet nec ullamcorper sit amet risus nullam eget. Aliquet nibh praesent tristique magna sit amet purus gravida. Nec dui nunc mattis enim ut tellus elementum. Scelerisque felis imperdiet proin fermentum leo vel orci porta. Tortor vitae purus faucibus ornare. Morbi tristique senectus et netus et malesuada fames. Morbi tincidunt augue interdum velit euismod in.</p>
        <p>Nulla at volutpat diam ut venenatis tellus. Turpis egestas maecenas pharetra convallis posuere morbi. Dui nunc mattis enim ut tellus. Arcu cursus euismod quis viverra nibh cras pulvinar. Nisi lacus sed viverra tellus in hac habitasse platea. Sapien pellentesque habitant morbi tristique senectus et netus et malesuada. Massa sapien faucibus et molestie ac feugiat sed. Ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum. Feugiat nisl pretium fusce id velit. Sed adipiscing diam donec adipiscing tristique risus nec feugiat in. Pellentesque habitant morbi tristique senectus et netus et malesuada. Nibh praesent tristique magna sit amet purus gravida. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Id aliquet risus feugiat in. Nunc pulvinar sapien et ligula. Imperdiet proin fermentum leo vel orci porta non pulvinar.</p>
    </div>
    <div class="sm_menu_outer_overlay">    </div>
    <script>
    $(document).ready(function() {
        $(".mobile_menu").slideMobileMenu({
            onMenuLoad: function(menu) {
                console.log(menu)
                console.log("menu loaded")
            },
            onMenuToggle: function(menu, opened) {
                console.log(opened)
            }
        });
    })
    </script>
</body>
</html>
