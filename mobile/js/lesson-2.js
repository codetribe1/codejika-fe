// CodeMirror HTMLHint Integration
(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "html", function(text) {
    var found = [], message;
    if (!window.HTMLHint) return found;
    var messages = HTMLHint.verify(text, ruleSets);
    for ( var i = 0; i < messages.length; i++) {
      message = messages[i];
      var startLine = message.line -1, endLine = message.line -1, startCol = message.col -1, endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity : message.type
      });
    }
    return found;
  }); 
});

// ruleSets for HTMLLint
var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};

var delay;

// Initialize CodeMirror editor2
var editor2 = CodeMirror.fromTextArea(document.getElementById("editor2"), {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',  
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop : true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
});
editor2.setValue(localStorage.getItem("code"));
$("#lesson2").click(function(e){

        var value2 = editor2.getValue();
		if (typeof(Storage) !== "undefined") {
            // Store
            localStorage.setItem("code2", value2);
            window.location.href = 'learn-html-css-javascript-03.htm';
        } else {
            //document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
            alert("Sorry, your browser does not support Web Storage")
        }
	});

// Live preview
editor2.on('change', function() {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});
function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor2.getValue());
  preview.close();
	
	validateCheckPoint();
	/*if (preg_match('%(<p[^>]*>.*?</p>)%i', $subject, $regs)) {
    $result = $regs[1];
} else {
    $result = "";
}*/
//console.log($("#editor2").text().match('<p[^>]*>.*?</p>'));

//$("div:match('/[^a-zA-Z0-9]/')")
}
function checkIfTagExists(src, tag, betweenTag) {
   if (!betweenTag) { betweenTag = "(.*?)";}
   
   var re = "<"+tag+"[^>]*>\s*"+betweenTag+"\s*<\\/"+tag+">";
   

		console.log(re);
		//console.log(re2);
		
    return new RegExp(re).test(src);
}

function validateCheckPoint() {
	
	var checkPoints = {
		8: "(style>|<style [^>]*>)\s*(.*?)\s*<\/style>",
		// 16: "(<style>|<style [^>]*>)\s*p\s*?{\s*?text[-]align\s*[:]\s*center[;]\s*}\s*<\/style>",
        16:"<style>\s?\n*.*(p.?.?.?.?.?.?{\n*.*text-align.?:.?center;)\n.*\n*?}\n.?<\/style>",
		22: "<body>.*\n.*<h1>(.*)<\/h1>.*\n.*<p>(.*)<\/p>.*\n.*<input(.*)>.*\n.*<input(.*)>.*\n.*<\/body>",
		//25: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
        25:"<style>\n.*?.(body.?.?.?.?.?.?.?{\n*.*text-align.?:.?center;)\n.*\n*?}\n.*.?<\/style>",
		28: "<head>\n.*?<title>(.*)<\/title>\n.*?<style>\n.*?.(body.?.?.?.?.?.?.?{\n*.*text-align.?:.?center;)\n.*\n*?}\n.*.?<\/style>\n.*?</head>",
		32: "<!DOCTYPE html>\n.*<head>\n.*?<title>(.*)<\/title>\n.*?<style>\n.*?.(body.?.?.?.?.?.?{\n*.*text-align.?:.?center;)\n.*\n*?}\n.*.?<\/style>\n*?.?.?.?<\/head>",
		37: "<!DOCTYPE html>\n.*<head>\n.*?<title>(.*)<\/title>\n.*?<style>\n.*?.(body.?.?.?.?.?.?{\n*.*text-align.?:.?center;\n*.*background.?:.?black;.*)\n.*}\n.*?<\/style>\n.*?.?.?.?.?<\/head>",
		39: "<!DOCTYPE html>\n.*<head>\n.*?<title>(.*)<\/title>\n.*?<style>\n.*?.(body.?.?.?.?.?.?{\n*.*text-align.?:.?center;\n*.*background\.?:.?black;\n.*color.?:.?white;)\n.*}\n.*?<\/style>\n.*?.?.?.?.?<\/head>",
		41: "<!DOCTYPE html>\n.*<head>\n.*?<title>(.*)<\/title>\n.*?<style>\n.*?.(body.?.?.?.?.?.?{\n*.*text-align.?:.?center;\n*.*background.?:.?black;\n.*color.?:.?white;\n.*font-family.?:.?helvetica;)\n.*}\n.*?<\/style>\n.*?.?.?.?.?<\/head>",
	}

	if( checkPoints[currentStep] !== undefined ) {
    // do something
		console.log('Run validation only on '+currentStep);
		var re = checkPoints[currentStep];
		var reResults = new RegExp(re, "gi").test(editor2.getValue());
	 		console.log('Run validation results '+reResults);
		if (reResults) {
			$("#slide"+currentStep+" .check-icon").html('/');
			$("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
			$("#slide"+currentStep+" .actions li").addClass('correct');
            $(".pagination .next").addClass('disabled');
			//$(".pagination .next").removeClass('disabled');
            console.log('Run validation using '+checkPoints[currentStep]);
		} else {
			$("#slide"+currentStep+" .check-icon").html('\\');
			$("#slide"+currentStep+" .actions li").removeClass('correct');
			$(".pagination .next").addClass('disabled');
		}
	}
	
	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		console.log('Unlocked skill '+unlockSkills[currentStep]);
		console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}	
}

//var activeTab = $(".tab-content .tab-content").find(".active");
//var id = activeTab.attr('id');
//console.log('Run validation only on '+currentStep);


setTimeout(updatePreview, 300);

var lessonProgressBar = document.getElementById('lessonProgressBar');
var maxNum = lessonProgressBar.dataset.valuemax;
var currentStep = 1;

// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
	if (currentStep = parseInt(url.split('#slide')[1])) {
		$('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]').tab('show') ;
		console.log('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]');
	} else {
		$('#submenu.nav-tabs a[href="#'+url.split('#')[1]+'"]').tab('show') ;
	}
	
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})



function updateProgressBar(e, step) {
	if (!step) {
		currentStep = parseInt($(e.target).data('step'));
	} else {
		currentStep = step;
	}
	
	   console.log("e: "+currentStep);
		if (!parseInt(currentStep)) {
		   currentStep = 1;
	   }
	   console.log("e2: "+currentStep);

  
  var percent = (parseInt(currentStep) / maxNum) * 100;

  
  $('.progress-bar').css({width: percent + '%'});
  $('.lessonProgressText').text("Step " + currentStep + " of " + maxNum);
  
  if (currentStep === 1) {
	  $(".pagination .next").html('Start Slideshow').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").addClass('disabled');
	 // $(".pagination .prev").click(function(e) {
	//	e.preventDefault();
	 // });
  }
  else if (currentStep === parseInt(maxNum)) {

	  $(".pagination .next").addClass('disabled');
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
	 // $(".pagination .next").click(function(e) {
	//	e.preventDefault();
	 // });	  
  }
  else {
	  $(".pagination .next").html('Next slide <i class="fa next-icon fa-step-forward">').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
  }
  //e.relatedTarget // previous tab

	validateCheckPoint();
}

$('a[data-toggle="tab"][role="tab-slides"]').on('shown.bs.tab', updateProgressBar );

function nextTab(elem) {
    var elemFind = $(elem).parent().next().find('a[data-toggle="tab"]')
	if (elemFind) {
		elemFind.click();
	}
}
function prevTab(elem) {
    var $elemFind = $(elem).parent().prev().find('a[data-toggle="tab"]')
	if ($elemFind) {
		$elemFind.click();
	}
}

$('.first').click(function(){

  $('#myWizard a:first').tab('show')

})


$(document).ready(function () {
	 console.log('echo1');
		updateProgressBar("lessonProgressBar", currentStep);
    console.log('echo2');


	//Initialize tooltips
    	$('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $("#lesson_tab .next").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        $active.parent().next().removeClass('disabled');
        nextTab($active);

    });
    $("#lesson_tab .prev").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        prevTab($active);

    });
	
	    //  Activate the Tooltips
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
		html: true,
		trigger: 'manual'
}).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function(){
	   $('#'+$(this).attr('id')+'.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
    });
});





