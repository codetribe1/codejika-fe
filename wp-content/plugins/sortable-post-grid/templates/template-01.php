<?php
$output .= '<div class="spg-thumb">';

if ( ! $img_overlay_hide ) {
	$output .= '<span class="spg-clip"><img src="' . $thumbnail . '"' . $image_alt_text . ' />';
	$output .= '<span class="spg-overlay" style="' . $overlay . '">';

	if ( '' !== $post_quickview ) {
		$n       = $post_count--;
		$output .= '<a href="#" class="spg-clip-overview post-id-' . $post->ID . $single_column_quick_view . '" data-post-id="' . $id . '" data-title="' . $post_quickview_title . '">
		<span class="spg-overlay-quick-view"></span></a>';
	} else {
		$overlay_link_img .= ' left:0;';
	}

	$output .= '<a href="' . get_permalink( $id ) . '" class="spg-clip-link">
	  <span class="spg-overlay-link" style="' . $overlay_link_img . '"></span></a>';
	$output .= '</span>';
	$output .= '</span>';
} else {
	$output .= '<span class="spg-clip"><img src="' . $thumbnail . '"' . $image_alt_text . ' /></span>';
}

$output .= '<div class="spg-image-overlay-data" style="font-size:' . $font_size . 'px;">';

if ( 'hide' !== $hide_date ) {
	$output .= '<div class="spg-post-date">';
	$output .= '<div class="spg-date-month">' . get_the_date( 'M' ) . '</div>';
	$output .= '<div class="spg-date-day">' . get_the_date( 'd' ) . '</div>';
	$output .= '<div class="spg-date-year">' . get_the_date( 'Y' ) . '</div>';
	$output .= '</div>';
}

$output .= '<div class="spg-post-title-section">';
$output .= '<span class="spg-entry-title"><h3 style="font-size:' . $font_size . 'px;">';

if ( 'yes' == $post_title_link ) {
	$output .= '<a href="' . get_permalink( $id ) . '" class="spg-clip-link">' . get_the_title( $id ) . '</a>';
} else {
	$output .= get_the_title( $id );
}

$output .= '</h3></span>';

if ( 'display' == $tax_meta ) {
	$output .= '<span class="spg-post-meta">' . spg_get_post_meta( $id ) . '</span>';
}

$output .= '</div>'; // .spg-post-title-section.
$output .= '</div>'; // .spg-image-overlay-data.
$output .= '</div>'; // .spg-thumb.

$excerpt_css = 'display:block;';
if ( 'enable' !== $excerpt_display ) {
	$excerpt_css = 'display:none;';
}

$output .= '<div class="spg-data" style="' . $excerpt_css . '">';
$output .= '<div class="spg-post-excerpt"><p class="spg-entry-summary" style="' . $excerpt_style . '">' . $excerpt . '</p></div>';
$output .= '</div>';
