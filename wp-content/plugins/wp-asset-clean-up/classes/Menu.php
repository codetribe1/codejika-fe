<?php
namespace WpAssetCleanUp;

/**
 * Class Menu
 * @package WpAssetCleanUp
 */
class Menu
{
	/**
	 * @var string
	 */
	static $capability = 'manage_options';

	/**
	 * @var string
	 */
	static $slug;

    /**
     * Menu constructor.
     */
    public function __construct()
    {
    	self::$slug = WPACU_PLUGIN_NAME . '_settings';

        add_action('admin_menu', array($this, 'activeMenu'));
    }

    /**
     *
     */
    public function activeMenu()
    {
	    if (! current_user_can(self::$capability)) {
		    return;
	    }

        add_menu_page(
            __('WP Asset CleanUp', WPACU_PLUGIN_NAME),
            __('WP Asset Clean Up', WPACU_PLUGIN_NAME),
	        self::$capability,
            self::$slug,
            array(new Settings, 'settingsPage'),
            'dashicons-filter'
        );

        add_submenu_page(
            self::$slug,
            __('Home Page', WPACU_PLUGIN_NAME),
            __('Home Page', WPACU_PLUGIN_NAME),
	        self::$capability,
            WPACU_PLUGIN_NAME.'_home_page',
            array(new HomePage, 'page')
        );

        add_submenu_page(
	        self::$slug,
            __('Bulk Unloads', WPACU_PLUGIN_NAME),
            __('Bulk Unloads', WPACU_PLUGIN_NAME),
	        self::$capability,
            WPACU_PLUGIN_NAME.'_bulk_unloads',
            array(new BulkUnloads, 'pageBulkUnloads')
        );

        // Get Help | Support Page
        add_submenu_page(
	        self::$slug,
            __('Get Help', WPACU_PLUGIN_NAME),
            __('Get Help', WPACU_PLUGIN_NAME),
	        self::$capability,
            WPACU_PLUGIN_NAME.'_get_help',
            array(new GetHelp, 'page')
        );

        // Rename first item from the menu which has the same title as the menu page
        $GLOBALS['submenu'][self::$slug][0][0] = esc_attr__('Settings', WPACU_PLUGIN_NAME);
    }
}
