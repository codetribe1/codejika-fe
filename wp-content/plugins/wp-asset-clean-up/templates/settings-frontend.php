<?php
/*
 * No direct access to this file
 */
if (! isset($data)) {
    exit;
}
?>

<form action="#wpacu_wrap_assets" method="post">
    <div id="wpacu_wrap_assets">
        <h1><?php echo apply_filters('wpacu_plugin_page_title', 'WP Asset CleanUp Lite'); ?></h1>
        <?php
        if ($data['is_updateable']) {
        ?>
            <p><small>* this area is shown only for the admin users and if "Manage in the Front-end?" was selected in the plugin's settings</small></p>
            <p><small>* 'admin-bar', 'wpassetcleanup-icheck-square-red' and 'wpassetcleanup-style' are not included as they are irrelevant since they are used by the plugin for this area</small></p>
            <?php
            if ($data['is_woocommerce_shop_page']) {
                ?>
                <p><strong><span style="color: #0f6cab;" class="dashicons dashicons-cart"></span> This a WooCommerce shop page ('product' type archive). Unloading assets will also take effect for the pagination/sorting pages (e.g. /2, /3, /?orderby=popularity etc.).</strong></p>
                <?php
            }

            if (isset($data['vars']['woo_url_not_match'])) {
                ?>
                <div class="wpacu_note wpacu_warning">
                    <p>Although this page is detected as the home page, its URL is not the same as the one from "General Settings" -&gt; "Site Address (URL)" and the WooCommerce plugin is not active anymore. This could be the "Shop" page that is no longer active.</p>
                </div>
                <?php
            }

	        do_action('wpacu_pro_frontend_before_asset_list');

            require_once 'meta-box-loaded.php';
        } else {
	        // Category, Tag, Search, 404, Author, Date pages (not supported by Lite version)

            if (\WpAssetCleanUp\Main::isWpDefaultSearchPage()) {
                echo '<span class="dashicons dashicons-search"></span> This is a <strong>WordPress Search Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (is_404()) {
                echo '<span class="dashicons dashicons-warning"></span> This is a <strong>404 (Not Found) Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (is_author()) {
	            echo '<span class="dashicons dashicons-admin-users"></span> This is an <strong>Author Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (is_category()) {
	            echo '<span class="dashicons dashicons-category"></span> This is a <strong>Category (Taxonomy) Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (function_exists('is_product_category') && is_product_category()) {
	            echo '<img src="'.WPACU_PLUGIN_URL . '/assets/icons/woocommerce-icon-logo.svg'.'" alt="" style="height: 40px !important; margin-top: -6px; margin-right: 5px;" align="middle" /> This is a <strong>WooCommerce Product Category (Taxonomy) Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (is_date()) {
	            echo '<span class="dashicons dashicons-calendar-alt"></span> This is a <strong>Date (Archive) Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (is_tag()) {
	            echo '<span class="dashicons dashicons-tag"></span> This is a <strong>Tag (Archive) Page</strong> and it is available in WP Asset CleanUp Pro.';
            } elseif (is_tax()) {
	            echo '<span class="dashicons dashicons-tag"></span> This is a <strong>Taxonomy Page</strong> and it is available in WP Asset CleanUp Pro.';
            }
        ?>
            <p><a class="go-pro-button" target="_blank" href="https://www.gabelivan.com/items/wp-asset-cleanup-pro/"><span class="dashicons dashicons-star-filled"></span>&nbsp; Get WP Asset CleanUp Pro</a> <em>* starting from $29</em></p>
        <?php
        }

        if ($data['is_updateable']) {
        ?>
        <div style="margin: 10px 0;">
            <button class="wpacu_update_btn"
                    type="submit"
                    name="submit"><span class="dashicons dashicons-update"></span> <?php esc_attr_e('UPDATE', WPACU_PLUGIN_NAME); ?></button>
        </div>

        <p align="right"><small>Powered by WP Asset CleanUp</small></p>
        <?php } ?>
    </div>

    <?php
    if ($data['is_updateable']) {
    ?>
    <?php wp_nonce_field($data['nonce_action'], $data['nonce_name']); ?>
    <input type="hidden" name="wpacu_update_asset_frontend" value="1" />
    <?php } ?>
</form>