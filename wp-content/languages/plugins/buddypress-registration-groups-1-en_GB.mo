��    !      $  /   ,      �     �  V   �     G     ^     u     �  
   �      �     �  ,   �     *     :  
   F     Q     a     o     |     �     �     �  %   �     �     �     �            Z   &     �     �     �  ?   �  @   �  8  &     _  V   f     �     �     �       
   *      5     V  ,   s     �     �  
   �     �     �     �     �     	     	     	  %   )	     O	     k	     s	     �	     �	  Z   �	     �	     �	     
  ?   
  @   [
                               !                            
                                                              	                                      Active Allows a new BuddyPress user to select groups to join during the registration process. Alphabetical (default) BP Registration Groups BP Registration Groups Settings BuddyPress Registration Groups Checkboxes Checkboxes Multiselect (default) Default: 0 (show all groups) Default: Check one or more areas of interest Default: Groups Description Display As Display Options Display Order Eric Johnson Most Forum Posts Most Forum Topics Newest No (default) No groups are available at this time. Number of Groups to Display Popular Radio Buttons Random Show Private Groups These options allow you to customize the list of groups on the new user registration form. Title Yes http://hardlyneutral.com/ https://wordpress.org/plugins/buddypress-registration-groups-1/ wordpress, multisite, buddypress, groups, registration, autojoin PO-Revision-Date: 2018-04-28 18:25:32+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: en_GB
Project-Id-Version: Plugins - BuddyPress Registration Groups - Stable (latest release)
 Active Allows a new BuddyPress user to select groups to join during the registration process. Alphabetical (default) BP Registration Groups BP Registration Groups Settings BuddyPress Registration Groups Checkboxes Checkboxes Multiselect (default) Default: 0 (show all groups) Default: Check one or more areas of interest Default: Groups Description Display As Display Options Display Order Eric Johnson Most Forum Posts Most Forum Topics Newest No (default) No groups are available at this time. Number of Groups to Display Popular Radio Buttons Random Show Private Groups These options allow you to customise the list of groups on the new user registration form. Title Yes http://hardlyneutral.com/ https://wordpress.org/plugins/buddypress-registration-groups-1/ wordpress, multisite, buddypress, groups, registration, autojoin 