<?php
/**
 * @package Boss Child Theme
 * The parent theme functions are located at /boss/buddyboss-inc/theme-functions.php
 * Add your own functions in this file.
 */

/**
 * Sets up theme defaults
 *
 * @since Boss Child Theme 1.0.0
 */
function boss_child_theme_setup()
{
  /**
   * Makes child theme available for translation.
   * Translations can be added into the /languages/ directory.
   * Read more at: http://www.buddyboss.com/tutorials/language-translations/
   */

  // Translate text from the PARENT theme.
  load_theme_textdomain( 'boss', get_stylesheet_directory() . '/languages' );

  // Translate text from the CHILD theme only.
  // Change 'boss' instances in all child theme files to 'boss_child_theme'.
  // load_theme_textdomain( 'boss_child_theme', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'boss_child_theme_setup' );

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since Boss Child Theme  1.0.0
 */
function boss_child_theme_scripts_styles()
{
  /**
   * Scripts and Styles loaded by the parent theme can be unloaded if needed
   * using wp_deregister_script or wp_deregister_style.
   *
   * See the WordPress Codex for more information about those functions:
   * http://codex.wordpress.org/Function_Reference/wp_deregister_script
   * http://codex.wordpress.org/Function_Reference/wp_deregister_style
   **/

  /*
   * Styles
   */

 wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/custom-bootstrap.css' );
 //wp_enqueue_style( 'now-ui', 'https://codejika.co.za/css/now-ui-kit.css?v=1.1.0' ); 
 wp_enqueue_style( 'flora-forms', site_url().'/css/floraforms.css' ); 
 wp_enqueue_style( 'boss-child-custom', get_stylesheet_directory_uri().'/css/custom.css', 1.2 );
  wp_enqueue_style( 'modal-custom', site_url().'/static/modal-custom.css' );
  wp_enqueue_script( 'script', site_url().'/js/core/popper.min.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'script2', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri().'/js/custom.js', array ( 'jquery' ), 1.31, true);
//wp_deregister_script( 'ck_editor_cdn' );
//wp_deregister_script( 'custom_script' );
}
add_action( 'wp_enqueue_scripts', 'boss_child_theme_scripts_styles', 9999 );

/**
 * Disable autop shortcode for our html pages
 */
function bfp_clean_formatter($content) {
    $new_content = '';
    $pattern_full = '{(\[clean\].*?\[/clean\])}is';
    $pattern_contents = '{\[clean\](.*?)\[/clean\]}is';
    $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

    foreach ($pieces as $piece) {
        if (preg_match($pattern_contents, $piece, $matches)) {
            $new_content .= $matches[1];
        } else {
            $new_content .= wptexturize(wpautop($piece));
        }
    }

    return $new_content;
}


remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'bfp_clean_formatter', 99);

/****************************** CUSTOM FUNCTIONS ******************************/

// Add your own custom functions here

//add_action( 'init', 'wpse206906_redirector' );

function wpse206906_redirector() {
    $url = $_SERVER['REQUEST_URI'];
    wp_redirect( 'https://staging.codejika.com/southafrica/' ) ;
          
    if ($url === "/") {
        //If IE go to /ie
        if ($record->country->name === "Mauritius") {
            wp_redirect( 'http://www.wpmultisite.com/ie' ) ;
          
            exit();
        }//If GB go to /uk
        if ($record->country->name === "Zambia") {
            wp_redirect( 'http://www.wpmultisite.com/uk' );
            exit();
        }
    }
}

function pll_link_att($atts, $content = null) {
    $default = array(
        'link' => "#",
        'style' => "",
        'class' => "",
    );
    $a = shortcode_atts($default, $atts);
    $content = do_shortcode($content);

    return '<a href="'.pll_home_url().$a['link'].'" style="'.$a['style'].'" class="'.$a['class'].'">'.$content.'</a>';
}
add_shortcode('plink', 'pll_link_att');


function pll_link() {
    return pll_home_url();
}

add_shortcode('homeurl', 'pll_link');

function domain_name() {
    return trim(preg_replace ("~^https://www\.~", "", pll_home_url()), "/");
}

add_shortcode('send-from-domain', 'domain_name');


function my_special_mail_tag( $output, $name, $html ) {
	if ( 'send-from-domain' == $name ) {
		$output = do_shortcode( "[$name]" );
  } elseif ('siteurl' == $name ) {
		$output = do_shortcode( "[$name]" );
  }
 
	return $output;
}
add_filter( 'wpcf7_special_mail_tags', 'my_special_mail_tag', 10, 3 );


function site_url_link() {
    return site_url()."/";
}

add_shortcode('siteurl', 'site_url_link');

add_filter('wpseo_enable_xml_sitemap_transient_caching', '__return_false');

add_filter('wpseo_canonical', 'swpseo_canonical_domain_replace');
function swpseo_canonical_domain_replace($url){

    $domain = 'codejika.com'; // this can be loaded from option table if you want admin to set it.
    $parsed = parse_url(home_url());
    $current_site_domain = $parsed['host'];
    return str_replace($current_site_domain, $domain, $url);

}

function debug_to_console( $data ) {
if ( is_array( $data ) )
 $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
 else
 $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
echo $output;
}

if ( !function_exists( 'bp_is_user' ) ) { 
    require_once '/bp-core/bp-core-template.php'; 
} 

if ( bp_current_component() ) {
 // debug_to_console("bp true");
} else {
 //   debug_to_console("bp false" );
}
 
    
if ( bp_is_activity_component() || bp_is_groups_component() || bp_is_group_forum() || bp_is_members_component() ||bp_is_page( BP_MEMBERS_SLUG ) ){ 

  add_filter("wpseo_robots", function() { return "noindex, nofollow"; });
}

function bpfr_hide_rss_feeds() {	
   remove_action( 'bp_actions', 'bp_activity_action_sitewide_feed' );
   remove_action( 'bp_actions', 'bp_activity_action_personal_feed' );
   remove_action( 'bp_actions', 'bp_activity_action_friends_feed' );
   remove_action( 'bp_actions', 'bp_activity_action_my_groups_feed' );
   remove_action( 'bp_actions', 'bp_activity_action_mentions_feed' );
   remove_action( 'bp_actions', 'bp_activity_action_favorites_feed' );
   remove_action( 'groups_action_group_feed', 'groups_action_group_feed' );    
}
add_action('init', 'bpfr_hide_rss_feeds');


//add_filter('pre_option_blogname', 'change_post_number_category3');
function change_post_number_category3( $pll_languages_list ) {
return get_option('default_category');
}



// Update CSS within in Admin
function admin_style() {
  wp_enqueue_style('admin-styles', get_stylesheet_directory_uri().'/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

    
  

add_filter( 'wp_kses_allowed_html', function ( $allowedposttags, $context ) {
        $allowedposttags['a']['data-href'] = 1;
    return $allowedposttags;
}, 10, 2 );


//add_filter('pre_transient_pll_languages_list', 'change_post_number_category21');

function change_post_number_category21( $polylang ) {

  //echo $polylang;
  return 'a:5:{i:0;a:27:{s:7:"term_id";i:117;s:4:"name";s:13:"International";s:4:"slug";s:2:"en";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:117;s:8:"taxonomy";s:8:"language";s:11:"description";s:2:"en";s:6:"parent";i:0;s:5:"count";i:14;s:10:"tl_term_id";i:118;s:19:"tl_term_taxonomy_id";i:118;s:8:"tl_count";i:4;s:6:"locale";R:9;s:6:"is_rtl";i:0;s:3:"w3c";s:2:"en";s:8:"facebook";N;s:8:"flag_url";s:0:"";s:4:"flag";s:0:"";s:8:"home_url";s:29:"https://staging.codejika.com/";s:10:"search_url";s:29:"https://staging.codejika.com/";s:4:"host";N;s:5:"mo_id";s:4:"1956";s:13:"page_on_front";s:4:"1556";s:14:"page_for_posts";s:3:"147";s:6:"filter";s:3:"raw";s:9:"flag_code";s:0:"";s:6:"active";b:1;}i:1;a:26:{s:7:"term_id";i:171;s:4:"name";s:7:"Namibia";s:4:"slug";s:7:"namibia";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:171;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_NA";s:6:"parent";i:0;s:5:"count";i:5;s:10:"tl_term_id";i:172;s:19:"tl_term_taxonomy_id";i:172;s:8:"tl_count";i:1;s:6:"locale";R:36;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-NA";s:8:"facebook";N;s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/na.png";s:4:"flag";s:930:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIZSURBVHjaYmRgmMjA8IOBgQWMgIxfDAz/GBj+gBGQLTJxqo+b2i8pV+N/YAmAAAAxAM7/AQAAh359Qw4L9wgH+OnsDPf5Avz8/v39/vz8/TI2GE9LHAnm3waRjP/y9P/7+/SysgIIaCobG+vvBK9VHOzfO+a4PHoo9vvPb05O5ugETTN1Dr6+hv/7D//79ev/7z+MtXXnKuMBAgiogYGR4R8jw29mxt9AEqhaRU04P1eD9/1Tjvz0/8/f/P/1+7+00uf6wjvijO6V1gABBNTw69MX1qYZHlxsX+8/FwoKV/e0F+RbOuP/+k3/f//6DzQ7LOJFjO+8l+sOn9zD8IQBIICAGv78/vPvyUteERGJzm4dwd/vOcrS/997CnTxf26hr13Ft5V5Ks7l3n55Q1FAERgQAAEEsuHv3782TtJxIdI829YyLl74/+cvkDOcXF9nRG75drJ/X+eXX19+A1377zcw2AACCKiBq7BIX4j5O2d71f/zV0AGM3N+rS1+Yqzccr3z1IPDvxh+AZX++fcHGHJAGwACiOXu9QTJBxcYu5r/f/kFUq1j/Koo/iTr4/aDSW9+vPkFNPjvrz8gDX+BCKgBIIBYpMwUmEKi/wtJ/Bdg+hXu/8pCb/Wzjfuu7RTgEOBm4/7z98+f/3+Abv77Hxg3f4FOAgggxq/g+AOiWysnb1P4Vj+nHBgUIP4vpOj+A4t9HgaAAAMAQO8VmfiJ/b4AAAAASUVORK5CYII=" title="Namibia" alt="Namibia" />";s:8:"home_url";s:37:"https://staging.codejika.com/namibia/";s:10:"search_url";s:37:"https://staging.codejika.com/namibia/";s:4:"host";N;s:5:"mo_id";s:4:"2247";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"na";}i:2;a:27:{s:7:"term_id";i:139;s:4:"name";s:12:"South Africa";s:4:"slug";s:11:"southafrica";s:10:"term_group";i:1;s:16:"term_taxonomy_id";i:139;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_ZA";s:6:"parent";i:0;s:5:"count";i:5;s:10:"tl_term_id";i:140;s:19:"tl_term_taxonomy_id";i:140;s:8:"tl_count";i:3;s:6:"locale";R:62;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-ZA";s:8:"facebook";s:5:"en_US";s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/za.png";s:4:"flag";s:932:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVHjaYmBoFWZo5ffeX37z4unP6up/GRgg6DcDw08Ghu8MDF8ZGD4zMHxkYHjPwPCWgQEggBgbDzP8Yp3C8O+PuJB0kJit6N4jLEePMfz9/f/PH4Y/f/7/BjHg5JdNmwACiMXoBoNZ+OfaM18ePnx56sXlcodE1W9fWI6d/v/gHkjdr9//f//6/+sXkM0oK/uPgQEgAAAxAM7/AQAAAMnBfiEn6oPXCuf4BP3993MxaCQGDxLe5v/19f/+/v/+/f/9/v/+/gEJCvGrqwIIpKGsrExH44WDLcPEB5xP/7C++/Xz738GDmaOv//+/P4LQSA3yfCIb5g0ESCAWIAa/vz5u3Hr19fvmcv8vnfe53j9j+vHn2+fP7/49ff3r7+/gKp//fsN1Mb+9yfDCwaAAAJp+Pv3j5sTk7P9v9kP2B78ZP3x5+uf//+4uIXZ/v4Dmf33zx+ghn9/eLhEGHgYAAIIpMHfnVFDl7HjJvelzyy/fn2dbFPPzcT95i73ty9///4F++If0Bf/eLhZZNTSAAKIZX4zg7oZS+5Jvjdf/zCw/i42Sdi9nHXz2vcvXj8DGgsOpH9AK4BIRYXz4sVdAAHE8s+LoeYiNxcTs4W8aJiU/455nGfOfeHmY5Dn4gS54w8wAv4B7fn7F0gCXfMPIIAYGTKBvmYQt7CuE5iQHfyKAegvhn9g9AvG+ANGDGCSDSDAAOBCLl8bj6ZDAAAAAElFTkSuQmCC" title="South Africa" alt="South Africa" />";s:8:"home_url";s:41:"https://staging.codejika.com/southafrica/";s:10:"search_url";s:41:"https://staging.codejika.com/southafrica/";s:4:"host";N;s:5:"mo_id";s:4:"2120";s:13:"page_on_front";i:2194;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"za";s:6:"active";b:1;}i:3;a:27:{s:7:"term_id";i:114;s:4:"name";s:6:"Zambia";s:4:"slug";s:6:"zambia";s:10:"term_group";i:2;s:16:"term_taxonomy_id";i:114;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_ZM";s:6:"parent";i:0;s:5:"count";i:72;s:10:"tl_term_id";i:115;s:19:"tl_term_taxonomy_id";i:115;s:8:"tl_count";i:7;s:6:"locale";R:89;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-ZM";s:8:"facebook";N;s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/zm.png";s:4:"flag";s:732:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" title="Zambia" alt="Zambia" />";s:8:"home_url";s:36:"https://staging.codejika.com/zambia/";s:10:"search_url";s:36:"https://staging.codejika.com/zambia/";s:4:"host";N;s:5:"mo_id";s:4:"1955";s:13:"page_on_front";i:2196;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"zm";s:6:"active";b:1;}i:4;a:27:{s:7:"term_id";i:167;s:4:"name";s:8:"Botswana";s:4:"slug";s:8:"botswana";s:10:"term_group";i:4;s:16:"term_taxonomy_id";i:167;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_BT";s:6:"parent";i:0;s:5:"count";i:1;s:10:"tl_term_id";i:168;s:19:"tl_term_taxonomy_id";i:168;s:8:"tl_count";i:1;s:6:"locale";R:116;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-BT";s:8:"facebook";N;s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/bw.png";s:4:"flag";s:660:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" title="Botswana" alt="Botswana" />";s:8:"home_url";s:38:"https://staging.codejika.com/botswana/";s:10:"search_url";s:38:"https://staging.codejika.com/botswana/";s:4:"host";N;s:5:"mo_id";s:4:"2246";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"bw";s:6:"active";b:0;}}';
}

//add_filter('pre_option_default_category', 'change_post_number_category2');

function change_post_number_category2( $polylang ) {

  //echo $polylang;
  return "141";
  //return 'a:14:{s:7:"browser";b:0;s:7:"rewrite";i:1;s:12:"hide_default";i:1;s:10:"force_lang";i:1;s:13:"redirect_lang";i:1;s:13:"media_support";i:1;s:9:"uninstall";i:1;s:4:"sync";a:1:{i:0;s:13:"_thumbnail_id";}s:10:"post_types";a:0:{}s:10:"taxonomies";a:0:{}s:7:"domains";a:3:{s:2:"en";s:29:"https://staging.codejika.com";s:6:"zambia";s:28:"https://namibia.codejika.com";s:11:"southafrica";s:28:"https://staging.codejika.com";}s:7:"version";s:5:"2.3.7";s:12:"default_lang";s:2:"en";s:9:"nav_menus";a:1:{s:10:"boss-child";a:6:{s:15:"left-panel-menu";a:5:{s:2:"en";i:0;s:7:"namibia";i:0;s:11:"southafrica";i:0;s:6:"zambia";i:0;s:8:"botswana";i:0;}s:11:"header-menu";a:5:{s:2:"en";i:90;s:7:"namibia";i:0;s:11:"southafrica";i:147;s:6:"zambia";i:90;s:8:"botswana";i:0;}s:17:"header-my-account";a:5:{s:2:"en";i:0;s:7:"namibia";i:0;s:11:"southafrica";i:0;s:6:"zambia";i:0;s:8:"botswana";i:0;}s:14:"secondary-menu";a:5:{s:2:"en";i:0;s:7:"namibia";i:0;s:11:"southafrica";i:0;s:6:"zambia";i:0;s:8:"botswana";i:0;}s:12:"profile-menu";a:5:{s:2:"en";i:0;s:7:"namibia";i:0;s:11:"southafrica";i:0;s:6:"zambia";i:0;s:8:"botswana";i:0;}s:10:"group-menu";a:5:{s:2:"en";i:0;s:7:"namibia";i:0;s:11:"southafrica";i:0;s:6:"zambia";i:0;s:8:"botswana";i:0;}}}}';
}


//add_filter('pre_transient_pll_languages_list', 'change_post_number_category');
function change_post_number_category(  ) {
//if ($_SERVER['HTTP_HOST'] == "staging3.codejika.com") {
   exit;
        return unserialize('a:5:{i:0;a:27:{s:7:"term_id";i:117;s:4:"name";s:13:"International";s:4:"slug";s:2:"en";s:10:"term_group";i:0;s:16:"term_taxonomy_id";i:117;s:8:"taxonomy";s:8:"language";s:11:"description";s:2:"en";s:6:"parent";i:0;s:5:"count";i:14;s:10:"tl_term_id";i:118;s:19:"tl_term_taxonomy_id";i:118;s:8:"tl_count";i:4;s:6:"locale";R:9;s:6:"is_rtl";i:0;s:3:"w3c";s:2:"en";s:8:"facebook";N;s:8:"flag_url";s:0:"";s:4:"flag";s:0:"";s:8:"home_url";s:32:"https://staging.codejika.com/en/";s:10:"search_url";s:32:"https://staging.codejika.com/en/";s:4:"host";N;s:5:"mo_id";s:4:"1956";s:13:"page_on_front";s:4:"1556";s:14:"page_for_posts";s:3:"147";s:6:"filter";s:3:"raw";s:9:"flag_code";s:0:"";s:6:"active";b:0;}i:1;a:27:{s:7:"term_id";i:139;s:4:"name";s:12:"South Africa";s:4:"slug";s:11:"southafrica";s:10:"term_group";i:1;s:16:"term_taxonomy_id";i:139;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_ZA";s:6:"parent";i:0;s:5:"count";i:5;s:10:"tl_term_id";i:140;s:19:"tl_term_taxonomy_id";i:140;s:8:"tl_count";i:3;s:6:"locale";R:36;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-ZA";s:8:"facebook";s:5:"en_US";s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/za.png";s:4:"flag";s:932:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVHjaYmBoFWZo5ffeX37z4unP6up/GRgg6DcDw08Ghu8MDF8ZGD4zMHxkYHjPwPCWgQEggBgbDzP8Yp3C8O+PuJB0kJit6N4jLEePMfz9/f/PH4Y/f/7/BjHg5JdNmwACiMXoBoNZ+OfaM18ePnx56sXlcodE1W9fWI6d/v/gHkjdr9//f//6/+sXkM0oK/uPgQEgAAAxAM7/AQAAAMnBfiEn6oPXCuf4BP3993MxaCQGDxLe5v/19f/+/v/+/f/9/v/+/gEJCvGrqwIIpKGsrExH44WDLcPEB5xP/7C++/Xz738GDmaOv//+/P4LQSA3yfCIb5g0ESCAWIAa/vz5u3Hr19fvmcv8vnfe53j9j+vHn2+fP7/49ff3r7+/gKp//fsN1Mb+9yfDCwaAAAJp+Pv3j5sTk7P9v9kP2B78ZP3x5+uf//+4uIXZ/v4Dmf33zx+ghn9/eLhEGHgYAAIIpMHfnVFDl7HjJvelzyy/fn2dbFPPzcT95i73ty9///4F++If0Bf/eLhZZNTSAAKIZX4zg7oZS+5Jvjdf/zCw/i42Sdi9nHXz2vcvXj8DGgsOpH9AK4BIRYXz4sVdAAHE8s+LoeYiNxcTs4W8aJiU/455nGfOfeHmY5Dn4gS54w8wAv4B7fn7F0gCXfMPIIAYGTKBvmYQt7CuE5iQHfyKAegvhn9g9AvG+ANGDGCSDSDAAOBCLl8bj6ZDAAAAAElFTkSuQmCC" title="South Africa" alt="South Africa" />";s:8:"home_url";s:29:"https://staging.codejika.com/";s:10:"search_url";s:29:"https://staging.codejika.com/";s:4:"host";N;s:5:"mo_id";s:4:"2120";s:13:"page_on_front";i:2194;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"za";s:6:"active";b:1;}i:2;a:27:{s:7:"term_id";i:114;s:4:"name";s:6:"Zambia";s:4:"slug";s:6:"zambia";s:10:"term_group";i:2;s:16:"term_taxonomy_id";i:114;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_ZM";s:6:"parent";i:0;s:5:"count";i:73;s:10:"tl_term_id";i:115;s:19:"tl_term_taxonomy_id";i:115;s:8:"tl_count";i:7;s:6:"locale";R:63;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-ZM";s:8:"facebook";N;s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/zm.png";s:4:"flag";s:732:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" title="Zambia" alt="Zambia" />";s:8:"home_url";s:36:"https://staging.codejika.com/zambia/";s:10:"search_url";s:36:"https://staging.codejika.com/zambia/";s:4:"host";N;s:5:"mo_id";s:4:"1955";s:13:"page_on_front";i:2196;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"zm";s:6:"active";b:0;}i:3;a:27:{s:7:"term_id";i:135;s:4:"name";s:7:"Namibia";s:4:"slug";s:7:"namibia";s:10:"term_group";i:3;s:16:"term_taxonomy_id";i:135;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_NA";s:6:"parent";i:0;s:5:"count";i:5;s:10:"tl_term_id";i:136;s:19:"tl_term_taxonomy_id";i:136;s:8:"tl_count";i:1;s:6:"locale";R:90;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-NA";s:8:"facebook";N;s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/na.png";s:4:"flag";s:930:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIZSURBVHjaYmRgmMjA8IOBgQWMgIxfDAz/GBj+gBGQLTJxqo+b2i8pV+N/YAmAAAAxAM7/AQAAh359Qw4L9wgH+OnsDPf5Avz8/v39/vz8/TI2GE9LHAnm3waRjP/y9P/7+/SysgIIaCobG+vvBK9VHOzfO+a4PHoo9vvPb05O5ugETTN1Dr6+hv/7D//79ev/7z+MtXXnKuMBAgiogYGR4R8jw29mxt9AEqhaRU04P1eD9/1Tjvz0/8/f/P/1+7+00uf6wjvijO6V1gABBNTw69MX1qYZHlxsX+8/FwoKV/e0F+RbOuP/+k3/f//6DzQ7LOJFjO+8l+sOn9zD8IQBIICAGv78/vPvyUteERGJzm4dwd/vOcrS/997CnTxf26hr13Ft5V5Ks7l3n55Q1FAERgQAAEEsuHv3782TtJxIdI829YyLl74/+cvkDOcXF9nRG75drJ/X+eXX19+A1377zcw2AACCKiBq7BIX4j5O2d71f/zV0AGM3N+rS1+Yqzccr3z1IPDvxh+AZX++fcHGHJAGwACiOXu9QTJBxcYu5r/f/kFUq1j/Koo/iTr4/aDSW9+vPkFNPjvrz8gDX+BCKgBIIBYpMwUmEKi/wtJ/Bdg+hXu/8pCb/Wzjfuu7RTgEOBm4/7z98+f/3+Abv77Hxg3f4FOAgggxq/g+AOiWysnb1P4Vj+nHBgUIP4vpOj+A4t9HgaAAAMAQO8VmfiJ/b4AAAAASUVORK5CYII=" title="Namibia" alt="Namibia" />";s:8:"home_url";s:37:"https://staging.codejika.com/namibia/";s:10:"search_url";s:37:"https://staging.codejika.com/namibia/";s:4:"host";N;s:5:"mo_id";s:4:"2119";s:13:"page_on_front";i:2198;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"na";s:6:"active";b:0;}i:4;a:27:{s:7:"term_id";i:167;s:4:"name";s:8:"Botswana";s:4:"slug";s:8:"botswana";s:10:"term_group";i:4;s:16:"term_taxonomy_id";i:167;s:8:"taxonomy";s:8:"language";s:11:"description";s:5:"en_BT";s:6:"parent";i:0;s:5:"count";i:0;s:10:"tl_term_id";i:168;s:19:"tl_term_taxonomy_id";i:168;s:8:"tl_count";i:1;s:6:"locale";R:117;s:6:"is_rtl";i:0;s:3:"w3c";s:5:"en-BT";s:8:"facebook";N;s:8:"flag_url";s:73:"https://staging.codejika.com/wp-content/plugins/polylang-pro/flags/bw.png";s:4:"flag";s:660:"<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" title="Botswana" alt="Botswana" />";s:8:"home_url";s:38:"https://staging.codejika.com/botswana/";s:10:"search_url";s:38:"https://staging.codejika.com/botswana/";s:4:"host";N;s:5:"mo_id";s:4:"2246";s:13:"page_on_front";b:0;s:14:"page_for_posts";b:0;s:6:"filter";s:3:"raw";s:9:"flag_code";s:2:"bw";s:6:"active";b:0;}}');

     //  } else {
       
       //return $_transient_pll_languages_list;
      // return "";
      // }
}

?>