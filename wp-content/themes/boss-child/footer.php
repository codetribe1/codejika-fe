<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Boss
 * @since Boss 1.0.0
 */
?>
    </div>
    <!-- #main .wrapper -->

    </div>
    <!-- #page -->

    </div>
    <!-- #inner-wrap -->

    </div>
    <!-- #main-wrap (Wrap For Mobile) -->

    <footer class="footer" data-background-color="black">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-24  powered-by">

                    <div class="row">

                        <div class="col-md-24 col-9 offset-3" style="">
                            <div class="c4c-logo pb-md-2 p-0">
                                <h4 class="pb-1">POWERED BY:</h4>
                                <a href="https://code4change.co.za/" target="_blank"><img src="<?php pll_home_url(); ?>/img/C4C-logo-white.png" style="max-width:80px;" >	</a>
                            </div>
                        </div>
                        <div class="col-md-24 col-9 oXffset-md-2 pt-md-0 pt-2" style="">
                            <ul>
                                <li><a href="https://code4change.co.za/" target="_blank">Learn More</a></li>
                                <li><a href="https://code4change.co.za/about" target="_blank">Team</a></li>
                                <li><a href="https://code4change.co.za/about/sponsors" target="_blank">Sponsors</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-14 col-24 " style="alXign-self: flex-end;">
                    <div class="row">
                        <div class="col-md-6 col-12 text-md-right text-left" style="alXign-self: flex-end;text-align: right;">

                            <div class="footer-menu text-md-left" style="display:inline-block; padding: 20px 0 0;">
                                <h4>CodeJika:</h4>
                                  <ul>
                                    <li class="hide">Sign Up</li>
                                    <li><a href="<?php echo pll_home_url();?>learn/nomzamos-website-01" target="_blank">Start Coding</a></li>
                                    <li><a href="<?php echo pll_home_url();?>club-signup">Start a Club</a></li>
                                    <li><a href="<?php echo pll_home_url();?>1-hour-website">1 Hour Website</a></li>
                                    <li><a href="<?php echo pll_home_url();?>the-5-minute-website">5 Minute Website</a></li>                                    
                                    <li><a href="<?php echo pll_home_url();?>coding-clubs">Coding Clubs</a></li>
                                    <li><a href="<?php echo pll_home_url();?>computer-science-week2018">Computer Science Week 2018</a></li>
                                  <ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 text-md-center text-left" style="alXign-self: flex-end;">
                            <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                                <h4>Education:</h4>
                                  <ul>
                                    <li><a href="<?php echo pll_home_url();?>news">News and Articles</a></li>
                                    <li><a href="<?php echo pll_home_url();?>coding-resources">CodeJIKA Resources</a></li>
                                    <li><a href="https://csedweek.org/" target="_blank">Hour of Code</a></li>
                                    
                                  <ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 " style="alXign-self: flex-end;">
                            <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                                <h4>Partners:</h4>
                                <ul>
                                    <li><a href="<?php echo pll_home_url();?>business">Corporates</a></li>
                                    <li><a href="<?php echo pll_home_url();?>schools">Schools</a></li>
                                    <li><a href="<?php echo pll_home_url();?>schools">Districts</a></li>
                                    <li><a href="<?php echo pll_home_url();?>media-corner">Media Corner</a></li>
                                    <li>Volunteers</li>
                                    <li>Launch in your Country</li>
                                    <ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 " style="alXign-self: flex-end;">
                            <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                                <h4>Country/Region:</h4>
                                <ul class="wpm-Xlanguage-switcher swiXtcher-list">
                                    <?php $lang_path = explode('/', rtrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/')); ?>
                                    <li class="item-language-en <?php if ($lang_path[1] == " " || $lang_path[1] == "/ " || $lang_path[1] == "/index.php ") { echo 'hide';} ?>">
                                        <a href="<?php pll_home_url(); ?>/" data-lang="en">
												<img class="hide" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjJGNDdGODZDODlDQTExRThBNEYxRERFNjc0OEQyNjk5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjJGNDdGODZEODlDQTExRThBNEYxRERFNjc0OEQyNjk5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MkY0N0Y4NkE4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MkY0N0Y4NkI4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AAAUHAAAFoAAABfUAAAZf/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wgARCAALABADAREAAhEBAxEB/8QAngABAQEAAAAAAAAAAAAAAAAABgUIAQEBAQAAAAAAAAAAAAAAAAABBAUQAAICAgMBAAAAAAAAAAAAAAABBAYCFAMTFQcRAAECBAUCBwAAAAAAAAAAAAQCAwEREgUAIhMUFTEWIVNzJCU1BhIBAAAAAAAAAAAAAAAAAAAAIBMAAgEDAwQDAAAAAAAAAAAAAREAITFBYXGB8FGhwRAgkf/aAAwDAQACEQMRAAAB3BXlJ0PDYT//2gAIAQEAAQUCvU2fhJpEiY+K+6m5876PG//aAAgBAgABBQJiGI//2gAIAQMAAQUCQxGR/9oACAECAgY/Ah//2gAIAQMCBj8CH//aAAgBAQEGPwJhhi5G28cYeBq0BkBiQOcixd3EskvuXG3H6SVW5EIJZWmEa41TywwcETcC7ogVysco50J9+hw65j6O4DLMi80hASVQi6uLufrGEsfm952/pbx6W/5vn66UU9v8D7z1p5JSngnbdu6HJlUdv8l0yfb8v8hy/manjKWP/9oACAEBAwE/IbhvebTdnCMKCHdmcZFvPVE386vCEVT/AM7vZNb9bU//2gAIAQIDAT8h+gf/2gAIAQMDAT8hEfHO8uzP/9oADAMBAAIRAxEAABBjH//aAAgBAQMBPxATZ6keXCUYEamC321NGkROi90y5wuP8IU9nHnJ3Ysv/9oACAECAwE/ECLuhGqCXEYa8vhRFRXx7c//2gAIAQMDAT8QMDIBJptbQjOYAYIAD7PTvHSVLRcum0a+pZXhUU//2Q==" alt="International">
													<span>Africa</span>
											</span>
					</li>
			<li class="item-language-southafrica <?php if ($lang_path[1] == 'southafrica') { echo 'hide';} ?>">
							<a href="<?php pll_home_url(); ?>/southafrica/"  data-lang="southafrica">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="South Africa">
													<span>South Africa</span>
											</a>
                                    </li>


			<li class="item-language-botswana <?php if ($lang_path[1] == 'botswana') { echo 'hide';} ?>">
							<a href="<?php pll_home_url(); ?>/botswana/"  data-lang="botswana">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Botswana">
													<span>Botswana</span>
											</a>
                                    </li>
                                    <li class="item-language-namibia <?php if ($lang_path[1] == 'namibia') { echo 'hide';} ?>">
                                        <a href="<?php pll_home_url(); ?>/namibia/" data-lang="namibia">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIZSURBVHjaYmRgmMjA8IOBgQWMgIxfDAz/GBj+gBGQLTJxqo+b2i8pV+N/YAmAAAAxAM7/AQAAh359Qw4L9wgH+OnsDPf5Avz8/v39/vz8/TI2GE9LHAnm3waRjP/y9P/7+/SysgIIaCobG+vvBK9VHOzfO+a4PHoo9vvPb05O5ugETTN1Dr6+hv/7D//79ev/7z+MtXXnKuMBAgiogYGR4R8jw29mxt9AEqhaRU04P1eD9/1Tjvz0/8/f/P/1+7+00uf6wjvijO6V1gABBNTw69MX1qYZHlxsX+8/FwoKV/e0F+RbOuP/+k3/f//6DzQ7LOJFjO+8l+sOn9zD8IQBIICAGv78/vPvyUteERGJzm4dwd/vOcrS/997CnTxf26hr13Ft5V5Ks7l3n55Q1FAERgQAAEEsuHv3782TtJxIdI829YyLl74/+cvkDOcXF9nRG75drJ/X+eXX19+A1377zcw2AACCKiBq7BIX4j5O2d71f/zV0AGM3N+rS1+Yqzccr3z1IPDvxh+AZX++fcHGHJAGwACiOXu9QTJBxcYu5r/f/kFUq1j/Koo/iTr4/aDSW9+vPkFNPjvrz8gDX+BCKgBIIBYpMwUmEKi/wtJ/Bdg+hXu/8pCb/Wzjfuu7RTgEOBm4/7z98+f/3+Abv77Hxg3f4FOAgggxq/g+AOiWysnb1P4Vj+nHBgUIP4vpOj+A4t9HgaAAAMAQO8VmfiJ/b4AAAAASUVORK5CYII=" alt="Namibia">
													<span>Namibia</span>
											</a>
                                    </li>
                                    <li class="item-language-zambia <?php if ($lang_path[1] == 'zambia') { echo 'hide';} ?>">

                                        <a href="<?php pll_home_url(); ?>/zambia/" data-lang="zambia">          
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" alt="Zambia">
													<span>Zambia</span>
											</a>
                                    </li>
                                    <li class="item-language-zambia <?php if ($lang_path[1] == 'zambia') { echo 'hide';} ?>">

                                        <a href="<?php pll_home_url(); ?>/mozambique/" data-lang="mozambique">          
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" alt="Mozambique">
													<span>Mozambique</span>
											</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6  col-24 offXset-md-2">
                    <p style="padding-top:20px; font-size: 12px">
                        CodeJIKA Movement: Eco-systems of vibrant, <strong>student-run coding clubs in secondary schools.</strong>
                        <p style="font-size: 12px">CodeJIKA launches online tools, hands-on roadshows & media campaigns to do this.</p>
                <p style="font-sXize: 12px">
                <a href="https://www.facebook.com/codejika/" class="mr-3" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                <a href="https://twitter.com/codejika" class="mr-3" target="_blank"><i class="fab fa-twitter-square fa-2x"></i></a>
                <a href="https://www.instagram.com/codejika/" class="mr-3" target="_blank"><i class="fab fa-instagram fa-2x"></i></a>
                <a href="http://www.linkedin.com/company/codejika/" class="mr-3" target="_blank"><i class="fab fa-linkedin fa-2x"></i></a>
                <a href="https://www.youtube.com/channel/UCkmsiNfz3SnmNCUJv4P5YUA" class="" target="_blank"><i class="fab fa-youtube fa-2x"></i></a>
                </p>
                </div>


            </div>

        </div>
        <div class="col-md-24  col-24 footer-inner" style="background-color: #292929; padding: 10px 20px; margin-top: 20px;">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="https://code4change.co.za/" target="_blank">Code for Change</a> | CodeJIKA
            </div>

            <?php get_template_part( 'template-parts/footer-links' ); ?>
        </div>
    </footer>

    </div>
    <!-- #right-panel-inner -->
    </div>
    <!-- #right-panel -->

    </div>
    <!-- #panels -->

    <?php wp_footer(); ?>
    <div class="modal " id="showModal" class="">
        <div class="modal-dialog">
            <div class="modal-content container" style="">



            </div>
        </div>
    </div>
    <script>
        $('.modal-show').click(function(){
               var dataURL = $(this).attr('data-href');
            $('.modal-content').load(dataURL,function(){
              $('.modal-content').append('<button type="button" class="close" data-dismiss="modal">&times;</button>')
            });
        
    });
    </script>
    </body>

    </html>