<?php
//$lang_path = $_SERVER['DOCUMENT_ROOT'].'/_custom-homepage/'.$get_lang;
$lang_home_url = 'https://'.$_SERVER['SERVER_NAME']; 
$lang_name = 'Zambia'; // Country Name
$lang_name_1 = 'Zambian';  // singular e.g 'Zambian'
$lang_name_2 = 'Zambians'; // plural e.g 'Zambians'
$lang_meta_title ='CodeJIKA.com - Coding Clubs in Zambian Schools - Have fun, make friends & build websites.';
$lang_meta_description = 'CodeJIKA - eco-systems of vibrant student-run coding clubs in secondary schools in Zambia';
$lang_meta_keywords = 'Coding, clubs, codejika, schools, africa, zambia';
$lang_h1_seo ='HELLO ZAMBIA... ';
$lang_h1_seo_typeit = 'LET\'S CODE';
$lang_testimonial_photo01 = '/img/testimonal-themba.jpg';
$lang_testimonial_photo02 = '/homepage/dev/testimonal-banji2.jpg';  //custom image
$lang_logos = array(
  '/homepage/dev/logo-intercontinental-lusaka.jpg', //custom logo
  '/img/logo-datatec.jpg',
  '/img/logo-tcs.jpg',
  '/img/logo-dell.jpg',
);
$lang_faqs = array(
  ['custom-faqs01', 'My custom FAQ title 01', 'Here is custom FAQ content 01'],
  ['custom-faqs02', 'My custom FAQ title 02', 'Here is custom FAQ content 02'],
);
?>