<?php
$home_url = 'https://'.$_SERVER['SERVER_NAME'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <meta name="google-site-verification" content="SxN62DDQ5qWO9qlDiyiqQBj72i1hGxOukG8sqIDTZVo" />
  <meta name="google-site-verification" content="Cv8eoG1XMqtBWl5jJasnizpoNzQgThpzSBwynXUId0c" />
  <link rel="icon" type="image/ico" href="assets/img/favicon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>CodeJIKA.com - Coding Clubs in Schools - Have fun, make friends & build websites.</title>
  <meta name="description" content="CodeJIKA - eco-systems of vibrant student-run coding clubs in secondary Schools in Africa">
  <meta name="keywords" content="Coding, clubs, codejika, schools, africa">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
   <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel='stylesheet' type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,500,700" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <!-- CSS Files -->
  <link href="<?php echo $home_url;?>/assets/css/custom-bootstrap.css" rel="stylesheet" />
  <link href="<?php echo $home_url;?>/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo $home_url;?>/css/floraforms.css">
  <link href="<?php echo $home_url;?>/css/custom.css?v=1.1.2" rel="stylesheet" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63106610-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-63106610-3');
</script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
  <style>
  .index-page .page-header {
    height: auto;
    min-height: 70vh;
    background-color: #f3f2ee;
    background-image: url(../img/homepage_footer.jpg);
    background-size: cover;
    background-position: top center;
    /* min-height: 700px; */
}
</style>  
</head>

<body class="index-page sidebXar-collapse">
<div class="notice hide"> INVITE: CodeJIKA Launch Gauteng - Thursday 23rd August - Melrose Arch Hotel --- <a href="https://www.eventbrite.com/e/49031050191" target="_blank">Sign up here</a></div>

  <div class="wrapper" style="">


    <div class="page-header clear-filter" style="">
      <!-- Navbar -->
      <div class="container">
        <nav class="navbar fixed-top navbar-expand-sm navbar-dark bg-dark">
          <div class="container ">


            <!-- Brand -->
            <a class="navbar-brand mr-auto" href="<?php echo $home_url;?><?php echo $lang_path;?>"><img src="<?php echo $home_url;?>/img/logo-jika_150x74.png" style="wiXdth: 120px;"> <span class="d-none d-sm-inline" style="font-size: 30px;
    font-family: 'rajdhani';
    font-weight: 500;
    padding-left: 36px;">CODE<strong>JIKA</strong>.com</span></a>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="nav-content">
              <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-block">
                  <a class="nav-link" href="<?php echo $home_url;?>/learn/nomzamos-website-01">Start Coding</a>
                </li>
                <li class="nav-item d-none d-sm-block">
                  <a class="nav-link" href="<?php echo pll_home_url();?>club-signup">Start a Coding Club</a>
                </li>
                <li class="nav-item"><a class="nav-menu-icon" id="menu-modal" data-toggle="modal" data-target="#menuModal" style="color: #999!important;    "><i class="fas fa-bars fxa-4x"></i></i></a></li>
              </ul>
            </div>
        </nav>
        </div>
        <!-- End Navbar -->
        <div class="page-header-image" data-parallax="true" style="backgroXund-image: url('img/homepage_header.jpg');">
        </div>
        <div class="container header-intro">
          <div class="row">
            <div class="col-md-24">
              <h1 id="h1-seo" style="margin-bottom: 0px;font-size: 5em;font-weight: 500;">READY... SET... <span id="type-here"></span></h1>
              <p class="subtitle">Learn to build websites with code!</p>
            </div>
              <div class="col-md-24 mt-3 ">
                <h4 class="mb-0 hide">The adventure begins<span id="countdown" class="hide">in "20" seconds</span></h4>
                <a class="btn btn-lg bg-primary white mr-0 mb-5" id="pbar_bgcolor" href="<?php echo $home_url;?>/learn/nomzamos-website-01">GO NOW</a><a class="hide btn btn-lg btn-secondary white ml-0 pl-2 pr-2 " id="pbar_pause" title="Cancel Countdown"><i class="far fa-pause-circle fa-2x"  ></i></a><br>
               
              </div>
            </div>
           
            </div>
          
          </div>



        </div>
      </div>
      <div class="clearfix"> </div>
      <div class="main">
       



        <div class="section section-about block-1 " style="">

                <div style="padding-top: 10px; text-align: center;" class="skew block-2">
          <div class="content">
          
           <div class="container text-center mt-3 mb-5 ">
            <div class="row justify-content-md-center">
              <div class="col-md-8 col-lg-11" style="    bacXkground-color: #fff;
    padXding: 0 0 20px 0">
              <img src="<?php echo $home_url;?>/img/testimonal-themba.jpg" style="display: block;width: 100%;border: 10px solid #ffcc00;">
                <h4 class="mt-1 mb-1 ml-4 mr-4" style="font-size:2.2em;">"I made a website for a business after 1 month."</h4>
                <p class="mt-0 pt-1 pb-4"  style="border-top: 1px solid #333; font-size: 1.1em; width: 60%;  margin: auto;">Themba, 15, Ivory Park</p>
              </div>
              <div class="col-md-8 col-lg-11 offset-md-2">
              <img src="<?php echo $home_url;?>/img/testimonal-lebo.jpg" style="display: block;width: 100%;border: 10px solid #ffcc00;" class="mb-1">
                <h4 class="mb-1 ml-4 mr-4 mt-2" style="font-size:2.2em;">"I made a website to tell my story - My message."</h4>
                <p class="mt-0 pt-1 pb-4"  style="border-top: 1px solid #333; font-size: 1.1em; width: 60%;
    margin: auto;">Lebo, 13, Diepsloot</p>
              </div>
            </div>
          </div>        

          <div class="container text-center mb-5">
            <div class="row justify-content-md-center">
              <div class="col-md-24 col-lg-8">

                <a class="btn btn-lg bg-primary white mb-8" style="font-size: 1.5em;" href="<?php echo $home_url;?>/learn/nomzamos-website-01">GO NOW</a><br>

         

              </div>
            </div>
          </div>
          
          </div>      </div>
          <div class="container text-center mb-5 ">
            <div class="row justify-content-md-center">
              <div class="col-md-6 col-offset-md-2">
            <a class="  slide-to" id="meXnu-modal" data-toggle="modal"     data-target="#whyModal" data-slide-to="0"><h4 style="text-transform: uppercase;">What is Coding?</h4></a>
                          </div> <div class="col-md-6">
          <a class="  slide-to" id="meXnu-modal" data-toggle="modal" data-target="#whyModal" data-slide-to="1"><h4 style="text-transform: uppercase;">Why Coding?</h4></a>
                                 </div>    <div class="col-md-6">
          <a class="  slide-to" id="meXnu-modal" data-toggle="modal" data-target="#whyModal" data-slide-to="2"><h4 style="text-transform: uppercase;">Be a coding hero.</h4></a>

              </div>
            </div>
          </div>
           
            <div class="container  mt-5 mb-5 pb-5" style="padding-bottom: 20px;">
          <h2 class="title" style="padding-top: 20px; position: relative;">How It Works</h2>
            <div class="row row-eq-height justify-content-md-center">
              <div class="col-md-7 offset-md-1 text-center" style="border-bottom: 1px solid #aaa;"><h2 class="mb-0">MAKE FRIENDS</h2>
              <p class="mt-1 pt-3 " style="border-top: 1px solid #aaa;font-size: 1.4em;">Get your besties together and start Project 1.</p>

                          </div> 
              <div class="col-md-7 offset-md-1 text-center" style="border-bottom: 1px solid #aaa;"><h2 class="mb-0">LEARN CODE</h2>
              <p class="mt-1 pt-3 " style="border-top: 1px solid #aaa;font-size: 1.4em;">One fun project at a time.<br>(It’ll take about 30 hours till Project 5)</p>

                          </div> 
            <div class="col-md-7 offset-md-1 text-center" style="border-bottom: 1px solid #aaa;"><h2 class="mb-0">MAKE WEBSITES</h2>
              <p class="mt-1 pt-3 " style="border-top: 1px solid #aaa;font-size: 1.4em;">Find someone awesome who needs one or make it for yourself.</p>

                          </div>               
            </div>
          </div>

<div class="container block-1">
<h2 class="title" style="padding-top: 20px; position: relative;">FAQ//: Freakin' Awesome Questions</h2>
              
    <div id="FAQs" class="accordion"  style=" ">
        <div class="card mb-0">
            <div class="card-header collapsed" data-toggle="collapse" href="#FAQ-what-CJ" >
                <a class="card-title">
                    What is CodeJika?
                </a>
            </div>
            <div id="FAQ-what-CJ" class="card-body collapse" data-parent="#FAQs" >
                <p>A vibrant eco-system of student-run coding clubs in secondary schools.</p>
<p>Operationally it is divided into 3 Pillars; 
<ol>
<li>Online (platform and partners), </li>
<li>Awareness (media & advocacy) and </li>
<li>Hands-on (In-school Clubs & Events)</li></ol></p>
                </p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" href="#FAQ-what-learn">
                <a class="card-title">What do I learn at CodeJIKA?</a>
            </div>
            <div id="FAQ-what-learn" class="card-body collapse" data-parent="#FAQs" >
                <p>You learn how to code, starting with “1 Hour Website” - How to code a simple website using HTML & CSS. Each consecutive project builds on this skill until you can customize and build beautiful business websites for SMEs from scratch – Like a PRO.  
                </p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-how-help">
                <a class="card-title">How can I help?</a>
            </div>
            <div id="FAQ-how-help" class="card-body collapse" data-parent="#FAQs" >
                <p>Run an event or assist schools or other organizations who are.</p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-run-CJ-event">
                <a class="card-title">How do I run a CodeJIKA event?</a>
            </div>
            <div id="FAQ-run-CJ-event" class="collapse" data-parent="#FAQs" >
                <div class="card-body">Easy. Go to the “How to run an event” page here to get started. There you’ll find the curriculum, suggested format, easy to use curriculum and tools to engage volunteers and even fundraise for the event.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-be-programmer">
                <a class="card-title">Do I have to be a programmer to run an event?</a>
            </div>
            <div id="FAQ-be-programmer" class="collapse" data-parent="#FAQs" >
                <div class="card-body">Anyone can run an event. It’s real fun and it’s about learning together and allowing others to have space to grow and someone who believes in them. You should have some desire to learn and work with the kids to find solutions though.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-for-primary-schools">
                <a class="card-title">Do you have a version for primary schools?
</a>
            </div>
            <div id="FAQ-for-primary-schools" class="collapse" data-parent="#FAQs" >
                <div class="card-body">There is no official version for primary schools, but from what we’ve heard the 5th, 6th and 7th graders love the program as well.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-not-secondary-school">
                <a class="card-title">Can I join if I’m not in secondary school?
</a>
            </div>
            <div id="FAQ-not-secondary-school" class="collapse" data-parent="#FAQs" >
                <div class="card-body">Of course. If you’ve already left school you can join the online CodeJIKA program and can help in organizing events and mentoring. The clubs and competitions are only for secondary school learners at this time.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-different-from-HOC">
                <a class="card-title">How is CodeJIKA different than Hour of Code?

</a>
            </div>
            <div id="FAQ-different-from-HOC" class="collapse" data-parent="#FAQs" >
                <div class="card-body">CodeJIKA teaches you how to become a Junior Frontend Web-developer, as fast as possible. It’s about building communites and creating revolutions within the existing educational system, stemming from the student base rather than a teacher-driven approach. 
CodeJIKA’s projected timeline is 3 years per group. You can go from never touching a PC to learning to hard-code professional websites in 6 months while still in school.

Our Belief: Our youth are engines of the new economy and are instinctively infused with a desire to build - Nothing can stop them! 
We’re here to provide tools. That’s all.
</div>
            </div>
        </div>
    </div>
</div>
          <div class="section " id="" style="backgXround-color: #777!important; coloXr:white;padding: 30px 0 50px 0;">        
        <div class="container" >
          <div class="row">
            <div class="col-md-24 "  >
            <h2 class="title" style="padding-top: 20px; position: relative;">Our Partners</h2>


            </div>
            <div class="col-md-24 text-Xmd-left text-center"  >
                <img src="<?php echo $home_url;?>/img/logo-google.jpg" class="mb-md-0 mb-3" style="max-width:120px; margin-right: 40px;" >	
                <img src="<?php echo $home_url;?>/img/logo-datatec.jpg" class="mb-md-0 mb-3" style="max-width:120px; margin-right: 40px;" >	
                <img src="<?php echo $home_url;?>/img/logo-pnet.jpg" class="mb-md-0 mb-3" style="max-width:120px; margin-right: 40px;" >	
                <img src="<?php echo $home_url;?>/img/logo-tcs.jpg" class="mb-md-0 mb-3" style="max-width:120px; margin-right: 40px;" >
                <img src="<?php echo $home_url;?>/img/logo-dell.jpg" class="mb-md-0 mb-3" style="max-width:120px; margin-right: 40px;" >	
            </div>
          </div>

        </div>        
        </div>                  
         
      </div>
        
        

        
        <div class="section section-signup hide" style="" id="start-learning">
                <div class="section section-about block-1 skew2" style="">
                </div>
          <div class="container">
            <div class="row">
            <div class="col-md-12 offset-md-2">
              <div class="card card-signup" style="margin-top:30px;    max-width: 500px;" data-background-color="orange">
                <form class="form" method="" action="">
                  <div class="header text-center">
                    <h4 class="title-up">Get started on your first lesson: <br>BUILD A WEBSITE</h4>
                  </div>
                  <div class="card-body" style="text-align:center;">
                    <img src="<?php echo $home_url;?>/img/homepage_lesson.jpg" style="width:100%; padding-bottom: 20px;">
                    <p style="padding: 0 20px 10px 20px; font-size: 1.3em">Nomzamo needs a website. Would you like to help build her a website?
                      <p style="padding: 0 20px 10px 20px; font-size: 1.3em">Take this intro lesson and learn the basics of how to code in HTML
                        <p>
                  </div>
                  <div class=" text-center mb-4">
                    <a href="<?php echo $home_url;?>/learn/nomzamos-website-01" class="btn btn-neutral btn-lg" style="font-size: 1.5em;">START LEARNING!</a>
                  </div>
                </form>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>

      <footer class="footer" data-background-color="black">
        <div class="container">
          <div class="row">
            <div class="col-md-3  col-24 powered-by">

              <div class="row" class="c4c-logo pb-2">
                <div class="col-md-24 col-9 offset-md-0 offset-4" style="">



                  <h4 class="pb-1">POWERED BY:</h4>
                    <a href="https://code4change.co.za/" target="_blank"><img src="<?php echo $home_url;?>/img/C4C-logo-white.png" style="max-width:80px; padding-bottom:10px" >	</a>
                 
                </div>               
                <div class="col-md-24 col-8 oXffset-md-2" style="">
                    <ul>
                      <li><a href="https://code4change.co.za/" target="_blank">Learn More</a></li>
                      <li><a href="https://code4change.co.za/about" target="_blank">Team</a></li>
                      <li><a href="https://code4change.co.za/about/sponsors" target="_blank">Sponsors</a></li>
                    </ul>                  
                </div>
              </div>
            </div>
            <div class="col-md-14" style="alXign-self: flex-end;">
              <div class="row">
                <div class="col-md-6 col-12" style="alXign-self: flex-end;texXt-align: right;">

                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                    <h4>CodeJika:</h4>
                    <ul>
                      <li>Sign Up</li>
                      <li><a href="<?php echo $home_url;?>/learn/nomzamos-website-01" target="_blank">Start Coding</a></li>
                      <li>Start a Club</li>
                 <li><a href="<?php echo pll_home_url();?>business">Partners</a></li>

                      <ul>
                  </div>
                </div>
                <div class="col-md-6 col-12" style="alXign-self: flex-end;text-align: center;">
                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                    <h4>Education:</h4>
                    <ul>
                      <li><a href="<?php echo $home_url;?>/learn/nomzamos-website-01" target="_blank">Start Coding</a></li>
                      <li><a href="<?php echo $home_url;?>/1-hour-website" target="_blank">Offline Courses</a></li>
                      <li><a href="<?php echo pll_home_url();?>coding-clubs">Coding Clubs</a></li>
                      <li><a href="<?php echo $home_url;?>/news">News and Articles</a></li>
                      <li>Events</li>
                      <ul>
                  </div>
                </div>
                <div class="col-md-6 col-12" style="alXign-self: flex-end;">
                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                    <h4>Partners:</h4>
                    <ul>
                      <li><a href="<?php echo pll_home_url();?>business">Corporates</a></li>
                      <li><a href="<?php echo pll_home_url();?>schools">Schools</a></li>
                      <li><a href="<?php echo pll_home_url();?>schools">Districts</a></li>
                      <li>Volunteers</li>
                      <li>Launch in your Country</li>
                      <ul>
                  </div>
                </div> 
                <div class="col-md-6 col-12" style="alXign-self: flex-end;">
                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                    <h4>Country/Region:</h4>
                    <ul class="wpXm-language-switcher switXcher-list">
			<li class="item-language-en <?php if ($lang_path == "" || $lang_path == "/" || $lang_path == "/index.php") { echo 'hide';} ?>" >
 							<a href="<?php echo $home_url;?>/"  data-lang="en">
												<img class="hide" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjJGNDdGODZDODlDQTExRThBNEYxRERFNjc0OEQyNjk5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjJGNDdGODZEODlDQTExRThBNEYxRERFNjc0OEQyNjk5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MkY0N0Y4NkE4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MkY0N0Y4NkI4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AAAUHAAAFoAAABfUAAAZf/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wgARCAALABADAREAAhEBAxEB/8QAngABAQEAAAAAAAAAAAAAAAAABgUIAQEBAQAAAAAAAAAAAAAAAAABBAUQAAICAgMBAAAAAAAAAAAAAAABBAYCFAMTFQcRAAECBAUCBwAAAAAAAAAAAAQCAwEREgUAIhMUFTEWIVNzJCU1BhIBAAAAAAAAAAAAAAAAAAAAIBMAAgEDAwQDAAAAAAAAAAAAAREAITFBYXGB8FGhwRAgkf/aAAwDAQACEQMRAAAB3BXlJ0PDYT//2gAIAQEAAQUCvU2fhJpEiY+K+6m5876PG//aAAgBAgABBQJiGI//2gAIAQMAAQUCQxGR/9oACAECAgY/Ah//2gAIAQMCBj8CH//aAAgBAQEGPwJhhi5G28cYeBq0BkBiQOcixd3EskvuXG3H6SVW5EIJZWmEa41TywwcETcC7ogVysco50J9+hw65j6O4DLMi80hASVQi6uLufrGEsfm952/pbx6W/5vn66UU9v8D7z1p5JSngnbdu6HJlUdv8l0yfb8v8hy/manjKWP/9oACAEBAwE/IbhvebTdnCMKCHdmcZFvPVE386vCEVT/AM7vZNb9bU//2gAIAQIDAT8h+gf/2gAIAQMDAT8hEfHO8uzP/9oADAMBAAIRAxEAABBjH//aAAgBAQMBPxATZ6keXCUYEamC321NGkROi90y5wuP8IU9nHnJ3Ysv/9oACAECAwE/ECLuhGqCXEYa8vhRFRXx7c//2gAIAQMDAT8QMDIBJptbQjOYAYIAD7PTvHSVLRcum0a+pZXhUU//2Q==" alt="International">
													<span>Africa</span>
											</span>
					</li>
			<li class="item-language-za <?php if ($lang_path == "/southafrica") { echo 'hide';} ?>" >
 							<a href="<?php echo $home_url;?>/southafrica/"  data-lang="southafrica">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVHjaYmBoFWZo5ffeX37z4unP6up/GRgg6DcDw08Ghu8MDF8ZGD4zMHxkYHjPwPCWgQEggBgbDzP8Yp3C8O+PuJB0kJit6N4jLEePMfz9/f/PH4Y/f/7/BjHg5JdNmwACiMXoBoNZ+OfaM18ePnx56sXlcodE1W9fWI6d/v/gHkjdr9//f//6/+sXkM0oK/uPgQEgAAAxAM7/AQAAAMnBfiEn6oPXCuf4BP3993MxaCQGDxLe5v/19f/+/v/+/f/9/v/+/gEJCvGrqwIIpKGsrExH44WDLcPEB5xP/7C++/Xz738GDmaOv//+/P4LQSA3yfCIb5g0ESCAWIAa/vz5u3Hr19fvmcv8vnfe53j9j+vHn2+fP7/49ff3r7+/gKp//fsN1Mb+9yfDCwaAAAJp+Pv3j5sTk7P9v9kP2B78ZP3x5+uf//+4uIXZ/v4Dmf33zx+ghn9/eLhEGHgYAAIIpMHfnVFDl7HjJvelzyy/fn2dbFPPzcT95i73ty9///4F++If0Bf/eLhZZNTSAAKIZX4zg7oZS+5Jvjdf/zCw/i42Sdi9nHXz2vcvXj8DGgsOpH9AK4BIRYXz4sVdAAHE8s+LoeYiNxcTs4W8aJiU/455nGfOfeHmY5Dn4gS54w8wAv4B7fn7F0gCXfMPIIAYGTKBvmYQt7CuE5iQHfyKAegvhn9g9AvG+ANGDGCSDSDAAOBCLl8bj6ZDAAAAAElFTkSuQmCC" alt="South Africa">
													<span>South Africa</span>
											</span>
					</li>
			<li class="item-language-zambia <?php if ($lang_path == '/zambia') { echo 'hide';} ?>">

							<a href="<?php echo $home_url;?>/zambia/"  data-lang="zambia">          
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" alt="Zambia">
													<span>Zambia</span>
											</a>
					</li>
			<li class="item-language-namibia <?php if ($lang_path == '/namibia') { echo 'hide';} ?>">
							<a href="<?php echo $home_url;?>/namibia/" data-lang="namibia">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIZSURBVHjaYmRgmMjA8IOBgQWMgIxfDAz/GBj+gBGQLTJxqo+b2i8pV+N/YAmAAAAxAM7/AQAAh359Qw4L9wgH+OnsDPf5Avz8/v39/vz8/TI2GE9LHAnm3waRjP/y9P/7+/SysgIIaCobG+vvBK9VHOzfO+a4PHoo9vvPb05O5ugETTN1Dr6+hv/7D//79ev/7z+MtXXnKuMBAgiogYGR4R8jw29mxt9AEqhaRU04P1eD9/1Tjvz0/8/f/P/1+7+00uf6wjvijO6V1gABBNTw69MX1qYZHlxsX+8/FwoKV/e0F+RbOuP/+k3/f//6DzQ7LOJFjO+8l+sOn9zD8IQBIICAGv78/vPvyUteERGJzm4dwd/vOcrS/997CnTxf26hr13Ft5V5Ks7l3n55Q1FAERgQAAEEsuHv3782TtJxIdI829YyLl74/+cvkDOcXF9nRG75drJ/X+eXX19+A1377zcw2AACCKiBq7BIX4j5O2d71f/zV0AGM3N+rS1+Yqzccr3z1IPDvxh+AZX++fcHGHJAGwACiOXu9QTJBxcYu5r/f/kFUq1j/Koo/iTr4/aDSW9+vPkFNPjvrz8gDX+BCKgBIIBYpMwUmEKi/wtJ/Bdg+hXu/8pCb/Wzjfuu7RTgEOBm4/7z98+f/3+Abv77Hxg3f4FOAgggxq/g+AOiWysnb1P4Vj+nHBgUIP4vpOj+A4t9HgaAAAMAQO8VmfiJ/b4AAAAASUVORK5CYII=" alt="Namibia">
													<span>Namibia</span>
											</a>
					</li>
			<li class="item-language-botswana <?php if ($lang_path == '/botswana') { echo 'hide';} ?>">
							<a href="<?php echo $home_url;?>/botswana/"  data-lang="botswana">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Botswana">
													<span>Botswana</span>
											</a>
					</li>
			<li class="item-language-mozambique <?php if ($lang_path == '/mozambique') { echo 'hide';} ?>">
							<a href="<?php echo $home_url;?>/mozambique/"  data-lang="mozambique">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Mozambique">
													<span>Mozambique</span>
											</a>
					</li>
                                   
	</ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-7  col-24 offsXet-md-1">
              <p style="padding-top:20px; font-siXze: 12px">
                CodeJIKA Movement: Eco-systems of vibrant, <strong>student-run coding clubs in secondary schools.</strong>
                <p style="font-sXize: 12px">CodeJIKA launches online tools, hands-on roadshows & media campaigns to do this.</p>
                <p style="font-sXize: 12px">
                <a href="https://www.facebook.com/codejika/" class="mr-2" target="_blank"><i class="fab fa-facebook-square fa-3x"></i></a>
                <a href="https://twitter.com/codejika" class="mr-2" target="_blank"><i class="fab fa-twitter-square fa-3x"></i></a>
                <a href="https://www.instagram.com/codejika/" class="mr-2" target="_blank"><i class="fab fa-instagram fa-3x"></i></a>
                <a href="http://www.linkedin.com/company/codejika/" class="mr-2" target="_blank"><i class="fab fa-linkedin fa-3x"></i></a>
                <a href="https://www.youtube.com/channel/UCkmsiNfz3SnmNCUJv4P5YUA" class="" target="_blank"><i class="fab fa-youtube-square fa-3x"></i></a>
                </p>
            </div>


          </div>

        </div>
        <div class="col-md-24" style="background-color: #292929;">
          <div class="copyright">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            <a href="https://code4change.co.za/" target="_blank">Code for Change</a> | CodeJIKA
          </div>
        </div>
      </footer>
    </div>
    <div class="modal fullscreen" id="menuModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <!-- Modal body -->
          <div class="container d-flex h-200 menu">
<div class="row">
<div class="col-md-24 text-center">
              <h2>Menu</h2>
              </div>
              <div class="col-md-12 d-sm-none">

                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">
                        <h4 style="user-select: text;">Start Coding</h4>
                        <p class="d-none d-sm-block"></p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/schools/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 d-sm-none">

                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">
                        <h4 style="user-select: text;">Start a Coding Club</h4>
                        <p class="d-none d-sm-block"></p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/schools/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">

                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">
                        <h4 style="user-select: text;">Schools</h4>
                        <p class="d-none d-sm-block">Join the network of forward-thinking schools and school management teams building an eco-system of fun, student-run coding clubs.</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/schools/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">

                        <h4 style="user-select: text;">Business</h4>
                        <p class="d-none d-sm-block">Coding in schools allows youth to dream of creating the digital products will allow ethical SMEs and manufacturers create efficiencies which will revitalize the economy.</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/business/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">

                        <h4 style="user-select: text;">CodeJIKA Resources</h4>
                        <p class="d-none d-sm-block">Find the tools to teach, advertise, code and rock this in your school.</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/coding-resources/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">

                        <h4 style="user-select: text;">Coding Clubs</h4>
                        <p class="d-none d-sm-block">Check out who's the top coding clubs in Southern Africa</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/coding-clubs/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            </div>
          
            </div>
            <div class="container d-sm-flex h-200 menu pt-2 d-none d-sm-none">
                   <div class="row justify-content-md-center w-100">
              <div class="col-md-8  text-center">
            <h4><a class="center slide-to" id="meXnu-modal" data-toggle="modal" data-target="#whyModal" data-slide-to="0" data-dismiss="modal">What is Coding?</a></h4>
                          </div> <div class="col-md-8  text-center">
          <h4><a class="  slide-to" id="meXnu-modal" data-toggle="modal" data-target="#whyModal" data-slide-to="1" data-dismiss="modal">Why Coding?</a><h4>
                                 </div>    <div class="col-md-8 text-center">
          <h4><a class="  slide-to" id="meXnu-modal" data-toggle="modal" data-target="#whyModal" data-slide-to="2" data-dismiss="modal">Be a coding hero</a><h4>

              </div>
            </div>

    
          </div>

        </div>
      </div>
    </div>
    <div class="modal " id="whyModal">
      <div class="modal-dialog" style="width: fit-content;">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-200" src="<?php echo $home_url;?>/img/what-is-coding.jpg" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-200" src="<?php echo $home_url;?>/img/why-coding-in-schools.jpg" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-200" src="<?php echo $home_url;?>/img/be-a-coding-hero.jpg" alt="Third slide">
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
          </a>
            </div>
          </div>


        </div>
      </div>
    </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="<?php echo $home_url;?>/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo $home_url;?>/js/core/popper.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: https://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?php echo $home_url;?>/js/plugins/bootstrap-switch.js"></script>

<script src="<?php echo $home_url;?>/js/homepage.js"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<!-- Load the script on the page. -->
<script src="https://cdn.jsdelivr.net/npm/typeit@5.10.1/dist/typeit.min.js" type="text/javascript"></script>


<script type="text/javascript">
  new TypeIt('#type-here', {
    strings: 'CODE.',
    speed: 150,
    startDelay: 1200,
    lifeLike: true,
    cursorChar: '<strong>_</strong>',
    autoStart: false,
    afterComplete: function (instance) {$('.ti-cursor').remove();}
  })

  function scrollToDownload() {

    if ($('.section-download').length != 0) {
      $("html, body").animate({
        scrollTop: $('.section-download').offset().top
      }, 1000);
    }
  }
  $(document).ready(function() {
    $(window).scroll(function() {
      if ($(window).scrollTop() > 100) {
        $('.navbar').addClass("sticky fixed-top");

      } else {
        $('.navbar').removeClass("sticky fixed-top");
      }
    });
  });
</script>


</html>