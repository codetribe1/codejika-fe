<?php
$home_url = 'https://'.$_SERVER['SERVER_NAME'];
Header('Vary: User-Agent');
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <meta name="google-site-verification" content="SxN62DDQ5qWO9qlDiyiqQBj72i1hGxOukG8sqIDTZVo" />
  <meta name="google-site-verification" content="Cv8eoG1XMqtBWl5jJasnizpoNzQgThpzSBwynXUId0c" />
  <link rel="icon" type="image/ico" href="<?php echo $home_url;?>/assets/img/favicon.ico">
      <link rel="canonical" href="<?php echo $home_url.$_SERVER['REQUEST_URI'];?>" />
  <link rel="alternate" media="handheld" href="<?php echo $home_url.$_SERVER['REQUEST_URI'];?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>CodeJIKA.com - Computer Science Education Week South Africa</title>
  <meta name="description" content="CodeJIKA - eco-systems of vibrant student-run coding clubs in secondary Schools in Africa">
  <meta name="keywords" content="CSE, Computer Science Education, Computer Science Education Week, Hour of Code, CodeJIKA, 1-Hour-Website, 60 minutes, coding, education, clubs, codejika, schools, africa, south africa">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
   <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel='stylesheet' type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,500,700" rel="stylesheet" type="text/css" />
  <!-- CSS Files -->
    <!--link href="<?php echo $home_url; ?>/css/merged-homepage.min.css?dfsdfs" rel="stylesheet" type="text/css" /-->

  <!--link href="<?php echo $home_url;?>/css/custom.css?v=1.1.2" rel="stylesheet" /-->
  <link rel="stylesheet" type="text/css" media="screen, print, projection" href="/css/css-compress.php" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63106610-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-63106610-3');
  console.log("<?php echo "re: ".$cj_cookies["devicetype"]." file end: ".$mobile." logic: ".$logic; ?>");
  </script>
  <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "http://schema.org/EducationalOrganization",
  "description": "An eco-systems of vibrant student-run coding clubs in Schools in Africa",
  "name": "CodeJIKA"
}

</script>
  <style>
 

</style>  
</head>

<body class="index-page sidebXar-collapse">
<div class="notice hide"> INVITE: CodeJIKA Launch Gauteng - Thursday 23rd August - Melrose Arch Hotel --- <a href="https://www.eventbrite.com/e/49031050191" target="_blank">Sign up here</a></div>

  <div class="wrapper" style="">


    <div class="page-header clear-filter" style="">
      <!-- Navbar -->
        <div class="container">
        <nav class="navbar fixed-top navbar-expand-sm navbar-dark bg-dark">
          <div class="container ">


            <!-- Brand -->
            <a class="navbar-brand mr-auto" href="<?php echo pll_home_url();?>"><img alt="jika-logo" src="<?php echo $home_url;?>/img/logo-jika_150x74.png" style="wiXdth: 120px;"> <span class="d-none d-lg-inline " style="font-size: 30px;
    font-family: 'rajdhani';
    font-weight: 500;
    padding-left: 36px;">CODE<strong>JIKA</strong>.com</span></a>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="nav-content">
              <ul class="navbar-nav">
                <li class="nav-item d-none d-md-block">
                  <a class="nav-link" href="<?php echo $home_url;?>/learn/nomzamos-website-01">Start Coding</a>
                </li>
                <li class="nav-item d-none d-md-block">
                  <a class="nav-link" href="<?php echo $home_url;?>/club-signup">Start a Coding Club</a>
                </li>
                <li class="nav-item"><a class="nav-menu-icon" id="menu-modal" data-toggle="modal" data-target="#menuModal" style=""><i class="icon-navicon fs-2"></i></a></li>
              </ul>
            </div>
        
            </div>
          </nav>
        </div>
      <!-- End Navbar -->
        <div class="page-header-image" data-parallax="true" style="backgroXund-image: url('img/homepage_header.jpg');">
        </div>
        
        <div class="container header-intro text-center">
          <div class="row">
            <div class="col-md-24">
              <h1 id="h1-seo" class="subtitle mt-4 mb-1" style="">YAAAYYYY!!!
              <span class="subtitle mt-1 mb-0" style="font-weight: 700;font-size: .5em;    display: block;">It’s Computer Science Education Week</span></h1>
              <p class=" mt-1 mb-5" style="font-weight: 300;">DEC 3-9, 2018</p>
<div class="mt-5" > 
                    <img alt="star-eye-emoji" style="" class="CSE-emoji mr-3" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAAHqsLF6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAL3UExURQAAALeSPxaFywN4w/iwJfngbPinI5GAYMzr/+60PDdfbVhjT2NOK+3t7fjCP0YsA0Kz/9OoUZZ8LUxaTt93F6HZ/5nW/wBUh+N7GYx3VCCl//ebH+B3F4jP/+miNJHT/6qaf+aUHiV9tD+ay/SxIwNdlE+y9PHx8fLBT8NzFMSsYP3vy3jJ/1aDj5p3N8evS/ncT/7w0QZXhnDG/z2Wzz8oA/ncWrm2ZTIeADuJs97HZPi1JwBLdXtuVP3tvvX19WRLF+uQHbbi/52gjfvgoABwurmkf990GvGqH9WLKxKW7vGPHPbXdeiCGuN5FmRUOvaWHgBLd996GXtjO+GACwBKdPj4+CyNyfvnipmhceLz/9XVrfiqI1Y+E2nC/wBLdfi8L++LGyRkffSSHeuFGvr6+hdXefWlIuqoRBGc+dLt/+eaKwBMdWLA/1m8/wBKdcnDuPrkha2FMpduK/zosg+P5apuIPeeIP39/vbTUrHg//nhc/jVRN96B/bTZPHDZ2ez5paIbz93j993F////+GBGfrie7GSNPnYiWdBAF49BVEzAAKD2ePe1uvq6uqbGDVndNasQZyQfHFPE4VtQfi4Kei0T9t1FvO6Niuj8yZ2p+OHELOok9SoOABKdEu2/7ivnsXo/+B6F1R3bfC+RfjcZQRxtq55Jjuw/+6xKiRVZm1gPNvZ1AR6x4tqJvitJDOt//ikIuujIN3Sv993F3FWHzxcXsCcTv3uxfTCNPbPX/XOdYHM/+yJG/GZHvL6/+n2//jIU/XMQHnJ/9LKvQFgnnBjSIdnNxRhjfehIeWHHOV+GcmnQVc3AA5Ob25GA5mQgXuRdOuqNMDm/4FYDlC5/0e1/zWt/z6x/xJmmQBJdeF5GLyJIO+5Vy2q/zau/8mTKwBNbt9vH0Wo6d93F+fn5qfc//fXfuaPFDciAJt+S/iyJgdnowVRfuypJwF4x9vw/+F5F9J6D3TH/8W8reWDDeB7FvKfH21eQlq8/wBhm9qzVHJIAK18crIAAAD7dFJOUwD//9////////+A////////////gP//z9////+P/////////////////////////////////////////4D//////////6r/MP///////+///7Zw//8w/////////////////////4///////1D//5T/////////////////////////IP/P//////////////////////9Q////////YP///6///////////9///////////0D/////////////////////n9/P////////////////////////67///////xcQ/2D/////////////7//P/////5/////f3pmR8wAAAAlwSFlzAAAOwwAADsMBx2+oZAAACkhJREFUSEudl39cU9cVwF+75CGzgFVKxVgBhYKtFUYLQ6P8UNsyYjBKVNa++pIlDoFiQcVoIVAH5ZfUWDSIZmupBgRmCEG0Q0gUdCshgvVHLIgyQFNDY6w4FZz7Y+fePClQ1vaz7+djuPfdc8477957fkiM4V54ePg7BLE2JSWxYMGCBQRx1GzOM5utBBHdqM5Q3/UlCK5lXW6EhUUQkiIYPNGDFo+yUejvKF7bwsP/dIcgZqWkbOpbsEBDGMzmlX+4adYTpCqzRd1630gIlvrn5roMSglCCwMLhdSMLK0M6xN3PmhPYNlYw4/S1+J58QsAXRCzbNkyrzdhLqd7gZXx2VUA3UAQZP5H94Vbn2TUyIU1Z32HwRb368GQ6YHswOlhgyNcA0HEkuctTh2IXcdEugEwMsChWgqP575VKCIl+C3AbLFEIJHmMLMfM6XBoLXZqCii2TH/7F46bBlwD7YNza+Co81Ne8HRZYkwt6cgOptu+MF2LkihCeIk8vzL+LRK8HzufXgj7M1688082OaXzTQ4ymedpVXCGnbFVLm8m9Y2EHrb9frWVjU7y7m10arQuoNEmFvYicDFgfmRgyNqqoFYQ/7F4hKxIzd3o/8ei0gH7+Vwn3uAP7bomBuLBw9m6pRODokOrhF5Skh1tn3f5R6/8CXLOIQfECYOCR+n1QlmO+YAXyIQ8GKZCWbJi+j31sftUVHt7Y/QTq39Hj0Z5eCZDX3a268yhLeQladPr2bWENcuA15eXpdnXdx2J+kLjxc8kpKSYpjF76uaR6n8LruplN7s8ZQNMZ8RRD+tqZrmQJOcnRbftKmmarODWTR+zcOHI10MbXlp8WmLmUmXXf4QfYkpQZsPZ0LPoOlDjy+y2RVz37DTNfQb8Ciagv0n9KStr40WKqqr632S1ey8rEs+Psl3FaoRO+3LMrrjnfO9cqUxMygoNDSrAiyEhoYGBWW2Xi+q99XKQCBWplVGWiwWF//UkpIRYGlJ6jGXPRbLYCOXRKdB6I0sUYhbXLl/xDo4FGBjhL+Ld6TbM1yKg09jQGwEG94u4wT27FbbSM7T4+BzdFrl1qJPRgU8OwJslJEHDjAMSAVGOFLXJ1tqa2ufOaW0UToZjznsp8yWSjgyow4wGjkCsQlFxo9pcO/pOew++drPoOmHn5xH7ejCfHwLPfk3nOMPvO+RThyOGkZXz8bSJhjaG1af9mPWMNfOnOlOKNvGXKhXC7R9L51OZ9YwlffuTXnNEYYODp450/w+s4johRvX7OX14eXgba9NS/Lw8IIbl4TvMaL/zUQPlF+A9L0FBfYXUPAC0zTYzyUxG5gL2dw8jd7UlL1Uw1xIoOpXBEEXm3GUA4+Df3ejoGlOZy+KdswsO3z+avss5s72Tt20Mj7ths+zzJ29Wkmj7WmYTz+L8gJwBy51/EqnOpTZqqp66Rly5ESsStjNXOOuxXCp89qYSdd24UMkoB9WdJvN5ps34WdvHvAyDNbDP/NRhQHFs4SKDk5JWdRZQ9vfmAuX9sjLh2iIAXB6S3QCH1wwaH3ps20nhSrh/aknszIyXqnZKhcK59Ovn41mkZDe3WUgQNda66vvJvtcOpWXEZBs9amv9xHSbdEoI2IBhcpa3Zq5s1UdkJXBbnF2Vre2lilU16Nt6Nq7cyjf6KLrrThwwAV2RdbOIAicxitXQECMfKC4ZSMjg4NhkdPzAwPZiwMDp58ICRt0+zqMy9JBrSB4pE00aLHscTn2oGQpiqySklR/Fwi1SKXWOBME+BA2WMA/YiOOix3rIHAslqWfcinDGhAY4lDcfLfdTufGCjzwdAsRsXTgAhN5bpY93nE4gWLi/mx57jyEpiP0YjmkTb17wis8kQc8JkL0Msr2aYdlrMD+IniBALmIcOeBhLrDc1Tgk6KtIhbJgYNgGOLJSJbyy8ILbx3PzT2+sHBfC1er40jHhOAQj6OjWMqWZyC4tzxx5WpJo2DsOsS2XiDTkVoWjiyKNHIkJmblKQMmseBp+Ms4Ev0PmX+UnFgpD6qAQCIR8ydZxgy4H+7pcYc6/EvJacD870I7gf5g80EcsDlrUOUxDA8nYIaHDSitrEGG+l/s6r2GZCbjA03S6W8rv2qPMiRQ2rJTBSvCf+NgRUFTAItKGI5q/4dmyo4pmiWMwgT6/VLeAb7ZW7rib5OyLbtt82ngczujMYHVxVMw32oWLZo3b31lcXFx97vh4e/egUHl+nnzFh1Kv3wPcVnzAaMylv4/Vm7+cDybK+nlK1YspzVnvxjP55W/RklllPff1CR2/QtK7ngS/Tqfz2663fQK7df77URe70r8/u84h64NTqx6fRI2F9Mnk0uzb9/OLk0+aU8/+9KPaa6KQeltCU37xdShXDSWukp6IdJPS7udvSl5P625umE8KXXpfvQ1plb8tZPujumtG0vvwZo5N5pux8dDOsy+Mcc+6+rnY7iKtDu/cmgTxCOhUL61s/ubcdStP5IFLQIYSMvLOnLwm3+Opa67ZrtcLn/k0D9gioqyqo52X53A3N/npWHyLq5nHo1Cb3+oslr5B7CBoSgZXNct3Y8x0HLQbXNvomH3YpQK2RW/PYQX5gbTtB0vPH5Mb1EMw838GOm/zTfotCzffBq2PuUQjXybHwc9DHAxi52Rwa44giczaufL5ds76UPojOi+aJaWNLS/DQbc9ai9AwPNH9npqUKV1QewqlQqnznqU+yMvIysgEs+KqFQBT5boQjMoIM/aqajfW0sShaFegp3vQwZiK6hO4+qrIq7ZY2urmpn50uXLjkHgIEMdlaA85zkOTAtKyurvquwCrd20lOxAUdTgj1g+UbXK6yK+urG1kwoBDtDdyKgjUKfcKqlpQXPgzIzWxur68ETBRjQkjIxMnAL9oBi2Xz7whCRtfnnoXxA81URWoH3EHaxAjVj0I0Fnc+vjcRifeCAluToUesxYOKhhkjZGLIKakbq8l2r3hscBQkzQ+C5VbuWp4LQqhBXJRe+wMDHqfWwHidd0fTBcliEOuPi7x8RsW7jDpzHMTs2rouI8Pd3QQ2fxVI+OF2khKZaJj6M9IkcE0+mo2xK1/dClpYf+1kDqU4hka6gTxolJiZb3uJLGAtu5d5xu4ETcd7e5efOnbNgdQuMyr13xUXCSqSTk9t7Dn2BFDdfCHdsgcVtrHXzZDzYc+6cN7AfgQZgzOGBp1vtU/0fGkuwwOPgnSzt2F3yU5/gubujVMTVQuMpGasPtR4qDzihtYlK93WcSJ3cQOqJjhOgzqJ0Rg7PNLG+9IATqL7ZuK7P7ysq/O7BeAPl+wuL9j2PnEfqEmkPozaGgSEpTyAzkpSWxRW1nF94ofDChf9gLsBw4fkWERe0SR2URunQ5N3vQA9fjDtoZMRmU4pcXeESt7i6ipRcXHVRYRWI+T2Tq2MOxIINVIfBCkmNQpK4KgskYmmsI4n8BDmPTFIxKtQcDkeGgQFUbZ5YasL/TftFHJg908SX6vViQK+X8k0zZ//sm/8vCOK/RaQO/rsFipEAAAAASUVORK5CYII="> 							
                    <img alt="star-eye-emoji" style="" class="CSE-emoji mr-3" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAAHqsLF6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAL3UExURQAAALeSPxaFywN4w/iwJfngbPinI5GAYMzr/+60PDdfbVhjT2NOK+3t7fjCP0YsA0Kz/9OoUZZ8LUxaTt93F6HZ/5nW/wBUh+N7GYx3VCCl//ebH+B3F4jP/+miNJHT/6qaf+aUHiV9tD+ay/SxIwNdlE+y9PHx8fLBT8NzFMSsYP3vy3jJ/1aDj5p3N8evS/ncT/7w0QZXhnDG/z2Wzz8oA/ncWrm2ZTIeADuJs97HZPi1JwBLdXtuVP3tvvX19WRLF+uQHbbi/52gjfvgoABwurmkf990GvGqH9WLKxKW7vGPHPbXdeiCGuN5FmRUOvaWHgBLd996GXtjO+GACwBKdPj4+CyNyfvnipmhceLz/9XVrfiqI1Y+E2nC/wBLdfi8L++LGyRkffSSHeuFGvr6+hdXefWlIuqoRBGc+dLt/+eaKwBMdWLA/1m8/wBKdcnDuPrkha2FMpduK/zosg+P5apuIPeeIP39/vbTUrHg//nhc/jVRN96B/bTZPHDZ2ez5paIbz93j993F////+GBGfrie7GSNPnYiWdBAF49BVEzAAKD2ePe1uvq6uqbGDVndNasQZyQfHFPE4VtQfi4Kei0T9t1FvO6Niuj8yZ2p+OHELOok9SoOABKdEu2/7ivnsXo/+B6F1R3bfC+RfjcZQRxtq55Jjuw/+6xKiRVZm1gPNvZ1AR6x4tqJvitJDOt//ikIuujIN3Sv993F3FWHzxcXsCcTv3uxfTCNPbPX/XOdYHM/+yJG/GZHvL6/+n2//jIU/XMQHnJ/9LKvQFgnnBjSIdnNxRhjfehIeWHHOV+GcmnQVc3AA5Ob25GA5mQgXuRdOuqNMDm/4FYDlC5/0e1/zWt/z6x/xJmmQBJdeF5GLyJIO+5Vy2q/zau/8mTKwBNbt9vH0Wo6d93F+fn5qfc//fXfuaPFDciAJt+S/iyJgdnowVRfuypJwF4x9vw/+F5F9J6D3TH/8W8reWDDeB7FvKfH21eQlq8/wBhm9qzVHJIAK18crIAAAD7dFJOUwD//9////////+A////////////gP//z9////+P/////////////////////////////////////////4D//////////6r/MP///////+///7Zw//8w/////////////////////4///////1D//5T/////////////////////////IP/P//////////////////////9Q////////YP///6///////////9///////////0D/////////////////////n9/P////////////////////////67///////xcQ/2D/////////////7//P/////5/////f3pmR8wAAAAlwSFlzAAAOwwAADsMBx2+oZAAACkhJREFUSEudl39cU9cVwF+75CGzgFVKxVgBhYKtFUYLQ6P8UNsyYjBKVNa++pIlDoFiQcVoIVAH5ZfUWDSIZmupBgRmCEG0Q0gUdCshgvVHLIgyQFNDY6w4FZz7Y+fePClQ1vaz7+djuPfdc8477957fkiM4V54ePg7BLE2JSWxYMGCBQRx1GzOM5utBBHdqM5Q3/UlCK5lXW6EhUUQkiIYPNGDFo+yUejvKF7bwsP/dIcgZqWkbOpbsEBDGMzmlX+4adYTpCqzRd1630gIlvrn5roMSglCCwMLhdSMLK0M6xN3PmhPYNlYw4/S1+J58QsAXRCzbNkyrzdhLqd7gZXx2VUA3UAQZP5H94Vbn2TUyIU1Z32HwRb368GQ6YHswOlhgyNcA0HEkuctTh2IXcdEugEwMsChWgqP575VKCIl+C3AbLFEIJHmMLMfM6XBoLXZqCii2TH/7F46bBlwD7YNza+Co81Ne8HRZYkwt6cgOptu+MF2LkihCeIk8vzL+LRK8HzufXgj7M1688082OaXzTQ4ymedpVXCGnbFVLm8m9Y2EHrb9frWVjU7y7m10arQuoNEmFvYicDFgfmRgyNqqoFYQ/7F4hKxIzd3o/8ei0gH7+Vwn3uAP7bomBuLBw9m6pRODokOrhF5Skh1tn3f5R6/8CXLOIQfECYOCR+n1QlmO+YAXyIQ8GKZCWbJi+j31sftUVHt7Y/QTq39Hj0Z5eCZDX3a268yhLeQladPr2bWENcuA15eXpdnXdx2J+kLjxc8kpKSYpjF76uaR6n8LruplN7s8ZQNMZ8RRD+tqZrmQJOcnRbftKmmarODWTR+zcOHI10MbXlp8WmLmUmXXf4QfYkpQZsPZ0LPoOlDjy+y2RVz37DTNfQb8Ciagv0n9KStr40WKqqr632S1ey8rEs+Psl3FaoRO+3LMrrjnfO9cqUxMygoNDSrAiyEhoYGBWW2Xi+q99XKQCBWplVGWiwWF//UkpIRYGlJ6jGXPRbLYCOXRKdB6I0sUYhbXLl/xDo4FGBjhL+Ld6TbM1yKg09jQGwEG94u4wT27FbbSM7T4+BzdFrl1qJPRgU8OwJslJEHDjAMSAVGOFLXJ1tqa2ufOaW0UToZjznsp8yWSjgyow4wGjkCsQlFxo9pcO/pOew++drPoOmHn5xH7ejCfHwLPfk3nOMPvO+RThyOGkZXz8bSJhjaG1af9mPWMNfOnOlOKNvGXKhXC7R9L51OZ9YwlffuTXnNEYYODp450/w+s4johRvX7OX14eXgba9NS/Lw8IIbl4TvMaL/zUQPlF+A9L0FBfYXUPAC0zTYzyUxG5gL2dw8jd7UlL1Uw1xIoOpXBEEXm3GUA4+Df3ejoGlOZy+KdswsO3z+avss5s72Tt20Mj7ths+zzJ29Wkmj7WmYTz+L8gJwBy51/EqnOpTZqqp66Rly5ESsStjNXOOuxXCp89qYSdd24UMkoB9WdJvN5ps34WdvHvAyDNbDP/NRhQHFs4SKDk5JWdRZQ9vfmAuX9sjLh2iIAXB6S3QCH1wwaH3ps20nhSrh/aknszIyXqnZKhcK59Ovn41mkZDe3WUgQNda66vvJvtcOpWXEZBs9amv9xHSbdEoI2IBhcpa3Zq5s1UdkJXBbnF2Vre2lilU16Nt6Nq7cyjf6KLrrThwwAV2RdbOIAicxitXQECMfKC4ZSMjg4NhkdPzAwPZiwMDp58ICRt0+zqMy9JBrSB4pE00aLHscTn2oGQpiqySklR/Fwi1SKXWOBME+BA2WMA/YiOOix3rIHAslqWfcinDGhAY4lDcfLfdTufGCjzwdAsRsXTgAhN5bpY93nE4gWLi/mx57jyEpiP0YjmkTb17wis8kQc8JkL0Msr2aYdlrMD+IniBALmIcOeBhLrDc1Tgk6KtIhbJgYNgGOLJSJbyy8ILbx3PzT2+sHBfC1er40jHhOAQj6OjWMqWZyC4tzxx5WpJo2DsOsS2XiDTkVoWjiyKNHIkJmblKQMmseBp+Ms4Ev0PmX+UnFgpD6qAQCIR8ydZxgy4H+7pcYc6/EvJacD870I7gf5g80EcsDlrUOUxDA8nYIaHDSitrEGG+l/s6r2GZCbjA03S6W8rv2qPMiRQ2rJTBSvCf+NgRUFTAItKGI5q/4dmyo4pmiWMwgT6/VLeAb7ZW7rib5OyLbtt82ngczujMYHVxVMw32oWLZo3b31lcXFx97vh4e/egUHl+nnzFh1Kv3wPcVnzAaMylv4/Vm7+cDybK+nlK1YspzVnvxjP55W/RklllPff1CR2/QtK7ngS/Tqfz2663fQK7df77URe70r8/u84h64NTqx6fRI2F9Mnk0uzb9/OLk0+aU8/+9KPaa6KQeltCU37xdShXDSWukp6IdJPS7udvSl5P625umE8KXXpfvQ1plb8tZPujumtG0vvwZo5N5pux8dDOsy+Mcc+6+rnY7iKtDu/cmgTxCOhUL61s/ubcdStP5IFLQIYSMvLOnLwm3+Opa67ZrtcLn/k0D9gioqyqo52X53A3N/npWHyLq5nHo1Cb3+oslr5B7CBoSgZXNct3Y8x0HLQbXNvomH3YpQK2RW/PYQX5gbTtB0vPH5Mb1EMw838GOm/zTfotCzffBq2PuUQjXybHwc9DHAxi52Rwa44giczaufL5ds76UPojOi+aJaWNLS/DQbc9ai9AwPNH9npqUKV1QewqlQqnznqU+yMvIysgEs+KqFQBT5boQjMoIM/aqajfW0sShaFegp3vQwZiK6hO4+qrIq7ZY2urmpn50uXLjkHgIEMdlaA85zkOTAtKyurvquwCrd20lOxAUdTgj1g+UbXK6yK+urG1kwoBDtDdyKgjUKfcKqlpQXPgzIzWxur68ETBRjQkjIxMnAL9oBi2Xz7whCRtfnnoXxA81URWoH3EHaxAjVj0I0Fnc+vjcRifeCAluToUesxYOKhhkjZGLIKakbq8l2r3hscBQkzQ+C5VbuWp4LQqhBXJRe+wMDHqfWwHidd0fTBcliEOuPi7x8RsW7jDpzHMTs2rouI8Pd3QQ2fxVI+OF2khKZaJj6M9IkcE0+mo2xK1/dClpYf+1kDqU4hka6gTxolJiZb3uJLGAtu5d5xu4ETcd7e5efOnbNgdQuMyr13xUXCSqSTk9t7Dn2BFDdfCHdsgcVtrHXzZDzYc+6cN7AfgQZgzOGBp1vtU/0fGkuwwOPgnSzt2F3yU5/gubujVMTVQuMpGasPtR4qDzihtYlK93WcSJ3cQOqJjhOgzqJ0Rg7PNLG+9IATqL7ZuK7P7ysq/O7BeAPl+wuL9j2PnEfqEmkPozaGgSEpTyAzkpSWxRW1nF94ofDChf9gLsBw4fkWERe0SR2URunQ5N3vQA9fjDtoZMRmU4pcXeESt7i6ipRcXHVRYRWI+T2Tq2MOxIINVIfBCkmNQpK4KgskYmmsI4n8BDmPTFIxKtQcDkeGgQFUbZ5YasL/TftFHJg908SX6vViQK+X8k0zZ//sm/8vCOK/RaQO/rsFipEAAAAASUVORK5CYII="> 							
                    <img alt="star-eye-emoji" style="" class="CSE-emoji" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAAHqsLF6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAL3UExURQAAALeSPxaFywN4w/iwJfngbPinI5GAYMzr/+60PDdfbVhjT2NOK+3t7fjCP0YsA0Kz/9OoUZZ8LUxaTt93F6HZ/5nW/wBUh+N7GYx3VCCl//ebH+B3F4jP/+miNJHT/6qaf+aUHiV9tD+ay/SxIwNdlE+y9PHx8fLBT8NzFMSsYP3vy3jJ/1aDj5p3N8evS/ncT/7w0QZXhnDG/z2Wzz8oA/ncWrm2ZTIeADuJs97HZPi1JwBLdXtuVP3tvvX19WRLF+uQHbbi/52gjfvgoABwurmkf990GvGqH9WLKxKW7vGPHPbXdeiCGuN5FmRUOvaWHgBLd996GXtjO+GACwBKdPj4+CyNyfvnipmhceLz/9XVrfiqI1Y+E2nC/wBLdfi8L++LGyRkffSSHeuFGvr6+hdXefWlIuqoRBGc+dLt/+eaKwBMdWLA/1m8/wBKdcnDuPrkha2FMpduK/zosg+P5apuIPeeIP39/vbTUrHg//nhc/jVRN96B/bTZPHDZ2ez5paIbz93j993F////+GBGfrie7GSNPnYiWdBAF49BVEzAAKD2ePe1uvq6uqbGDVndNasQZyQfHFPE4VtQfi4Kei0T9t1FvO6Niuj8yZ2p+OHELOok9SoOABKdEu2/7ivnsXo/+B6F1R3bfC+RfjcZQRxtq55Jjuw/+6xKiRVZm1gPNvZ1AR6x4tqJvitJDOt//ikIuujIN3Sv993F3FWHzxcXsCcTv3uxfTCNPbPX/XOdYHM/+yJG/GZHvL6/+n2//jIU/XMQHnJ/9LKvQFgnnBjSIdnNxRhjfehIeWHHOV+GcmnQVc3AA5Ob25GA5mQgXuRdOuqNMDm/4FYDlC5/0e1/zWt/z6x/xJmmQBJdeF5GLyJIO+5Vy2q/zau/8mTKwBNbt9vH0Wo6d93F+fn5qfc//fXfuaPFDciAJt+S/iyJgdnowVRfuypJwF4x9vw/+F5F9J6D3TH/8W8reWDDeB7FvKfH21eQlq8/wBhm9qzVHJIAK18crIAAAD7dFJOUwD//9////////+A////////////gP//z9////+P/////////////////////////////////////////4D//////////6r/MP///////+///7Zw//8w/////////////////////4///////1D//5T/////////////////////////IP/P//////////////////////9Q////////YP///6///////////9///////////0D/////////////////////n9/P////////////////////////67///////xcQ/2D/////////////7//P/////5/////f3pmR8wAAAAlwSFlzAAAOwwAADsMBx2+oZAAACkhJREFUSEudl39cU9cVwF+75CGzgFVKxVgBhYKtFUYLQ6P8UNsyYjBKVNa++pIlDoFiQcVoIVAH5ZfUWDSIZmupBgRmCEG0Q0gUdCshgvVHLIgyQFNDY6w4FZz7Y+fePClQ1vaz7+djuPfdc8477957fkiM4V54ePg7BLE2JSWxYMGCBQRx1GzOM5utBBHdqM5Q3/UlCK5lXW6EhUUQkiIYPNGDFo+yUejvKF7bwsP/dIcgZqWkbOpbsEBDGMzmlX+4adYTpCqzRd1630gIlvrn5roMSglCCwMLhdSMLK0M6xN3PmhPYNlYw4/S1+J58QsAXRCzbNkyrzdhLqd7gZXx2VUA3UAQZP5H94Vbn2TUyIU1Z32HwRb368GQ6YHswOlhgyNcA0HEkuctTh2IXcdEugEwMsChWgqP575VKCIl+C3AbLFEIJHmMLMfM6XBoLXZqCii2TH/7F46bBlwD7YNza+Co81Ne8HRZYkwt6cgOptu+MF2LkihCeIk8vzL+LRK8HzufXgj7M1688082OaXzTQ4ymedpVXCGnbFVLm8m9Y2EHrb9frWVjU7y7m10arQuoNEmFvYicDFgfmRgyNqqoFYQ/7F4hKxIzd3o/8ei0gH7+Vwn3uAP7bomBuLBw9m6pRODokOrhF5Skh1tn3f5R6/8CXLOIQfECYOCR+n1QlmO+YAXyIQ8GKZCWbJi+j31sftUVHt7Y/QTq39Hj0Z5eCZDX3a268yhLeQladPr2bWENcuA15eXpdnXdx2J+kLjxc8kpKSYpjF76uaR6n8LruplN7s8ZQNMZ8RRD+tqZrmQJOcnRbftKmmarODWTR+zcOHI10MbXlp8WmLmUmXXf4QfYkpQZsPZ0LPoOlDjy+y2RVz37DTNfQb8Ciagv0n9KStr40WKqqr632S1ey8rEs+Psl3FaoRO+3LMrrjnfO9cqUxMygoNDSrAiyEhoYGBWW2Xi+q99XKQCBWplVGWiwWF//UkpIRYGlJ6jGXPRbLYCOXRKdB6I0sUYhbXLl/xDo4FGBjhL+Ld6TbM1yKg09jQGwEG94u4wT27FbbSM7T4+BzdFrl1qJPRgU8OwJslJEHDjAMSAVGOFLXJ1tqa2ufOaW0UToZjznsp8yWSjgyow4wGjkCsQlFxo9pcO/pOew++drPoOmHn5xH7ejCfHwLPfk3nOMPvO+RThyOGkZXz8bSJhjaG1af9mPWMNfOnOlOKNvGXKhXC7R9L51OZ9YwlffuTXnNEYYODp450/w+s4johRvX7OX14eXgba9NS/Lw8IIbl4TvMaL/zUQPlF+A9L0FBfYXUPAC0zTYzyUxG5gL2dw8jd7UlL1Uw1xIoOpXBEEXm3GUA4+Df3ejoGlOZy+KdswsO3z+avss5s72Tt20Mj7ths+zzJ29Wkmj7WmYTz+L8gJwBy51/EqnOpTZqqp66Rly5ESsStjNXOOuxXCp89qYSdd24UMkoB9WdJvN5ps34WdvHvAyDNbDP/NRhQHFs4SKDk5JWdRZQ9vfmAuX9sjLh2iIAXB6S3QCH1wwaH3ps20nhSrh/aknszIyXqnZKhcK59Ovn41mkZDe3WUgQNda66vvJvtcOpWXEZBs9amv9xHSbdEoI2IBhcpa3Zq5s1UdkJXBbnF2Vre2lilU16Nt6Nq7cyjf6KLrrThwwAV2RdbOIAicxitXQECMfKC4ZSMjg4NhkdPzAwPZiwMDp58ICRt0+zqMy9JBrSB4pE00aLHscTn2oGQpiqySklR/Fwi1SKXWOBME+BA2WMA/YiOOix3rIHAslqWfcinDGhAY4lDcfLfdTufGCjzwdAsRsXTgAhN5bpY93nE4gWLi/mx57jyEpiP0YjmkTb17wis8kQc8JkL0Msr2aYdlrMD+IniBALmIcOeBhLrDc1Tgk6KtIhbJgYNgGOLJSJbyy8ILbx3PzT2+sHBfC1er40jHhOAQj6OjWMqWZyC4tzxx5WpJo2DsOsS2XiDTkVoWjiyKNHIkJmblKQMmseBp+Ms4Ev0PmX+UnFgpD6qAQCIR8ydZxgy4H+7pcYc6/EvJacD870I7gf5g80EcsDlrUOUxDA8nYIaHDSitrEGG+l/s6r2GZCbjA03S6W8rv2qPMiRQ2rJTBSvCf+NgRUFTAItKGI5q/4dmyo4pmiWMwgT6/VLeAb7ZW7rib5OyLbtt82ngczujMYHVxVMw32oWLZo3b31lcXFx97vh4e/egUHl+nnzFh1Kv3wPcVnzAaMylv4/Vm7+cDybK+nlK1YspzVnvxjP55W/RklllPff1CR2/QtK7ngS/Tqfz2663fQK7df77URe70r8/u84h64NTqx6fRI2F9Mnk0uzb9/OLk0+aU8/+9KPaa6KQeltCU37xdShXDSWukp6IdJPS7udvSl5P625umE8KXXpfvQ1plb8tZPujumtG0vvwZo5N5pux8dDOsy+Mcc+6+rnY7iKtDu/cmgTxCOhUL61s/ubcdStP5IFLQIYSMvLOnLwm3+Opa67ZrtcLn/k0D9gioqyqo52X53A3N/npWHyLq5nHo1Cb3+oslr5B7CBoSgZXNct3Y8x0HLQbXNvomH3YpQK2RW/PYQX5gbTtB0vPH5Mb1EMw838GOm/zTfotCzffBq2PuUQjXybHwc9DHAxi52Rwa44giczaufL5ds76UPojOi+aJaWNLS/DQbc9ai9AwPNH9npqUKV1QewqlQqnznqU+yMvIysgEs+KqFQBT5boQjMoIM/aqajfW0sShaFegp3vQwZiK6hO4+qrIq7ZY2urmpn50uXLjkHgIEMdlaA85zkOTAtKyurvquwCrd20lOxAUdTgj1g+UbXK6yK+urG1kwoBDtDdyKgjUKfcKqlpQXPgzIzWxur68ETBRjQkjIxMnAL9oBi2Xz7whCRtfnnoXxA81URWoH3EHaxAjVj0I0Fnc+vjcRifeCAluToUesxYOKhhkjZGLIKakbq8l2r3hscBQkzQ+C5VbuWp4LQqhBXJRe+wMDHqfWwHidd0fTBcliEOuPi7x8RsW7jDpzHMTs2rouI8Pd3QQ2fxVI+OF2khKZaJj6M9IkcE0+mo2xK1/dClpYf+1kDqU4hka6gTxolJiZb3uJLGAtu5d5xu4ETcd7e5efOnbNgdQuMyr13xUXCSqSTk9t7Dn2BFDdfCHdsgcVtrHXzZDzYc+6cN7AfgQZgzOGBp1vtU/0fGkuwwOPgnSzt2F3yU5/gubujVMTVQuMpGasPtR4qDzihtYlK93WcSJ3cQOqJjhOgzqJ0Rg7PNLG+9IATqL7ZuK7P7ysq/O7BeAPl+wuL9j2PnEfqEmkPozaGgSEpTyAzkpSWxRW1nF94ofDChf9gLsBw4fkWERe0SR2URunQ5N3vQA9fjDtoZMRmU4pcXeESt7i6ipRcXHVRYRWI+T2Tq2MOxIINVIfBCkmNQpK4KgskYmmsI4n8BDmPTFIxKtQcDkeGgQFUbZ5YasL/TftFHJg908SX6vViQK+X8k0zZ//sm/8vCOK/RaQO/rsFipEAAAAASUVORK5CYII=">
<p></p></div>
                <a class="btn btn-lg bg-primary white mr-0 mb-2 hide" id="pbar_bgcolor" style="background-color: #14b0bf!important;" href="<?php echo $home_url;?>/learn/5-minute-website">BUILD A WEBSITE NOW</a>
                <p class=" hide">Learn more</p>
               
              </div>
            </div>
           
          </div>


          <div class="container lesson-cards " style="">
           <div   style="toXp:-100px; posXition: relative;    backXground-color: #fff;" >
            <h2 class="title" style=" position: relative; font-weight: 300;">Let’s get 1,000,000 youth to code their first website.</h2>
              <div class="card-deck col-md-18 mx-auto">
                <div class="card ordXer-md-1 order-sm-0">
                  <div class="card-body">
                    <h5 class="card-title" style="font-weight: bold;">Build a website for Nomzamo</h5>
                    <p class="card-text" style="font-weight: bold;">Learn the basics of HTML with this "Intro" Lesson.</p>
                    <a  class="btn btn-lg bg-primary white " style="font-weight: bold;background-color: #ff00a5!important;">START CODING!</a>
                    <a href="<?php echo $home_url;?>/learn/nomzamos-website-01" class=" divLink" > </a>                               
                  </div> 
                </div>
                <div class="card ordXer-md-1 order-sm-0">
                  <div class="card-body">
                    <h5 class="card-title" style="">5&#8209;Minute&#8209;Website</h5>
                    <p class="card-text" style="">Waiting for the bus?<br>Code your first website in HTML. Don't believe me?</p>
                    <div class="btn btn-lg bg-primary white " style="background-color: #777!important;cursor:initial;">TRY CODING NOW!</div><small style="font-size: 80%;"><br>ONLY AVAILABLE ON MOBILE</small>
                                                  
                  </div> 
                </div>  

              </div>
            </div>
          </div>
         
      </div>


    
      
      <div class="clearfix"> </div>
      <div class="main">
       
   <div class="section section-about block-1 " style="">


     

                <div style="padding-top: 10px; text-align: center;" class="skew block-2">
          <div class="content">
          
           <div class="container text-center mt-3 mb-3 ">
            <div class="row justify-content-md-center">
              <div class="col-md-16 col-lg-11 ofXfset-md-2">
              <img alt="testimonal-lebo" src="<?php echo $home_url;?>/img/testimonal-lebo.jpg" style="display: block;width: 100%;border: 10px solid #ffcc00;" class="mb-1">
                <h4 class="mb-1 ml-4 mr-4 mt-2" style="font-size:2.2em;">Don't be that girl that's so 2017. <br>
</h4>
              </div>
            </div>
          </div>        

          <div class="container text-center mb-5">
            <div class="row justify-content-md-center">
              <div class="col-md-24 col-lg-8 mb-5">

                <a class="btn btn-lg bg-primary white mb-8" style="font-size: 1.5em;" href="<?php echo $home_url;?>/learn/5-minute-website">MAKE YOURS NOW</a><br>

         

              </div>
            </div>
          </div>
          
          </div>      </div>
           


        <div class="container block-1 mb-5 pl-4 pr-4" style="backgrouXnd-color: white;   margin-top: 6rem !important;">
          <div class="row pt-5 pb-5 take-action" style="background-color: white; box-shadow: 2px 2px 25px 5px rgba(0, 0, 0, .2);   ">
            <div class="col-md-24 text-Xmd-left text-center mb-5 mb-sm-3"  >
               <h2 class="title mb-0" style="text-align: center;">Take Action!</h2>
            </div>
            <div class="col-md-8 text-Xmd-left text-left pb-5 mb-5 mb-sm-0"  >
            <div class="icon"><img src="<?php echo $home_url;?>/img/emoji/smiling-face-with-open-mouth-and-smiling-eyes_1f604.png" alt="Grinning Face With Smiling Eyes" title="Grinning Face With Smiling Eyes" width="64" height="64"></div>
               <p style="font-size: 20px;" class="pb-0 mb-0 pr-md-3">01. <b>Do the 5&#8209;Minute&#8209;Website on your phone</b>. It’s easy and anyone can: <br><a style="font-size: 85%;    word-break: break-word;" href="https://www.codejika.com/5min" target="_blank">www.codejika.com/5min</a></p>
            </div>
            <div class="col-md-8 text-Xmd-left text-left pb-5 mb-5 mb-sm-0"  >
            <div class="icon"><img src="<?php echo $home_url;?>/img/emoji/smiling-face-with-sunglasses_1f60e.png" alt="Smiling Face With Sunglasses" title="Smiling Face With Sunglasses" width="64" height="64"></div>
               <p style="font-size: 20px;" class="pb-0 mb-0 pr-md-3">02. <b>Organize the 1&#8209;Hour&#8209;Website</b> Offline Event in your School at your school, club or church.<br><a style="font-size: 85%;    word-break: break-word;" href="https://www.codejika.com/1hour" target="_blank">www.codejika.com/1hour</a></p>
            </div>
            <div class="col-md-8 text-Xmd-left text-left pb-5 "  >
            <div class="icon"><img  class=" lazyloaded" src="<?php echo $home_url;?>/img/emoji/person-raising-both-hands-in-celebration_1f64c.png" alt="Raising Hands" title="Raising Hands" width="64" height="64" ></div>
               <p style="font-size: 20px;" class="pb-0 mb-0">03. <b>Share your 5&#8209;Minute&#8209;Website</b> results or a photo of your 1&#8209;Hour&#8209;Website Offline event. <span style="font-size: 85%; color:#f96332;">#CODEJIKA</span></p>
            </div>
            <div class="col-md-24 text-Xmd-left text-center mt-5 mt-sm-2"  >
            <a class="btn btn-md bg-primary white " style="font-size: 1.3em;    white-space: normal;" href="https://www.codejika.com/downloads/201811 CodeJIKA.com CSEDWeek one-pager v3.pdf" target="_blank">Check out the CSEd Week 2018 1&#8209;pager PDF</a>
            </div>
            </div>
            </div>
            
                    <div class="container block-1  mb-5 pl-4 pr-4" style="backgrouXnd-color: white;   margin-top: 4rem !important;">
          <div class="row pt-5 pb-5 take-action" style="baXckground-color: white; boxX-shadow: 2px 2px 25px 5px rgba(0, 0, 0, .2);   ">
            <div class="col-md-24 text-Xmd-left text-center  mb-3 mb-sm-3 "  >
               <h2 class="title mb-0" style="text-align: center;">Offline?!?</h2>
               <h2 class="subtitle mt-1 mb-0" style="text-align: center;font-size: 1.35rem;">Host the 1&#8209;Hour&#8209;Website in an school or lab?</h2>
            </div>
            
            <div class="card-deck offline-steps">
            <div class="d-flex justify-content-around flex-md-row flex-column">
          <div class="arrow_box flex-column justify-content-center align-items-start align-items-sm-start">
            <p style="font-size: 20px;"><b>All you need are computers, no internet, no installation.</b></p>
            <p style="font-size: 20px;">Just you! It's super easy.</p>
          </div>        
          <div class="arrow_box  flex-column justify-content-center align-items-center align-items-sm-start">
            <p style="font-size: 20px;">Download this A4 Instruction sheet, print it for each student and start coding.</p>
            <p class="mb-0"><a class="btn btn-md bg-primary " style="font-size: 1.3em; marXgin: auto;  background-color: #fff!important;
    color: #5c2d91;   white-space: normal;" href="<?php echo $home_url;?>/downloads/1-Hour-Website-Instructions-with-Bonus.pdf" target="_blank">DOWNLOAD</a></p>
          </div>        
          <div class="arrow_box flex-column justify-content-center align-items-center align-items-sm-start">
            <p style="font-size: 20px;">See all 1-Hour-Website Resources</p>
            <p class="mb-0"><a class="btn btn-md bg-primary " style="font-size: 1.3em; maXrgin: auto;  background-color: #fff!important;
    color: #5c2d91;   white-space: normal;" href="<?php echo $home_url;?>/1-hour-website" target="_blank">GO NOW</a></p>
              </div>
</div>
          </div>
 </div>
 </div>
<div class="container block-1">
<h2 class="title" style="padding-top: 20px; position: relative;    text-align: center;">FAQ//: Freakin' Awesome Questions</h2>
              
    <div id="FAQs" class="accordion"  style=" ">
        <div class="card mb-0">
            <div class="card-header collapsed" data-toggle="collapse" href="#FAQ-what-CJ" >
                <a class="card-title">
                    What is CodeJika?
                </a>
            </div>
            <div id="FAQ-what-CJ" class="card-body collapse" data-parent="#FAQs" >
                <p>A vibrant eco-system of student-run coding clubs in secondary schools.</p>
<p>Operationally it is divided into 3 Pillars; 
<ol>
<li>Online (platform and partners), </li>
<li>Awareness (media & advocacy) and </li>
<li>Hands-on (In-school Clubs & Events)</li></ol></p>
                </p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" href="#FAQ-what-learn">
                <a class="card-title">What do I learn at CodeJIKA?</a>
            </div>
            <div id="FAQ-what-learn" class="card-body collapse" data-parent="#FAQs" >
                <p>You learn how to code, starting with "1&#8209;Hour&#8209;Website" - How to code a simple website using HTML & CSS. Each consecutive project builds on this skill until you can customize and build beautiful business websites for SMEs from scratch – Like a PRO.  
                </p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-how-help">
                <a class="card-title">How can I help?</a>
            </div>
            <div id="FAQ-how-help" class="card-body collapse" data-parent="#FAQs" >
                <p>Run an event or assist schools or other organizations who are.</p>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-run-CJ-event">
                <a class="card-title">How do I run a CodeJIKA event?</a>
            </div>
            <div id="FAQ-run-CJ-event" class="collapse" data-parent="#FAQs" >
                <div class="card-body">Easy. Go to the “How to run an event” page here to get started. There you’ll find the curriculum, suggested format, easy to use curriculum and tools to engage volunteers and even fundraise for the event.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-be-programmer">
                <a class="card-title">Do I have to be a programmer to run an event?</a>
            </div>
            <div id="FAQ-be-programmer" class="collapse" data-parent="#FAQs" >
                <div class="card-body">Anyone can run an event. It’s real fun and it’s about learning together and allowing others to have space to grow and someone who believes in them. You should have some desire to learn and work with the kids to find solutions though.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-for-primary-schools">
                <a class="card-title">Do you have a version for primary schools?
</a>
            </div>
            <div id="FAQ-for-primary-schools" class="collapse" data-parent="#FAQs" >
                <div class="card-body">There is no official version for primary schools, but from what we’ve heard the 5th, 6th and 7th graders love the program as well.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-not-secondary-school">
                <a class="card-title">Can I join if I’m not in secondary school?
</a>
            </div>
            <div id="FAQ-not-secondary-school" class="collapse" data-parent="#FAQs" >
                <div class="card-body">Of course. If you’ve already left school you can join the online CodeJIKA program and can help in organizing events and mentoring. The clubs and competitions are only for secondary school learners at this time.</div>
            </div>
            <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-different-from-HOC">
                <a class="card-title">How is CodeJIKA different than Hour of Code?

</a>
            </div>
            <div id="FAQ-different-from-HOC" class="collapse" data-parent="#FAQs" >
                <div class="card-body">CodeJIKA teaches you how to become a Junior Frontend Web-developer, as fast as possible. It’s about building communites and creating revolutions within the existing educational system, stemming from the student base rather than a teacher-driven approach. 
CodeJIKA’s projected timeline is 3 years per group. You can go from never touching a PC to learning to hard-code professional websites in 6 months while still in school.

Our Belief: Our youth are engines of the new economy and are instinctively infused with a desire to build - Nothing can stop them! 
We’re here to provide tools. That’s all.
</div>
            </div>
        </div>
    </div>
</div>
        <div class="section " id="" style="background-color: #fff!important; coloXr:white;padding: 30px 0 50px 0;">        
        <div class="container" >
          <div class="row">
            <div class="col-md-24 text-center"  >
            <h2 class="title mb-1" style="padding-top: 20px; positXion: relative;font-weight: 300;">CAMPAIGN PARTNERS:</h2>


            </div>
            <div class="col-md-24 text-Xmd-left text-center logos"  >
                <a href="https://www.microsoft.com/" target="_blank"><img alt="ms-logo" src="<?php echo $home_url;?>/img/logo/logo-microsoft.jpg" class="mb-md-0 mb-3" style="    max-width: 250px;" ></a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-24 text-center"  >
            <h2 class="title mb-1" style="padding-top: 60px; positXion: relative;font-weight: 300;">PEOPLE THAT CARE ABOUT YOU:</h2>
            </div>
            <div class="col-md-24 text-Xmd-left text-center logos"  >

                <a href="https://www.datatec.com/" target="_blank"><img alt="datatec-logo" src="<?php echo $home_url;?>/img/logo/logo-datatec.jpg" class="mb-md-0 mb-3" style="" ></a>	
                <a href="https://www.iress.com/za/" target="_blank"><img alt="iress-logo" src="<?php echo $home_url;?>/img/logo/logo-iress.jpg" class="mb-md-0 mb-3" style="" >	</a>
                <a href="https://www.pnet.co.za/" target="_blank"><img alt="pnet-logo" src="<?php echo $home_url;?>/img/logo/logo-pnet.jpg" class="mb-md-0 mb-3" style="" >	</a>
                <a href="https://www.tcs.com/" target="_blank"><img alt="tcs-logo" src="<?php echo $home_url;?>/img/logo/logo-tcs.jpg" class="mb-md-0 mb-3" style="" >	</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-24 text-center"  >
            <h2 class="title mb-1" style="padding-top: 60px; posXition: relative;font-weight: 300;"><span class="aqua">CODE<strong>JIKA</strong></span> PARTNERS:</h2>


            </div>
            <div class="col-md-24 text-Xmd-left text-center logos"  >
                <a href="https://codeforchange.co.za/" target="_blank"><img alt="C4C-logo" src="<?php echo $home_url;?>/img/logo/logo-C4C.jpg" class="mb-md-0 mb-3" style="" >	</a>
                <a href="https://www.education.gov.za/" target="_blank"><img alt="Gov-Edu-logo" src="<?php echo $home_url;?>/img/logo/logo-Basic-Education.jpg" class="mb-md-0 mb-3" style="" >	</a>
                <a href="https://www.code.org/" target="_blank"><img alt="-logo" src="<?php echo $home_url;?>/img/logo/logo-code-org.jpg" class="mb-md-0 mb-3" style="" >	</a>
                <a href="https://africacodeweek.org/" target="_blank"><img alt="africacodeweek-logo" src="<?php echo $home_url;?>/img/logo/logo-africacodeweek.jpg" class="mb-md-0 mb-3" style="" >	</a>
                <a href="https://www.hourofcode.com/" target="_blank"><img alt="HoC-logo" src="<?php echo $home_url;?>/img/logo/logo-hourofcode.jpg" class="mb-md-0 mb-3" style="" >	</a>
            </div>
          </div>

        </div>        
        </div>                  
         
      </div>
        



      <footer class="footer" data-background-color="black">
        <div class="container">
          <div class="row">
            <div class="col-md-3  col-24 powered-by">

              <div class="row" class="c4c-logo pb-2">
                <div class="col-md-24 col-9 offset-md-0 offset-4" style="">



                  <h4 class="pb-1">POWERED BY:</h4>
                    <a href="https://code4change.co.za/" target="_blank"><img alt="C4C-logo" src="<?php echo $home_url;?>/img/C4C-logo-white.png" style="max-width:80px; padding-bottom:10px" >	</a>
                 
                </div>               
                <div class="col-md-24 col-8 oXffset-md-2" style="">
                    <ul>
                      <li><a href="https://code4change.co.za/" target="_blank">Learn More</a></li>
                      <li><a href="https://code4change.co.za/about" target="_blank">Team</a></li>
                      <li><a href="https://code4change.co.za/about/sponsors" target="_blank">Sponsors</a></li>
                    </ul>                  
                </div>
              </div>
            </div>
            <div class="col-md-14" style="alXign-self: flex-end;">
              <div class="row">
                <div class="col-md-6 col-12" style="alXign-self: flex-end;texXt-align: right;">

                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                    <h4>CodeJika:</h4>
                    <ul>
                      <li class="hide">Sign Up</li>
                      <li><a href="<?php echo pll_home_url();?>learn/nomzamos-website-01" target="_blank">Start Coding</a></li>
                      <li><a href="<?php echo pll_home_url();?>club-signup">Start a Club</a></li>
                      <li><a href="<?php echo pll_home_url();?>1-hour-website">1 Hour Website</a></li>
                      <li><a href="<?php echo pll_home_url();?>the-5-minute-website">5 Minute Website</a></li>
                      <li><a href="<?php echo pll_home_url();?>coding-clubs">Coding Clubs</a></li>
                      <li><a href="<?php echo pll_home_url();?>computer-science-week2018">Computer Science Week 2018</a></li>
                    <ul>
                  </div>
                </div>
                <div class="col-md-6 col-12" style="alXign-self: flex-end;text-align: center;">
                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                    <h4>Education:</h4>
                    <ul>
                      <li><a href="<?php echo pll_home_url();?>news">News and Articles</a></li>
                      <li><a href="<?php echo pll_home_url();?>coding-resources">CodeJIKA Resources</a></li>
                      <li><a href="https://csedweek.org/" target="_blank">Hour of Code</a></li>
                      
                      <ul>
                  </div>
                </div>
                <div class="col-md-6 col-12" style="alXign-self: flex-end;">
                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                    <h4>Partners:</h4>
                    <ul>
                      <li><a href="<?php echo pll_home_url();?>business">Corporates</a></li>
                      <li><a href="<?php echo pll_home_url();?>schools">Schools</a></li>
                      <li><a href="<?php echo pll_home_url();?>schools">Districts</a></li>
                      <li><a href="<?php echo pll_home_url();?>media-corner">Media Corner</a></li>
                      <li>Volunteers</li>
                      <li>Launch in your Country</li>
                      <ul>
                  </div>
                </div> 
                <div class="col-md-6 col-12" style="alXign-self: flex-end;">
                  <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                    <h4>Country/Region:</h4>
                    <ul class="wpXm-language-switcher switXcher-list">
			<li class="item-language-en <?php if ($lang_path == "" || $lang_path == "/" || $lang_path == "/index.php") { echo 'hide';} ?>" >
 							<a href="<?php echo $home_url;?>/"  data-lang="en">
												<img  class="hide" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjJGNDdGODZDODlDQTExRThBNEYxRERFNjc0OEQyNjk5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjJGNDdGODZEODlDQTExRThBNEYxRERFNjc0OEQyNjk5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MkY0N0Y4NkE4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MkY0N0Y4NkI4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AAAUHAAAFoAAABfUAAAZf/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wgARCAALABADAREAAhEBAxEB/8QAngABAQEAAAAAAAAAAAAAAAAABgUIAQEBAQAAAAAAAAAAAAAAAAABBAUQAAICAgMBAAAAAAAAAAAAAAABBAYCFAMTFQcRAAECBAUCBwAAAAAAAAAAAAQCAwEREgUAIhMUFTEWIVNzJCU1BhIBAAAAAAAAAAAAAAAAAAAAIBMAAgEDAwQDAAAAAAAAAAAAAREAITFBYXGB8FGhwRAgkf/aAAwDAQACEQMRAAAB3BXlJ0PDYT//2gAIAQEAAQUCvU2fhJpEiY+K+6m5876PG//aAAgBAgABBQJiGI//2gAIAQMAAQUCQxGR/9oACAECAgY/Ah//2gAIAQMCBj8CH//aAAgBAQEGPwJhhi5G28cYeBq0BkBiQOcixd3EskvuXG3H6SVW5EIJZWmEa41TywwcETcC7ogVysco50J9+hw65j6O4DLMi80hASVQi6uLufrGEsfm952/pbx6W/5vn66UU9v8D7z1p5JSngnbdu6HJlUdv8l0yfb8v8hy/manjKWP/9oACAEBAwE/IbhvebTdnCMKCHdmcZFvPVE386vCEVT/AM7vZNb9bU//2gAIAQIDAT8h+gf/2gAIAQMDAT8hEfHO8uzP/9oADAMBAAIRAxEAABBjH//aAAgBAQMBPxATZ6keXCUYEamC321NGkROi90y5wuP8IU9nHnJ3Ysv/9oACAECAwE/ECLuhGqCXEYa8vhRFRXx7c//2gAIAQMDAT8QMDIBJptbQjOYAYIAD7PTvHSVLRcum0a+pZXhUU//2Q==" alt="International">
													<span>Africa</span>
											</span>
					</li>
			<li class="item-language-za <?php if ($lang_path == "/southafrica") { echo 'hide';} ?>" >
 							<a href="<?php echo $home_url;?>/southafrica/"  data-lang="southafrica">
												<img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVHjaYmBoFWZo5ffeX37z4unP6up/GRgg6DcDw08Ghu8MDF8ZGD4zMHxkYHjPwPCWgQEggBgbDzP8Yp3C8O+PuJB0kJit6N4jLEePMfz9/f/PH4Y/f/7/BjHg5JdNmwACiMXoBoNZ+OfaM18ePnx56sXlcodE1W9fWI6d/v/gHkjdr9//f//6/+sXkM0oK/uPgQEgAAAxAM7/AQAAAMnBfiEn6oPXCuf4BP3993MxaCQGDxLe5v/19f/+/v/+/f/9/v/+/gEJCvGrqwIIpKGsrExH44WDLcPEB5xP/7C++/Xz738GDmaOv//+/P4LQSA3yfCIb5g0ESCAWIAa/vz5u3Hr19fvmcv8vnfe53j9j+vHn2+fP7/49ff3r7+/gKp//fsN1Mb+9yfDCwaAAAJp+Pv3j5sTk7P9v9kP2B78ZP3x5+uf//+4uIXZ/v4Dmf33zx+ghn9/eLhEGHgYAAIIpMHfnVFDl7HjJvelzyy/fn2dbFPPzcT95i73ty9///4F++If0Bf/eLhZZNTSAAKIZX4zg7oZS+5Jvjdf/zCw/i42Sdi9nHXz2vcvXj8DGgsOpH9AK4BIRYXz4sVdAAHE8s+LoeYiNxcTs4W8aJiU/455nGfOfeHmY5Dn4gS54w8wAv4B7fn7F0gCXfMPIIAYGTKBvmYQt7CuE5iQHfyKAegvhn9g9AvG+ANGDGCSDSDAAOBCLl8bj6ZDAAAAAElFTkSuQmCC" alt="South Africa">
													<span>South Africa</span>
											</span>
					</li>
			<li class="item-language-zambia <?php if ($lang_path == '/zambia') { echo 'hide';} ?>">

							<a href="<?php echo $home_url;?>/zambia/"  data-lang="zambia">          
												<img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" alt="Zambia">
													<span>Zambia</span>
											</a>
					</li>
			<li class="item-language-namibia <?php if ($lang_path == '/namibia') { echo 'hide';} ?>">
							<a href="<?php echo $home_url;?>/namibia/" data-lang="namibia">
												<img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIZSURBVHjaYmRgmMjA8IOBgQWMgIxfDAz/GBj+gBGQLTJxqo+b2i8pV+N/YAmAAAAxAM7/AQAAh359Qw4L9wgH+OnsDPf5Avz8/v39/vz8/TI2GE9LHAnm3waRjP/y9P/7+/SysgIIaCobG+vvBK9VHOzfO+a4PHoo9vvPb05O5ugETTN1Dr6+hv/7D//79ev/7z+MtXXnKuMBAgiogYGR4R8jw29mxt9AEqhaRU04P1eD9/1Tjvz0/8/f/P/1+7+00uf6wjvijO6V1gABBNTw69MX1qYZHlxsX+8/FwoKV/e0F+RbOuP/+k3/f//6DzQ7LOJFjO+8l+sOn9zD8IQBIICAGv78/vPvyUteERGJzm4dwd/vOcrS/997CnTxf26hr13Ft5V5Ks7l3n55Q1FAERgQAAEEsuHv3782TtJxIdI829YyLl74/+cvkDOcXF9nRG75drJ/X+eXX19+A1377zcw2AACCKiBq7BIX4j5O2d71f/zV0AGM3N+rS1+Yqzccr3z1IPDvxh+AZX++fcHGHJAGwACiOXu9QTJBxcYu5r/f/kFUq1j/Koo/iTr4/aDSW9+vPkFNPjvrz8gDX+BCKgBIIBYpMwUmEKi/wtJ/Bdg+hXu/8pCb/Wzjfuu7RTgEOBm4/7z98+f/3+Abv77Hxg3f4FOAgggxq/g+AOiWysnb1P4Vj+nHBgUIP4vpOj+A4t9HgaAAAMAQO8VmfiJ/b4AAAAASUVORK5CYII=" alt="Namibia">
													<span>Namibia</span>
											</a>
					</li>
			<li class="item-language-botswana <?php if ($lang_path == '/botswana') { echo 'hide';} ?>">
							<a href="<?php echo $home_url;?>/botswana/"  data-lang="botswana">
												<img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Botswana">
													<span>Botswana</span>
											</a>
					</li>
			<li class="item-language-mozambique <?php if ($lang_path == '/mozambique') { echo 'hide';} ?>">
							<a href="<?php echo $home_url;?>/mozambique/"  data-lang="mozambique">
												<img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Mozambique">
													<span>Mozambique</span>
											</a>
					</li>
                                   
	</ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-7  col-24 offsXet-md-1">
              <p style="padding-top:20px; font-siXze: 12px">
                CodeJIKA Movement: Eco-systems of vibrant, <strong>student-run coding clubs in secondary schools.</strong>
                <p style="font-sXize: 12px">CodeJIKA launches online tools, hands-on roadshows & media campaigns to do this.</p>
                <p style="font-sXize: 12px">
                <a href="https://www.facebook.com/codejika/" class="mr-2" target="_blank"><i class="icon-facebook-official fs2"></i></a>
                <a href="https://twitter.com/codejika" class="mr-2" target="_blank"><i class="icon-twitter-square fs2"></i></a>
                <a href="https://www.instagram.com/codejika/" class="mr-2" target="_blank"><i class="icon-instagram fs2"></i></a>
                <a href="http://www.linkedin.com/company/codejika/" class="mr-2" target="_blank"><i class="icon-linkedin fs2"></i></a>
                <a href="https://www.youtube.com/channel/UCkmsiNfz3SnmNCUJv4P5YUA" class="" target="_blank"><i class="icon-youtube-square fs2"></i></a>                
                </p>
            </div>
 

          </div>

        </div>
        <div class="container">
        <div class="row">
          <div class="col-md-24 pl-1 pr-1  pl-sm-2 pr-sm-2" style="background-color: #292929;">
            <div class="copyright ">
              <a class="switch-mode mr-2  mr-sm-4" href="<?php echo pll_home_url();?>?mobile">View as Mobile</a> &copy;
              <script>
                document.write(new Date().getFullYear())
              </script>
              <a href="https://code4change.co.za/" target="_blank">Code for Change</a> | CodeJIKA
            </div>
          </div>

        </div>
        </div>
      </footer>
    </div>
    <div class="modal fullscreen" id="menuModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <!-- Modal body -->
          <div class="container d-flex h-200 menu">
<div class="row">
<div class="col-md-24 text-center">
              <h2>Menu</h2>
              </div>
              <div class="col-md-12 d-md-none">

                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">
                        <h4 style="user-select: text;">Start Coding</h4>
                        <p class="d-none d-sm-block"></p>
                        <a href="<?php echo $home_url;?>/learn/nomzamos-website-01" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 d-md-none">

                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">
                        <h4 style="user-select: text;">Start a Coding Club</h4>
                        <p class="d-none d-sm-block"></p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/club-signup/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">

                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">
                        <h4 style="user-select: text;">Schools</h4>
                        <p class="d-none d-sm-block">Join the network of forward-thinking schools building an eco-system of fun, student-run coding clubs.</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/schools/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">

                        <h4 style="user-select: text;">Business</h4>
                        <p class="d-none d-sm-block">Coding in schools allows youth to create and build tools to empower SMEs and the economy.</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/business/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">

                        <h4 style="user-select: text;">CodeJIKA Resources</h4>
                        <p class="d-none d-sm-block">Find the tools to teach, inspire, advertise and "Rock this". :)</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/coding-resources/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body divLink">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home" role="tabpanel">

                        <h4 style="user-select: text;">Coding Clubs</h4>
                        <p class="d-none d-sm-block">Check out who's the top coding clubs in Southern Africa</p>
                        <a href="<?php echo $home_url;?><?php echo $lang_path;?>/coding-clubs/" class="divLink"></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-24 text-center hide">
                <a class="switch-mode mr-2  mr-sm-4" style="background:white;" href="<?php echo pll_home_url();?>?mobile">Switch to Mobile View</a> 
              </div>


            </div>
          
            </div>
        </div>
      </div>
    </div>
</div>
<pre class="hide">
<?php 
//print_r(str_replace('"', "", $headers));
//printf("<pre>%s</pre>", print_r($headers, true));

var_dump(apache_response_headers());
//print_r($headers);
/*  $headerCookies = explode('; ', $headers['set-cookie']);
    $cj_cookies = array();
    foreach($headerCookies as $itm) {
        list($key, $val) = explode('=', $itm,2);
        $cj_cookies[$key] = $val;
        echo $val;
    } */


//var_dump($_COOKIE);
 ?>

</pre>
</body>
<!--   Core JS Files   -->
<!--script src="<?php echo $home_url;?>/js/merged-homepage.js" type="text/javascript"></script-->
<!--script src="<?php echo $home_url;?>/js/jquery-3.3.1.slim.min.js" type="text/javascript"></script-->
<!--script src="<?php echo $home_url;?>/js/core/popper.min.js" type="text/javascript"></script-->
<!--script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" type="text/javascript"></script-->
<!--  Plugin for Switches, full documentation here: https://www.jque.re/plugins/version3/bootstrap.switch/ -->
<!--script src="<?php echo $home_url;?>/js/plugins/bootstrap-switch.js"></script-->

<script src="<?php echo $home_url;?>/js/js-compress.php"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<!-- Load the script on the page. -->
<!--script src="https://cdn.jsdelivr.net/npm/typeit%405.10.1/dist/typeit.min.js" type="text/javascript"></script-->


<script type="text/javascript">
  /*new TypeIt('#type-here', {
    strings: 'CODE.',
    speed: 150,
    startDelay: 1200,
    lifeLike: true,
    cursorChar: '<strong>_</strong>',
    autoStart: false,
    afterComplete: function (instance) {$('.ti-cursor').remove();}
  })*/

  function scrollToDownload() {

    if ($('.section-download').length != 0) {
      $("html, body").animate({
        scrollTop: $('.section-download').offset().top
      }, 1000);
    }
  }
  $(document).ready(function() {
    $(window).scroll(function() {
      if ($(window).scrollTop() > 100) {
        $('.navbar').addClass("sticky fixed-top");

      } else {
        $('.navbar').removeClass("sticky fixed-top");
      }
    });
  });
</script>


</html>