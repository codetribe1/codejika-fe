var lesson_id = "P001-L01-M-V001";
var save_lesson_id = "P001-L01";
//var auth_id = "R9rHPGhBvTQ21Uk8fUYZua7Iwil1";
var auth_id = localStorage.getItem("userid"); 
var baseURL = "https://www.codejika.com/preview/5-minute-website?"
console.log("auth_id: "+auth_id);
if (auth_id == null){
  auth_id = Math.random().toString(36).replace('0.', '');
  localStorage.setItem("userid", auth_id);
}

var delay;
var lesson_data = null;
var lesson_progress = null;
var slide_data = null;
var user_skills = null;
var active_slide;
var active_tab;
var save_pending = false;
var checkpoint_id = 0;
var checkpoint_count = 0;
var checkpoint_completed = 0;
var progressPercentage;
var current_avatar = "robot";
var swiperTabs;
var swiperLesson;

var DEBUG = true;


if (!DEBUG) {
  if (!window.console) window.console = {};
  var methods = ["log", "debug", "warn", "info"];
  for (var i = 0; i < methods.length; i++) {
    console[methods[i]] = function () {};
  }
}

function timeNow() {
  return new Date().toISOString();
}

function saveUserData(save_code) {
  var now = new Date().toISOString();
  if (save_pending || save_code) {
    console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).update({
      user_code: editor.getValue(),
      last_checkpoint: checkpoint_id,
      progress_completed: lesson_progress.progress_completed,
      last_updated: timeNow()
    });
    console.log("Saved code and checkpoint #" + checkpoint_id + " " + timeNow());
    save_pending = false;
  } else {
    //console.log("Nothing to save: "+timeNow());
  }
}

function resetUserData() {
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).remove();

}

function resetCurrentProject() {
  resetUserData();
  localStorage.setItem("active_slide", 0);
  window.location.reload();
}

function checkpointCompleted(completed_id) {
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id + '/user_checkpoint/' + completed_id).update({
    completed: true,
    completed_at: timeNow()
  });
  checkpoint_completed++;

  if (checkpoint_completed === checkpoint_count) {
    lesson_progress.progress_completed = "100";
    lesson_status = "completed";
    console.log("checkpointCompleted: " + completed_id + " " + timeNow() + " " + checkpoint_completed + " " + checkpoint_count);
  } else {
    lesson_progress.progress_completed = Math.round((checkpoint_id * 100) / (checkpoint_count + 1));
    console.log(checkpoint_id + " * 100 / " + checkpoint_count + " = " + lesson_progress.progress_completed);
  }

  //saveUserData();
}

function initCodeEditor(custom_layout) {
  // Initialize CodeMirror editor
  editor = CodeMirror.fromTextArea(document.getElementById("code-editor"), {
      mode: "htmlmixed",
      tabMode: "indent",
      theme: 'base16-dark',
      styleActiveLine: true,
      lineNumbers: true,
      lineWrapping: true,
      autoCloseTags: false,
      foldGutter: false,
      dragDrop: true,

      lint: true,
      onKeyEvent: function (e, s) {
        if (s.type == "keyup") {
          CodeMirror.showHint(e);
        }
      },
      gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
    });
/*
  $.keyboard.keyaction.undo = function (base) {
    base.execCommand('undo');
    return false;
  };
  $.keyboard.keyaction.redo = function (base) {
    base.execCommand('redo');
    return false;
  };

  $(inf).keyboard({
    keyBinding: "pointerdown",
	repeatRate: 0,
    usePreview: false,
    useCombos: false,
    autoAccept: true,
    layout: 'custom',
    customLayout: {
      'normal': [
        custom_layout
      ]
    },
    position: false,
    beforeInsert: function (evnt, keyboard, elem, txt) {
      var position = editor.getCursor();
      if (txt === "\b") {
        editor.execCommand("delCharBefore");
      }
      if (txt === "\b" && position.ch === 0 && position.line !== 0) {
        elem.value = editor.getLine(position.line) || "";
        txt = "";
      }
      return txt;
    }
  }); */
}

function updatePreview() {

  var previewFrame = document.getElementById('preview');
  var preview = previewFrame.contentDocument || previewFrame.contentWindow.document;
  preview.open();
  //var re = /\r/;
  //var reResults = new RegExp(re, "i").test(editor.getValue());
  var check_if_blank = editor.getValue().replace(/(\r\n\t|\n|\r\t)/gm,"").trim();
  
    console.log('update preview: "'+check_if_blank+'"');
  if(check_if_blank == "") {
	  preview.write("<html><head></head><body><h2 class=\"no_preview\" style=\"height:100%;width:100%;color:#bbb;font-size:16px;font-family:'Rajdhani',sans-serif;text-transform:uppercase;font-weight:bold;position:absolute;top:35%;text-align:center;margin:auto;padding:0;\">No preview available<br><br>Time to get coding</h2></body></html>");
	  console.log('no preview');
  } else {
	  preview.write(editor.getValue());
  }
  preview.close();
  save_pending = true;
  validateCheckpoint();
}

function successAnimation() {
if (active_tab > 0) {
		$(".success-animation").removeClass('hide');

		setTimeout(function () {
			$(".success-animation").addClass('hide');
			console.log('animate2');
		}, 2500);
	}
		console.log('animate1'+active_tab);
}

function buildSlides() {
  var slide_checkpoint_id = 1;
  var slide_indexes = [];
  var unlocked_class = "";

  if (localStorage.getItem('active_slide') !== null) {
    active_slide = localStorage.getItem('active_slide');
    //console.log(active_slide + "not null");
  } else {
    if (lesson_progress.last_checkpoint > 0) {
      active_slide = slide_indexes[lesson_progress.last_checkpoint - 1] + 1;
      //console.log(lesson_progress.last_checkpoint + " null__ " + active_slide);
    } else {
      active_slide = 0;
      //console.log(lesson_progress.last_checkpoint+" null__3 "+active_slide);
    }
    //active_slide = lesson_progress.last_checkpoint;
    localStorage.setItem("active_slide", active_slide);

    //str = JSON.stringify(slide_indexes, null, 4);
    //console.log("slide_indexes:\n"+str); 
  }
  
  slide_data = lesson_data.slides[active_slide];
  all_slides = lesson_data.slides;
  
  //console.log("active_slide:\n" + active_slide);
  //str = JSON.stringify(slide_data, null, 4);
  //console.log("slide_data:\n" + str);
  //str = JSON.stringify(all_slides, null, 4);  
  //console.log("all_slides:\n" + str);

  $.each(all_slides, function (index) {
    //console.log(all_slides[index]);

    if (!!all_slides[index].css_class) {
      custom_class = all_slides[index].css_class;
    } else {
      custom_class = "";
    }
    //		console.log("checkpoint: "+index); 
    if (all_slides[index].checkpoint === true) {
      //cp_class = " checkpoint";

      if (lesson_progress.user_checkpoint !== undefined && lesson_progress.user_checkpoint[slide_checkpoint_id] !== undefined) {
        unlocked_class = " cp-unlocked";
        //console.log("checkpoint completed: "+slide_checkpoint_id); 
        checkpoint_completed++
      } else {
        //console.log("checkpoint not completed " + slide_checkpoint_id);
      }
      checkpoint_count++;
      slide_indexes.push(index);
    } else {
      //cp_class = "";
      unlocked_class = "";

    }
	



    $("#lesson-page .swiper-wrapper").append("<div class=\"swiper-slide " + custom_class + unlocked_class + " \" id=\"slide" + (index + 1) + "\">\n" + all_slides[index].html_content + "\n</div>");
	
	if (all_slides[index].onload_function !== undefined) {
	  console.log('onload_function triggered: ' + all_slides[index].onload_function);
	  var func = new Function(all_slides[index].onload_function);
	  func();	
	}
	
    if (all_slides[index].checkpoint === true) {
      $('#lesson-page .swiper-wrapper .swiper-slide:nth-child(' + (index + 1) + ')').attr('data-checkpoint_id', slide_checkpoint_id++);
      //console.log("cp_index: "+slide_checkpoint_id);
    }

  });

  
  

  //console.log("checkpoint_count: "+checkpoint_count); 
  //$( "#lesson-page .swiper-wrapper" )
  //$('#lesson-page .swiper-wrapper .swiper-slide:nth-child(10)').addClass('active');
}

function loadSkills() {
  if (user_skills) {
    $.each(user_skills, function (index) {
      unlockSkills(user_skills[index]);
    });
  }

}
function loadUserKey(){
  if(user_profile){
    $.each(user_profile, function(index){
      console.log(index,'test2')
    })
  }
}

function unlockSkills(skills) {
  //console.log("skills: "+skills);
  $("#" + skills + "-skill").addClass('has-skill-true');
  $("#" + skills + "-skill").removeClass('has-skill-false');
  //console.log('Unlocked skill '+skills);
  //console.log("#"+slide_id+"-skill");

  $("#" + skills + "-skill").attr('data-original-title', $("#" + skills + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
}

function test123() {
  console.log("test123");
}

function validateCheckpoint() {
  //slide_data = lesson_data.slides[active_slide-1];
  //active_slide = swiperV.activeIndex+1;
  //console.log('Trigger validation'+slide_data.regex);
  str = JSON.stringify(slide_data, null, 4);
  //console.log("slide_data:\n" + str);

  slide_id = active_slide + 1;
  /*if (slide_data.checkpoint === true) {
    if (!$("#slide" + slide_id).hasClass('cp-unlocked')) {

    }
  }*/
  if (slide_data.action === true) {
    console.log('action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
    if (slide_data.regex !== undefined) {
      console.log('validate_code trigger: ' + slide_data.regex);
      // do something
      console.log('Run validation on ' + active_slide);
      var re = slide_data.regex;
      var reResults = new RegExp(re, "i").test(editor.getValue());
      console.log('Run validation results ' + reResults);
      //	$(".check-info").removeClass('d-none');			
      if (reResults) {
        $("#slide" + slide_id + " .check-icon").html('/');
        $("#slide" + slide_id + " .actions li").addClass('correct');
        $(".check-info").addClass('correct');
        checkpoint_id = $("#slide" + slide_id).data("checkpoint_id");

        if (!$("#slide" + slide_id).hasClass('cp-unlocked')) {
          $("#slide" + slide_id).addClass('cp-unlocked');
          checkpointCompleted(checkpoint_id);
        } //else { //else used so don't save data twice
        saveUserData(true);
        //}
        successAnimation();

        $(".check-info .check-icon").html('/');
        $(".pagination .next").addClass('disabled');
        //$(".pagination .next").removeClass('disabled');
        console.log('Run validation using: ' + re);
        //$(".success-animation").show();	

        //console.log('checkpoint_id: '+checkpoint_id);


        if (slide_data.unlock_skills !== undefined) {
          unlockSkills(slide_data.unlock_skills);

          if (user_skills !== null) {
            user_skills.push(slide_data.unlock_skills);
            var unique_user_skills = [];
            $.each(user_skills, function (i, el) {
              if ($.inArray(el, unique_user_skills) === -1) unique_user_skills.push(el);
            });
            console.log("set unique unlockSkills: " + unique_user_skills);
            firebase.database().ref('/user_profile/' + auth_id + '/skills').set(unique_user_skills);

          } else {
            user_skills = {
              "0": slide_data.unlock_skills
            };
            console.log("set unlockSkills: " + user_skills);
            firebase.database().ref('/user_profile/' + auth_id + '/skills').set(user_skills);
          }
        }
      } else {
        $("#slide" + slide_id + " .check-icon").html('\\');
        $("#slide" + slide_id + " .actions li").removeClass('correct');
        $(".check-info .check-icon").html('\\');
        $(".check-info").removeClass('correct');
        $(".pagination .next").addClass('disabled');
      }
    }
    if (slide_data.js_function !== null) {
      console.log('js_function trigger: ' + slide_data.js_function);
      //var func = new Function("console.log('i am a new function')");
      var func = new Function(slide_data.js_function);
      func();
    }
  } else {
    console.log('no action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
  }
}

function progressUpdate() {
  var progressPercentage = checkpoint_count / checkpoint_id;
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).set({
    progressCompleted: progressPercentage
  });
}

function inactivityTime() {
  var t;
  window.onload = resetTimer;
  document.onload = resetTimer;
  document.onmousemove = resetTimer;
  document.onmousedown = resetTimer; // touchscreen presses
  document.ontouchstart = resetTimer;
  document.onclick = resetTimer; // touchpad clicks
  document.onscroll = resetTimer; // scrolling with arrow keys
  document.onkeypress = resetTimer;

  console.log("inactivityTime");
  resetTimer();

	var resumeCoding = [
		"You snooze, you loose.",
		"Well begun is only half done",
		"You are on your way to becoming a coding genius",
		"Did you know when you complete the lesson you will get a certificate?"
	]
	var rindex = Math.floor(Math.random() * resumeCoding.length); 
	
  function timedOut() {
		 if ( !$(".modal:visible").length ) {
    $('.modal').modal('hide');

    //console.log("timed out");
    $.createDialog({
      modalName: 'timed-out',
 			popupStyle: 'speech-bubble '+current_avatar+' surprised', 
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">'+resumeCoding[rindex]+'</h2>'+
			'\t<div id=\"confirm_actions\">'+
			'\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s continue coding</button>'+
			'\t</div>'+
			'</div>'	
    });
  }
  }
	
  function resetTimer() {

      //console.log("r timer: "+$(".modal").length);
      clearTimeout(t);
      t = setTimeout(timedOut, 60000)
     
		     // console.log("didn't reset timer"+$(".modal").attr('style').display == 'block' ));
  }
};

function closeModal(options) {
  console.log("close modal: " + options);
  $(options).modal('hide');
}

function onStart() {

  //TO DO: below not working to set kb layout using DB value
  //var customKB = lesson_data.kbLayout; 
  //initCodeEditor(customKB);

  initCodeEditor("< > /  =");
  editor.setValue(lesson_progress.user_code);

  buildSlides();

  console.log("start" + active_slide);
  //saveUserData(true);
  //Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip();

  //Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    var $target = $(e.target);

    if ($target.parent().hasClass('disabled')) {
      return false;
    }
  });

  //  Activate the Tooltips
  $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
    html: true,
    trigger: 'manual'
  }).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function () {
    $('#' + $(this).attr('id') + '.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
  });
  loadSkills();
  loadUserKey();

  setInterval(saveUserData, 10000);

  // Live preview
  editor.on('change', function () {
    clearTimeout(delay);
    //console.log('preview');
    delay = setTimeout(updatePreview, 300);
  });
  var lessonLoaded = false;

  //initFullPage();

  $("#fp-nav ul li:nth-child(2) a span").addClass('cp-green');
  $("#fp-nav ul li:nth-child(12) a span").addClass('cp-red');

  swiperH = new Swiper('.swiper-container-h', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowToXuchMove: false,
    pagiXnation: {
      el: '.swiper-pagination-h',
      clickable: true,
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () {

      },
      slideChange: function () {
        var slider = this;
				active_tab = slider.activeIndex;
        $('#lesson-navbar li.active').removeClass('active');
        var mindex = slider.activeIndex + 1;

        $('#lesson-navbar li:nth-child(' + mindex + ')').addClass('active');
        //console.log('#lesson-navbar:nth-child('+mindex+')');
        if (slider.activeIndex === 1) {
          $('.CodeMirror-code').focus();
          editor.focus();
          $('.ui-keyboard').show();
        } else {
          $('.CodeMirror-code').blur();
          $('.ui-keyboard').hide();

        }
        if (slider.activeIndex <= 0) {
          $(".success-animation").addClass('hide');
        }
        if (slider.activeIndex <= 3) {
          slider.allowSlideNext = true;
        } else {
          slider.allowSlideNext = false;
        }
          //console.log("tab: "+active_tab);			  
      },
    }

  });
  swiperV = new Swiper('.swiper-container-v', {
    direction: 'vertical',
    spaceBetween: 50,
    shortSwipes: true,
    mousewheel: true,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouXchMove: false,
    // Disable preloading of all images
    preloadImages: false,
    //initialSlide: active_slide,
    // Enable lazy loading
    lazy: true,
    pagination: {
      el: '.swiper-pagination-v',
      clickable: false,
    },
    observer: true,
    observeParents: true,

    on: {
      init: function () {

	  },
	  lazyImageReady: function() {
	    if (slide_data.lazy_function !== undefined) {
		  
		  var func = new Function(slide_data.lazy_function);
		  func();
		}
		console.log('lazy_function triggered: ' + slide_data.lazy_function);
      },
      slideChange: function () {
        var slider = this;
        //var mindex = slider.activeIndex+1;
        active_slide = slider.activeIndex;
        localStorage.setItem("active_slide", active_slide);
        slide_data = lesson_data.slides[active_slide];
        var unlocked = $("#slide" + (active_slide + 1)).hasClass("cp-unlocked");
        if (!unlocked && slide_data.action !== undefined) {
          slider.allowSlideNext = false;
          console.log('lock' + slider.allowSlideNext + active_slide);
        } else {
          slider.allowSlideNext = true;
          console.log('unlock' + active_slide);
        }
        //console.log("mindex: "+mindex);
        console.log("active_slide: " + active_slide);

        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')').prevAll().addClass('bullet-green');
        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')').next().removeClass('bullet-green');
        validateCheckpoint();
      },
    }
  });

  swiperTabs = document.querySelector('.swiper-container-h').swiper;
  swiperLesson = document.querySelector('.swiper-container-v').swiper;

  $('#lesson-navbar li').click(function () {
    if ($(this).index() === 5) {
      console.log(swiperH.activeIndex);
      // console.log($(this).index());
      swiperH.allowSlideNext = true;
      swiperH.slideTo($(this).index());
      swiperH.allowSlideNext = false;

    } else {
      //console.log("dfd"+swiperH.activeIndex);
      // console.log("dfd"+$(this).index());
      swiperH.slideTo($(this).index());
      $('#lesson-navbar li.active').removeClass('active');
      $(this).addClass('active')

    }
  })
  $('.swiper-pagination-switch').click(function () {
    swiperH.slideTo($(this).index() + 1);
    $('.swiper-pagination-switch').removeClass('active');
    $(this).addClass('active')
  })
  $('.swiper-next').click(function () {
    swiperLesson.allowSlideNext = true;
    swiperLesson.slideNext();
    //swiperLesson.allowSlideNext = false;
    console.log("skip");
  })
  $('.swiper-editor').click(function () {
    swiperTabs.allowSlideNext = true;
    swiperTabs.slideNext();
    //swiperTabs.allowSlideNext = false;
    console.log("skip2");
  })
  swiperLesson.slideTo(active_slide);
  //active_slide = 0;

  $('.reset-profile').click(function () {
    resetCurrentProject();
  });

  $("#profile_page .profile_name").append(", " + user_profile.first_name);

  $('.reset-lesson').click(function () {
		$('.modal ').remove(); 
    $.createDialog({
      modalName: 'reset-lesson',
			popupStyle: 'speech-bubble '+current_avatar+' scared', 
      actionButton: resetCurrentProject,
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">Are you sure you want to reset this lesson?</h2>'+
			'\t<div id=\"confirm_actions\">'+
			'\t\t<button id=\"close_button\" class=\"btn btn-primary light cancel\" datax-dismiss=\"modal\">Hmmm, maybe not</button>'+
			'\t\t<button id=\"action_button\" class=\"btn btn-primary light action reset-profile\">Yes</button>'+
			'\t</div>'+
			'</div>'					
    });
  });
  
  
  if (!localStorage.getItem("skipLessonIntro") ) {
	
	    $.createDialog({
      modalName: 'project-intro',
 			popupStyle: 'speech-bubble '+current_avatar+' surprised', 
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">Welcome to the world of coding</h2>'+
			'\t<p id=\"\">Things are about to get exciting as you learn how to build a personal website using HTML. </p>'+
			'\t<div id=\"confirm_actions\">'+
			'\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s start coding</button>'+
			'<label class=\"form-check-label\"><input id=\"skip-lesson-intro\" type=\"checkbox\" class=\"form-check-input\">Don\'t show again</label>'+			
			'\t</div>'+
			'</div>'	
    });
	    $('#skip-lesson-intro').click(function(){
			if ($('#skip-lesson-intro').is(':checked')) {
				localStorage.setItem("skipLessonIntro", 1) ;
			} else {
				localStorage.removeItem("skipLessonIntro") ;
			}
		});

	}

	var i;
	var current_year = new Date().getFullYear();
	function pad2(number) {
			 return (number < 10 ? '0' : '') + number
		
	}

	date_form = "<form lpformnum=\"1\" id=\"email-login\"><div ><input class=\"form-control\" placeholder=\"First name\" name=\"fname\" type=\"text\" required=\"\"><input class=\"form-control\" placeholder=\"Last name\" name=\"lname\" type=\"text\" required=\"\"></div><div><label style=\"    margin: 0;\">Date of birth</label></div>\n"+
	"<select name=\"day\" class=\"custom-select\" style=\"width: 28%\">\n"+
	"<option value=\"\" selected></option>\n";
	for (i = 1; i <= 31; i++) { 
			date_form += "<option value=\""+pad2(i)+"\">"+pad2(i)+"</option>\n";
	}
	date_form += "</select>\n"+
	"<select name=\"month\" class=\"custom-select\" style=\"width: 28%\">\n"+
	"<option value=\"\" selected></option>\n"+
	"<option value=\"01\">Jan</option>\n"+
	"<option value=\"02\">Feb</option>\n"+
	"<option value=\"03\">Mar</option>\n"+
	"<option value=\"04\">Apr</option>\n"+
	"<option value=\"05\">May</option>\n"+
	"<option value=\"06\">June</option>\n"+
	"<option value=\"07\">July</option>\n"+
	"<option value=\"08\">Aug</option>\n"+
	"<option value=\"09\">Sept</option>\n"+
	"<option value=\"10\">Oct</option>\n"+
	"<option value=\"11\">Nov</option>\n"+
	"<option value=\"12\">Dec</option>\n"+	
	"</select>\n"+
	"<select name=\"year\" class=\"custom-select\" style=\"width: 38%\">\n"+
	"<option value=\"\" selected></option>\n";
	for (i = current_year; i >= 1950; i--) { 
			date_form += "<option value=\""+i+"\">"+i+"</option>\n";
	}
	date_form += "</select>\n";
	date_form += "</div><div id=\"error\"></div></form>\n";

	$('.login-button').click(function () {
    loginModal();
  });
	
	$('.register-button').click(function () {
		registerModal()
  });

    $(".select_lego").click(function(){changeAvatar("lego")});
	$(".select_robot").click(function(){changeAvatar("robot")});
	


	updatePreview();	
	// temp disable inactivityTime();
	//$("#lesson-navbar").click(function(){
	
//drawArrow("slide-25-p", "slide-25-img", [[ .3, 1.1, -1, 1 ], [ .1, .75, -1, 1  ]]);
//drawArrow("p-opening", "p-tags", [[.5, 1.1, 1, 1],[.2, .4, 0, 1]]);
//drawArrow("p-closing", "p-tags", [[.5, 1.1, 1, 1],[.2, .4, 0, 1]]);
//drawArrow("unlocked-skills", "menu-skills", [[.5, 1.1, 1, 1],[.2, .4, 0, 1]])
	// });
}

	
function loginModal() {
	$('.modal ').remove(); 
	$.createDialog({
      modalName: 'login-modal',
			popupStyle: 'speech-bubble top-align '+current_avatar+' happy', 
      //actionButton: resetCurrentProject,
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">Please enter your details below to login</h2>'+
			date_form+
			'\t<div id=\"confirm_actions\">'+
			'\t\t<button id=\"close_button\" class=\"btn btn-primary light cancel\" datax-dismiss=\"modal\">Mabye later</button>'+
			'\t\t<input id=\"action_button login-button\" class=\"btn btn-primary light action\" type=\"submit\" value=\"Sign In\">'+
			'\t</div>'+
			'<p class=\"sign-up register-button\">Dont have an account? <span class=\"a-link register-button\">Sign Up</span></p>'+
			'</div>'					
    });
		$('.register-button').click(function () {
			registerModal()
		});
}

function drawArrow(start_id, end_id, dynamicAnchors, acolor = "red-line") {
	
	jsPlumb.ready(function(){
			jsPlumb.setContainer("container");
			var link = jsPlumb.getInstance();

			link.connect({
			   cssClass: acolor,
				source: start_id,
				target: end_id,
				anchors: dynamicAnchors, 
				endpoint: "Blank",
				overlays: [
					["Arrow", {
						location: 1,
						width: 10,
						length: 10
					}]
				],
				paintstyle: {
					lineWidth: 40,
					strokeStyle: "red",
					// dashstyle:" 0 1"
				},
				connector: ["Bezier", {
					curviness: 80
				}]
			});
		});	
	
}

function registerModal() {
	//console.log("reg");
	$('.modal ').remove(); 
	$.createDialog({
		modalName: 'register-modal',
		popupStyle: 'speech-bubble top-align '+current_avatar+' happy', 
		actionButton: registerNextModal,
		htmlContent: 
		'<div id=\"confirm_dialog\" style=\"display: block;\">'+
		'\t<h2 id=\"confirm_title\">Registration is soooo easy. We just need a few details to get started</h2>'+
		date_form+
		'\t<div id=\"confirm_actions\">'+
		'\t\t<button id=\"close_button\" class=\"btn btn-primary light cancel\" datax-dismiss=\"modal\">Mabye later</button>'+
		'\t\t<input id=\"action_button\" class=\"btn btn-primary light action\" type=\"submit\" value=\"Sign up\">'+
		'\t</div>'+
		'<p class=\"sign-up login-button\">Already have an account ? <span class=\"a-link\">Sign In</span></p>'+
		'</div>'					
	});
	$('.login-button').click(function () {
		loginModal();
	});
}

function registerNextModal() {
	console.log("add code here to save user and then proceed to collect addtional data");	
	
	var register_addtional = '<div id=\"confirm_dialog\" style=\"display: block;\">'+
		'\t<h2 id=\"confirm_title\">Congrats you have successfullly registered.</h2>'+
		'<p style=\"margin-bottom: 8px;\">Please could you add in few more details about yourself so we can stay in touch.</p>'+		
		'<form lpformnum=\"1\" id=\"email-login\"><div >\n'+
		"<input class=\"form-control\" placeholder=\"Mobile number\" name=\"mobile\" type=\"text\" required=\"\" style=\"width: 100%\">\n"+		
		"<input class=\"form-control\" placeholder=\"Email address\" name=\"email\" type=\"text\" required=\"\" style=\"width: 100%\">\n"+		
		"<input class=\"form-control\" placeholder=\"ID number\" name=\"id_number\" type=\"text\" required=\"\" style=\"width: 100%\">\n"+				
		"<select name=\"country\" class=\"custom-select\" style=\"width: 44%\">\n"+
		"<option value=\"\" selected>Country</option>\n"+
		"<option value=\"ZA\">South Africa</option>\n"+
		"<option value=\"MZ\">Mozambique</option>\n"+
		"<option value=\"NM\">Nambia</option>\n"+
		"<option value=\"ZM\">Zambia</option>\n"+
		"<option value=\"ZI\">Zimbabwe</option>\n"+	
		"</select>\n"+
		"<input class=\"form-control school\" placeholder=\"Name of School\" name=\"school\" type=\"text\" required=\"\" style=\"width: 54%\">\n"+	
		"</div><div id=\"error\"></div></form>\n"+
		'\t<div id=\"confirm_actions\">'+
		'\t\t<button id=\"close_button\" class=\"btn btn-primary light cancel\" datax-dismiss=\"modal\">Skip</button>'+
		'\t\t<input id=\"action_button\" class=\"btn btn-primary light action\" type=\"submit\" value=\"Save\">'+
		'\t</div>'+
		'</div>'		
	
	$('#register-modal .modal-body').html(register_addtional);
	
	$('#close_button, .modal-backdrop').click(function () {
		$('#register-modal').modal('hide'); 
		$('.modal , .modal-backdrop').remove(); 
		console.log('closed #register-modal');
	});
}

function changeAvatar(avatar) {
	$(".modal-content").removeClass(current_avatar).addClass(avatar);
	$(".select_robot, .select_lego").toggleClass("option_selected option_unselected");
	current_avatar = avatar;
	successAnimation();
}


function initFirebase() {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
  }

  firebase.initializeApp(config);
}

function getLessonDataPromise() {

  return firebase.database().ref('/lessons/' + lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonDataPromise");
}

function getUserProfilePromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/profile').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserProfilePromise");
}

function getUserSkillsPromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/skills').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserSkillsPromise");
}

function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonProgressPromise");
}
function getGalleryData(){
  return firebase.database().ref('/user_profile/').once('value').then(function (snapshot){
    var data = snapshot.val()
    var keys = Object.keys(data)
    // console.log(keys,'1')

    
    var userCodeHtml2 = ''

    for( i= 0; i <= 9; i++){
        var k = keys[i];
        // console.log(k,"10");
        var userCodeURL = "https://www.codejika.com/preview/5-minute-website?" + k;
        userCodeHtml2 += ` 
        <div id = "userKey" onclick="${userCodeURL}">  
          <a href="${userCodeURL}"><div class="card-blog" id="cardHTML">
            <h3>title</h3>
            <div class="HTMLscreen" >
              <iframe src="${userCodeURL}" scrolling="no"></iframe>
            </div>
          </a>
            <div class="detail-blog">
            <h4>User Name</h4>
            <div class="d-flex view-section">
              <div class="likes">
                  <button onclick="likes()"><span><i class="far fa-thumbs-up"></i></button><small>124</small></span>
              </div>
              <div class="views">
                <span><i class="fas fa-eye"></i> <small>84</small></span>
              </div>
            </div>
            </div>
          </div>
         
        </div>`

        // document.getElementById('galleryContainer').innerHTML = userCodeHtml2;
    }

    return snapshot.val();
  }, function(error){
    console.log(error,'galleryError');
  });
}



$(document).ready(function () {

  initFirebase();

  var getLessonData = getLessonDataPromise();
  var getUserProfile = getUserProfilePromise();
  var getUserSkills = getUserSkillsPromise();
  var getLessonProgress = getLessonProgressPromise();
  var getGallery = getGalleryData();


  Promise.all([getLessonData, getUserProfile, getUserSkills, getLessonProgress,getGallery]).then(function (results) {
    lesson_data = results[0];
    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 

    user_profile = results[1];
    //str = JSON.stringify(user_profile, null, 4);
    // console.log("user_profile:\n"+str); 

    user_skills = results[2];
    //str = JSON.stringify(user_skills, null, 4);
    //console.log("user_skills:\n"+str); 

    lesson_progress = results[3];

    //if no lesson data save default values to DB
    if (lesson_progress === null) {
      console.log("no lesson progress found");
      lesson_progress = {
          "last_checkpoint": "0",
          "user_code": lesson_data.defaultCode,
          "progress_completed": "0"
        }
        //saveUserData(true);
      console.log("default lesson_progress inserted");
    }

    //str = JSON.stringify(lesson_progress, null, 4);
    //console.log("default lesson_progress:\n"+str); 

    // console.log(getGallery,'firebase')

    console.log("promise all is true, now running onStart");
    onStart();
  });


});
//  ============ end of document.ready firebaseinit()

function likes(){
 
  console.log('Likes')
};