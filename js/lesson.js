var save_lesson_id = "P001-L01";
var auth_id = localStorage.getItem("userid");
console.log("auth_id: " + auth_id);
if (auth_id == null) {
  auth_id = Math.random().toString(36).replace('0.', '');
  localStorage.setItem("userid", auth_id);
}

var lessson_url = "nomzamos-website-01.php";

var checkpoint_id = 0;

function findIndexOfKeys(keys, value) {
  var index = 1
  for (var key in keys) {
    if (value == key) {
      return index
    } else {
      index++
    }
  }
}

///lazy loading images
$(function () {
  const observer = lozad(); // lazy loads elements with default selector as '.lozad'
  observer.observe();
});

(function ($) {
  $.createDialog = function (options) {
    $(".modal , .modal-backdrop").remove();
    // console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class="modal fade" id="' +
      options.modalName +
      '">\n' +
      '<div class="modal-dialog ' +
      options.popupStyle +
      '">\n' +
      '<div class="modal-content">\n' +
      '<div class="modal-body">\n' +
      options.htmlContent +
      "</div>\n" +
      "</div>\n" +
      "</div>\n" +
      "</div>\n"
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function (e) {
      $("#" + options.modalName).modal("hide");
      $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display", 'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      // console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);

// thumnail modal
(function ($) {
  $.galleryDialog = function (options) {
    $(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class=" fade" id="' +
      options.modalName +
      '">\n' +
      '<div class="' +
      options.popupStyle +
      '">\n' +
      '<div class="modal-content">\n' +
      '<div class="">\n' +
      options.htmlContent
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function (e) {
      $("#" + options.modalName).modal("hide");
      $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display", 'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);

//========================== Login page
$('.create-profilePage').click(function () {
  dialogInMenu = true
  checkLoggedInAndGetUserDetails().then(function(user_details){ createProfilePage(user_details)}).catch(function(err){
    console.log(err, "Here is the error");
     UnRegisteredModal()})
})


function createProfilePage(user_details) {

  console.log("Inside createProfilepage", user_details);
  let ProfileFName;
  let ProfileLName;
  $.galleryDialog({
    modalName: 'createProfilePage',
    htmlContent: `
    <div  id="user_profile" style="height:${screenHeight}; padding: 23px;overflow-y: scroll;">
      <div class="container">
          <i class="icon-account_circle fs6" style="padding: 30px 0px 10px 0; display: block;"></i>
          <div>
            <input class="form-control" id="pf_name" placeholder="First name" name="fname" type="text" required="" style="width:45%;display:inline-block" value="${user_details.first_name ? user_details.first_name : ''}">
            <input class="form-control" id="pl_name" placeholder="Last name" name="fname" type="text" required="" style="width:45%;display:inline-block" value="${user_details.last_name ? user_details.last_name : ''}">
            <div class="text-left ml-4"><label style="margin: 0;">Aspiring:</label></div>
            <div class="mb-3"><textarea class="form-control m-auto" id="aspiringContent" placeholder="Dream Job" type="text" cols="4" style="width:90%" >${user_details.details ? user_details.details.aspiring ? user_details.details.aspiring : '' : ''}</textarea></div>
            <input class="form-control" id="Pgrade" placeholder="Grade" name="Grade" type="text" required="" style="width:45%;display:inline-block" value="${user_details.details ? user_details.details.grade ? user_details.details.grade : '' : ''}">
            <input class="form-control" id="PConuntry" placeholder="Country" name="country" type="text" required="" style="width:45%;display:inline-block" value="${user_details.details ? user_details.details.country ? user_details.details.country : '' : ''}">
            <button id="action_button" class="btn btn-primary light action ">Save</button>
          </div>
          <br><br>
          <div class="container">
            <h2 class="profile_name mt-4">Certificates:</h2>`
            +
            `<hr>`
            +`
            <div class="certificates-blog" id="certificateBlog">
              ${user_details.certificate ? prepareCertificates(user_details) :'<p>Please complete the Course to get the certificates</p>'}
            </div>`+
            `<hr>`+
          `
          </div>
      </div>
    </div>
    `
  })
  // let hideSaveBtn = document.getElementById('pf_name').value
  $('.action').click(function (){
    let ProfileFName = document.getElementById('pf_name').value;
    let ProfileLName = document.getElementById('pl_name').value;
    let Aspiring = document.getElementById('aspiringContent').value;
    let grade = document.getElementById('Pgrade').value;
    let p_country = document.getElementById('PConuntry').value

    // store to users
    var ref = firebase.database().ref('/users/' + auth_id)

    ref.update({
      first_name : ProfileFName,
      last_name: ProfileLName,
      'details/country' : p_country,
      'details/aspiring': Aspiring,
      'details/grade': grade
    })

    document.getElementById('action_button').style.display = 'none';
    successProfileDetails('Profile Updated', 'Thank you for updating your profile')
  })
  console.log("Completed task");

}
function prepareCertificates(user_details) {
  var sectionCertificate = ''
  var certificates = Object.keys(user_details.certificate)
  certificates.map(element => {
    sectionCertificate +=`<h6 class="mt-3">${element}</h6>
        <div class="certificates-container">
          <a class="certificate-card Card_pdf_${element}" onclick="exportPdfCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}',showCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}'))">
            <div class="certificate"> certificate PDF </div>
            <strong style="display-block">PDF</strong>
          </a>
          <a class="certificate-card Card_html_${element}" onclick="openHtmlCertificate(showCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}'))">
            <div class="certificate"> certificate html </div>
            <strong style="display:block">HTML</strong>
          </a>
        </div>`
  });
  return sectionCertificate
}

function openHtmlCertificate(pageContent) {
  console.log("Inside ", 'openHtmlCertificate');
  subModal = true
  $.nestedModal1({
    modalName: 'HtmlCertificate',
    htmlContent:pageContent,
    closeExisting:false
  })
}

function exportPdfCertificate(first_name, last_name, certificate_key, certificate_time){
  window.event.preventDefault()
 var docDefinition = {
    	content: [
	
      {
        image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAqALADASIAAhEBAxEB/8QAHgAAAQQDAQEBAAAAAAAAAAAACQAFBwgEBgoDAgH/xABTEAABAwMDAgIEBggPEQAAAAABAgMEBQYRAAcIEiEJExQxQVEVFiJhcbMXGSMyN1didhgkMzY4OUJ1gZGVlrTS0zVHUlNWWGVmcnN0d4OjsbLU/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ABb0ih1q4JYgUGjzalJI6gzEjrecx7+lIJ051PbvcCiQ11Gs2NcMCK0MrflUx9ptP0qUkAaNNx4l2Hwt8OWmb50ayWanUHrdh3HVfKUlmRU5UxxsNoceKVEIR5yEjsQlKCQCSc4vDbxLE8t91JO0Fd2cZt7z6TInNSW6t6a24GykLacbUyjsUrPfJ9WCO+QAQ9PlGsW97ii+nW/Z1cqcbJT50OnvPIyPWOpCSM6u3vRxg21j+KVRtlqfR2odm3NV6dVH6WwPLaQ06z58iOgDHQ2tTbgATjpSvAxgavHzT520zg3ULP28tTZ+HWm6nTXJLTLU8U6LCjtr8tDbaENLz3CuwCQAB689gBvW7YuS2nUMXHb1TpTjoyhE2I4wpQ94CwM6VDtm5LnfcjW3b9SqzzKPMcbgxHH1ITnGSEAkDPt0dzbrcKxvEh4f3LVb124YpLby6hSjFefTLMKayylbUqO8UJKVJ8xtQPSDkKScj11B8ED8I+6A/wBCQPr16AfP2Jt1B/ezuv8AkaT/AFNa3MhTKdKchVCI9GkMq6XGXmyhaD7ik9wdGC348XWo7J733XtOrYmNV4lr1VVPVOFwqZcfQnGVhv0dQSe/q6j9OvvxcdubDvnjTbfISFQ2YlxRZ1PDc0NpS+9AltKPkOkff9Ki2pOSekhWPvjkBI0ewb6uGEKlQLKr1SiKUUCRDprzzZUPWOpCSMjWd9ibdT8Wd1/yNJ/qaM74cV1rsXw5Y17JhCaq3o9x1VMYudAeLD8h3o6sHp6unGcHGfUdV7+3kV//ADcKf/Ohf/y6AXzzL0d5ceQ0tp1pRQtC0lKkqBwQQe4IPs18afb9ut2+76uO+H4aIjlw1aZVVx0LK0sqfeU6UBR7kArxn240xaBaK8x4WW22/wDxl213J2wqvxLvmp2fS5cwO9b1NqchUVsrW6jutlajklbeRkklsk50KHXSFwyV1cS9oCf8jKSP4oyNBzw7jbf3JtZfVe27u1hlqsW5Pdp05LDodbDyCQelY7EHGQfdr425hQ6luFa9OqEZuRFlVmEw+y4MpcbU+gKSR7iCQdFq4w7f2VuVza5c2buBa1Or9FnTmA/Cnx0utq/TDuFAH71QzkKGCD3BB1BHNTglt9xY3K2z3B2wr1QFDua8IkH4Dm/djBcS4hzLb+epTeAR0rBUO3ylewJM5aeD/RnY1Sv/AIy1humKZQ5KkWtVHyWCkZUoRZCsqR2Bwh3I/LSO2hP66kru/WpWv3uk/Vq1zE2NZdx7jXjRrDtGnrnVmvzWqfCjp/duuKCRk+xIzkk9gASew0DHpamfkTxD304wVcwtzbScTS3XC3DrsDL9Nle4JeAHQr8hwJX82O+pA4FcJZnMG8K0it1efQrPtyKFTqnEbSp1yW5+ox2+sFOcBa1HBwlIHYrSdBVjS0XS6fBK28btuqO2bu/cztcREeVTmp8eP6O5JCCW0OFKQoIKsAkdwDnvjGhMVyiVa2q1Ptyv096DU6XJdhzIryelxh9tRQtCh7CFAg/RoDObvftN0H8wLb/pEPQbrTqV30mstzLGqFYh1YIUlt2kuutyAkj5QBaIVjHr+bRstj6FbHMnw06Ts5a14xqfONtw7dnu9AeXT50JxtQS80CCErLCFDuMocBGmDhF4aNz8Vt43N2br3QpNbQzSZNPjw4MFxvKnSjK1rcPYBKT2A7kjuMdwHlwfqN21XnVtfMvefV5lXVWAHnaq665JIEZzpClOkqxjGM+zGrCeN1+GHbr82n/AOlK0x72cittB4rlF3Vj12M/als1em0iZVWlhTA8tjyH3godlIbW4vKhkENkjIxq5PO/gVVuaFds++rL3NpNH+CaY5DIkxlSGJTLiw626242r51ewgggg+8NZ8I39hRcv5zVf+hxtQL4IH4R90P3kgfXr1cDZna2g+Hhw6umn7jX9BqTcNdRrMiWhox23JDzKG2orKVEqWtXlISn2lSj2xqoHggA/ZH3QOO3wJA+vXoKk88/2Yu7X5yP/wDhOiYeJL+1z0D/AGrb+qGtG5B+Ejf+9e+t37qQ94LfpdPuirLnoju05915htWOxwoJURj3gH5tbJ4t95WfYHFS2NjhW2pNenzqciJFKx55hw2lBclaR96kqDaR7yo4z0nAbv4bPxW+13Qvj15fxb8q4vhjzevo9B9Ikef1dHyseX156e/u76gXck+DV9jq6PiUKH8YfgWb8EeSK95npvkL8jp6/k58zpx1dvf21Ofh0WrJvjw4GbKiy24j9wxbkpbT7iSpDSn35DQWQO5AKskD3arH9pD3U/Hhan8nydANbS093xa0qxb1uCyZ0lqRJt6qS6U880CEOLYdU2pSc98EoJGe+DrAo9IqVwVaFQqNDXLn1GQ3FisIx1OurUEpSM9skkDQYeuj/hWeriRtCf8AU6mD/sJ1RDip4Orjnod58p6mW0/JdbtKlyPlH8mXJQe3zoZP/UHcaJi4/t3svYLSZMqj2jaFswkMNqedRGiQozaQlKcqIAAAAA9Z+c6ClfC8Y8QPlb/xsb65zX74s39zNi/+Ykb/ANRqMOG3JvYiNzs35uKduNToFJ3DmMptudOC40ecpDqsgLcADZPUOkL6er2d+2pO8WRSV0rYpaFBSVbhxiCDkEdI0F5bu/WpWv3uk/Vq0MfwdOK3kszuU95U35boepVpIdR6k90SpifpOWUn3B73g6Jvd4zadaBz3p0n1f7pWqs+GHyHoO9nGyj2q2zDg3Dt1HYoFShR0BtJZQjEaSlI9QcQkhR/xiHPm0Db4m2+cy0drIPHyw4DdXv/AHgeTQ6fTw0l1bcRa0odd6VZAUpSktIJxgqUsHLepv4n8eqHxj2PoG1tLDL05hv0ytTW049NqLgBed9/SCAhGe4QhA9mqkc96BWeOXKLbPnhBpz1ft6C8zQLjhPjzvQEFLjaXWAr9T62nXekjADyAScunUleIbzIouz3GyLL23uJmTcO6MLyLblRXMluA62lT09J9Yw24lKD6wt1B/cnQT1sryT2u38rF70TbyselybDrSqNUArpw6QPkyGsE9TKlpdQlXtLSjjGCRr+MLxW+LF0w+Tlm03pplxOIp9zNtJ+SxPCcMySB6g6hPQo+rrQCclzVTOFHJaocXN+aNfq3Xl29OPwZccVGT51PcUOpYT7VtqCXU+0lHTnCjoiPO7xI+OMjbq5NkLKpsfdCVcUBcKW+y8UUqH1AFLgfHd11CulaQ12CkjKwRjQCOta97zsaYuo2Td1at+W4kIW/Sp7sRxSfcVNqSSNP9X333wuCnvUivby3zUoMhJQ9FmXFMeacSfWFIW4QR9I1o2loFrcrd3o3itCmoo1p7s3lRKe195Ep1elRmU/QhtYSP4tabpaDYrs3I3Ev3yRfV+3Hcfo5yz8LVR+Z5Z96fNUrH8GvO0r/vuwJL82xL1r1tyJTYafepFSehrdQDkJUppSSoZ74OmHS0Ei/okeRH4+9xv50zv7XWkVuvVy5ak7WbjrM6q1B85dlzZC33nD+UtZKj/CdYOloNxtrebeCy6Sig2duteNCpjSlLRCpldlRWEqUcqIbbWEgk9ycd9On6JHkT+Pvcb+dM7+11HWloPWXLl1CW9PnynZMmS4p5555ZW464o5UpSj3USSSSe5J15aWloLZ8avEt5D8doKLaeqDV72uy0W41Krrq1qiHB6fIkA+YhIOPuZ6kYBACSc6ijkFyq3t5NV34X3Uu96VEZcK4VHi5Zp0LPb7kwDjOO3WoqWfao6iPS0C1JUDkHuc5RrQs26rrqVdtWzK5HrdMpkt4OGMtsjLbLigVoQU5HRnoB7hOc5jXS0HQxstzY2H5S2NVmbCuT0K4xSpC5Nt1PpZqDRDJKihOSHkD/DbKgO2ek9tBh4Tclp/FvfmjX4468u3Zx+C7jioyfNp7ih1LCfatpQS4n2ko6fUo6gyDOnUyW1Ppsx+JKYV1tPsOFtxtXvSoYIPzjXhoOi/lduXxoi7B1eBvve1LYtK86SpuKlpwPSZyHEBbTsNpOVOLSehxCgOlJCSSB31z13PdNbuAU+lVC46hVqbbsdVMovpZI9HhB5xxKEoyQgFTi1dIJwVEZOBrAqVZq9ZMY1eqzJxhx24cYyX1O+Sw2MIaR1E9KEjsEjsPYNYegWlpaWg//Z',
        style: 'logo'
      },
	 	  {
		    text: 'Certificate',
		    style: 'header'
	    },
      {
        text: 'Congratularions '+ `${first_name} ` + `${last_name}`+ ' for succefull completion of ', 
        style: 'content'
      },
      {
        text: `${certificate_key}` + ' on ' + `${certificate_time} \n\n`,
        style: 'content'
      },
		{image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAqALADASIAAhEBAxEB/8QAHgAAAQQDAQEBAAAAAAAAAAAACQAFBwgEBgoDAgH/xABTEAABAwMDAgIEBggPEQAAAAABAgMEBQYRAAcIEiEJExQxQVEVFiJhcbMXGSMyN1didhgkMzY4OUJ1gZGVlrTS0zVHUlNWWGVmcnN0d4OjsbLU/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ABb0ih1q4JYgUGjzalJI6gzEjrecx7+lIJ051PbvcCiQ11Gs2NcMCK0MrflUx9ptP0qUkAaNNx4l2Hwt8OWmb50ayWanUHrdh3HVfKUlmRU5UxxsNoceKVEIR5yEjsQlKCQCSc4vDbxLE8t91JO0Fd2cZt7z6TInNSW6t6a24GykLacbUyjsUrPfJ9WCO+QAQ9PlGsW97ii+nW/Z1cqcbJT50OnvPIyPWOpCSM6u3vRxg21j+KVRtlqfR2odm3NV6dVH6WwPLaQ06z58iOgDHQ2tTbgATjpSvAxgavHzT520zg3ULP28tTZ+HWm6nTXJLTLU8U6LCjtr8tDbaENLz3CuwCQAB689gBvW7YuS2nUMXHb1TpTjoyhE2I4wpQ94CwM6VDtm5LnfcjW3b9SqzzKPMcbgxHH1ITnGSEAkDPt0dzbrcKxvEh4f3LVb124YpLby6hSjFefTLMKayylbUqO8UJKVJ8xtQPSDkKScj11B8ED8I+6A/wBCQPr16AfP2Jt1B/ezuv8AkaT/AFNa3MhTKdKchVCI9GkMq6XGXmyhaD7ik9wdGC348XWo7J733XtOrYmNV4lr1VVPVOFwqZcfQnGVhv0dQSe/q6j9OvvxcdubDvnjTbfISFQ2YlxRZ1PDc0NpS+9AltKPkOkff9Ki2pOSekhWPvjkBI0ewb6uGEKlQLKr1SiKUUCRDprzzZUPWOpCSMjWd9ibdT8Wd1/yNJ/qaM74cV1rsXw5Y17JhCaq3o9x1VMYudAeLD8h3o6sHp6unGcHGfUdV7+3kV//ADcKf/Ohf/y6AXzzL0d5ceQ0tp1pRQtC0lKkqBwQQe4IPs18afb9ut2+76uO+H4aIjlw1aZVVx0LK0sqfeU6UBR7kArxn240xaBaK8x4WW22/wDxl213J2wqvxLvmp2fS5cwO9b1NqchUVsrW6jutlajklbeRkklsk50KHXSFwyV1cS9oCf8jKSP4oyNBzw7jbf3JtZfVe27u1hlqsW5Pdp05LDodbDyCQelY7EHGQfdr425hQ6luFa9OqEZuRFlVmEw+y4MpcbU+gKSR7iCQdFq4w7f2VuVza5c2buBa1Or9FnTmA/Cnx0utq/TDuFAH71QzkKGCD3BB1BHNTglt9xY3K2z3B2wr1QFDua8IkH4Dm/djBcS4hzLb+epTeAR0rBUO3ylewJM5aeD/RnY1Sv/AIy1humKZQ5KkWtVHyWCkZUoRZCsqR2Bwh3I/LSO2hP66kru/WpWv3uk/Vq1zE2NZdx7jXjRrDtGnrnVmvzWqfCjp/duuKCRk+xIzkk9gASew0DHpamfkTxD304wVcwtzbScTS3XC3DrsDL9Nle4JeAHQr8hwJX82O+pA4FcJZnMG8K0it1efQrPtyKFTqnEbSp1yW5+ox2+sFOcBa1HBwlIHYrSdBVjS0XS6fBK28btuqO2bu/cztcREeVTmp8eP6O5JCCW0OFKQoIKsAkdwDnvjGhMVyiVa2q1Ptyv096DU6XJdhzIryelxh9tRQtCh7CFAg/RoDObvftN0H8wLb/pEPQbrTqV30mstzLGqFYh1YIUlt2kuutyAkj5QBaIVjHr+bRstj6FbHMnw06Ts5a14xqfONtw7dnu9AeXT50JxtQS80CCErLCFDuMocBGmDhF4aNz8Vt43N2br3QpNbQzSZNPjw4MFxvKnSjK1rcPYBKT2A7kjuMdwHlwfqN21XnVtfMvefV5lXVWAHnaq665JIEZzpClOkqxjGM+zGrCeN1+GHbr82n/AOlK0x72cittB4rlF3Vj12M/als1em0iZVWlhTA8tjyH3godlIbW4vKhkENkjIxq5PO/gVVuaFds++rL3NpNH+CaY5DIkxlSGJTLiw626242r51ewgggg+8NZ8I39hRcv5zVf+hxtQL4IH4R90P3kgfXr1cDZna2g+Hhw6umn7jX9BqTcNdRrMiWhox23JDzKG2orKVEqWtXlISn2lSj2xqoHggA/ZH3QOO3wJA+vXoKk88/2Yu7X5yP/wDhOiYeJL+1z0D/AGrb+qGtG5B+Ejf+9e+t37qQ94LfpdPuirLnoju05915htWOxwoJURj3gH5tbJ4t95WfYHFS2NjhW2pNenzqciJFKx55hw2lBclaR96kqDaR7yo4z0nAbv4bPxW+13Qvj15fxb8q4vhjzevo9B9Ikef1dHyseX156e/u76gXck+DV9jq6PiUKH8YfgWb8EeSK95npvkL8jp6/k58zpx1dvf21Ofh0WrJvjw4GbKiy24j9wxbkpbT7iSpDSn35DQWQO5AKskD3arH9pD3U/Hhan8nydANbS093xa0qxb1uCyZ0lqRJt6qS6U880CEOLYdU2pSc98EoJGe+DrAo9IqVwVaFQqNDXLn1GQ3FisIx1OurUEpSM9skkDQYeuj/hWeriRtCf8AU6mD/sJ1RDip4Orjnod58p6mW0/JdbtKlyPlH8mXJQe3zoZP/UHcaJi4/t3svYLSZMqj2jaFswkMNqedRGiQozaQlKcqIAAAAA9Z+c6ClfC8Y8QPlb/xsb65zX74s39zNi/+Ykb/ANRqMOG3JvYiNzs35uKduNToFJ3DmMptudOC40ecpDqsgLcADZPUOkL6er2d+2pO8WRSV0rYpaFBSVbhxiCDkEdI0F5bu/WpWv3uk/Vq0MfwdOK3kszuU95U35boepVpIdR6k90SpifpOWUn3B73g6Jvd4zadaBz3p0n1f7pWqs+GHyHoO9nGyj2q2zDg3Dt1HYoFShR0BtJZQjEaSlI9QcQkhR/xiHPm0Db4m2+cy0drIPHyw4DdXv/AHgeTQ6fTw0l1bcRa0odd6VZAUpSktIJxgqUsHLepv4n8eqHxj2PoG1tLDL05hv0ytTW049NqLgBed9/SCAhGe4QhA9mqkc96BWeOXKLbPnhBpz1ft6C8zQLjhPjzvQEFLjaXWAr9T62nXekjADyAScunUleIbzIouz3GyLL23uJmTcO6MLyLblRXMluA62lT09J9Yw24lKD6wt1B/cnQT1sryT2u38rF70TbyselybDrSqNUArpw6QPkyGsE9TKlpdQlXtLSjjGCRr+MLxW+LF0w+Tlm03pplxOIp9zNtJ+SxPCcMySB6g6hPQo+rrQCclzVTOFHJaocXN+aNfq3Xl29OPwZccVGT51PcUOpYT7VtqCXU+0lHTnCjoiPO7xI+OMjbq5NkLKpsfdCVcUBcKW+y8UUqH1AFLgfHd11CulaQ12CkjKwRjQCOta97zsaYuo2Td1at+W4kIW/Sp7sRxSfcVNqSSNP9X333wuCnvUivby3zUoMhJQ9FmXFMeacSfWFIW4QR9I1o2loFrcrd3o3itCmoo1p7s3lRKe195Ep1elRmU/QhtYSP4tabpaDYrs3I3Ev3yRfV+3Hcfo5yz8LVR+Z5Z96fNUrH8GvO0r/vuwJL82xL1r1tyJTYafepFSehrdQDkJUppSSoZ74OmHS0Ei/okeRH4+9xv50zv7XWkVuvVy5ak7WbjrM6q1B85dlzZC33nD+UtZKj/CdYOloNxtrebeCy6Sig2duteNCpjSlLRCpldlRWEqUcqIbbWEgk9ycd9On6JHkT+Pvcb+dM7+11HWloPWXLl1CW9PnynZMmS4p5555ZW464o5UpSj3USSSSe5J15aWloLZ8avEt5D8doKLaeqDV72uy0W41Krrq1qiHB6fIkA+YhIOPuZ6kYBACSc6ijkFyq3t5NV34X3Uu96VEZcK4VHi5Zp0LPb7kwDjOO3WoqWfao6iPS0C1JUDkHuc5RrQs26rrqVdtWzK5HrdMpkt4OGMtsjLbLigVoQU5HRnoB7hOc5jXS0HQxstzY2H5S2NVmbCuT0K4xSpC5Nt1PpZqDRDJKihOSHkD/DbKgO2ek9tBh4Tclp/FvfmjX4468u3Zx+C7jioyfNp7ih1LCfatpQS4n2ko6fUo6gyDOnUyW1Ppsx+JKYV1tPsOFtxtXvSoYIPzjXhoOi/lduXxoi7B1eBvve1LYtK86SpuKlpwPSZyHEBbTsNpOVOLSehxCgOlJCSSB31z13PdNbuAU+lVC46hVqbbsdVMovpZI9HhB5xxKEoyQgFTi1dIJwVEZOBrAqVZq9ZMY1eqzJxhx24cYyX1O+Sw2MIaR1E9KEjsEjsPYNYegWlpaWg//Z',
     width: 70,
     margin: [20,0,0,5]
     },
		{
			columns: [
				{
          type: 'none',
          lineHeight: 1.5,
					ul: [
						'Academic Director',
					]
				},
				{
          type: 'none',
          lineHeight: 1.5,
					ul: [
						`Hours: ${certificate_key}: 2hours`,
            `Difficulty; ${certificate_key}: Explorer.`
					]
				}
			],
      alignment: 'justify'
		},
	],
	styles: {
    logo: {
      alignment: 'center',
      margin: [0,20,0,40]
    },
		header: {
			fontSize: 22,
			bold: true,
			alignment: 'center',
      margin: [0, 40, 0, 40],
		},
		subheader: {
			fontSize: 15,
			bold: true
		},
		quote: {
			italics: true
		},
		content: {
			fontSize: 16,
      alignment: 'center',
      lineHeight: 1.5,
		},
    footer: {
      // alignment: 'center',
      margin: [25,0]
    }
	}
  };
  pdfMake.createPdf(docDefinition).open();
  // pdfMake.createPdf(docDefinition).download('certificate');

}
//===================== ends



var lesson_mappings = {
   "coding1-t1": "P1Training1",
  "coding1-t2": "P1Training2",
  "coding1-t3": "P1Training3",
  "coding1-t4": "P1Training4",

  "coding2-t1": "P2Training1",
  "coding2-t2": "P2Training2",
  "coding2-t3": "P2Training3",
  "coding2-t4": "P2Training4",
  "coding2-t5": "P2Training5",
  "coding2-t6": "P2Training6",

  "coding3-t1": "P3Training1",
  "coding3-t2": "P3Training2",
  "coding3-t3": "P3Training3",
  "coding3-t4": "P3Training4",
  "coding3-t5": "P3Training5",
  "coding3-t6": "P3Training6",

  "lesson-m": "P1Training1",
  "P1Training1": "P1Training1",
  "P1Training2": "P1Training2",
}

// added getcurrentslidenumber++++++++++++++++++++++++++++++++++++
function getCurrentSlideNumber() {
  var current_lesson = window.location.href;
  console.log(current_lesson)
  for (var key in lesson_mappings) {
      console.log(key)
      console.log(current_lesson);
      console.log(current_lesson.includes(key))
      if (current_lesson.includes(key)) {
          var lesson_id = lesson_mappings[key];
          console.log(lesson_id, 'lesson_id');
          firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/').once('value').then(function (snapshot) {
              data = snapshot.val()
              if(data){
                var keys = Object.keys(data)
                var k = keys[Object.keys(data).length - 1]
                if(data[k].user_code){
                  editor.value = data[k].user_code
                }
              }

              // return snapshot.val();
          })
      }
  }
}
// *********************** Open Gallery Page Dialog ****************

var loading = null
$('.create-GalleryPage').click(function () {
  dialogInMenu = true;
  userCodeHtml2 = ''

  createGalleryPage();
  // hideButton()
      document.getElementById('previous').style.visibility = 'hidden'
    document.getElementById('previous2').style.visibility = 'hidden'
  loading = document.getElementById('loadingIcon')
  document.getElementById('pageNumber').innerHTML = current_page + 1
  document.getElementById('pageNumber2').innerHTML = current_page + 1

  getGalleryData();
})
// ++++++++++++++++++added code ++++++++++++++++++++++++
function createGalleryPage() {
  let GalleryInit = document.getElementById('#galleryContainer')

  $.galleryDialog({
    modalName: 'createGalleryPage',
    htmlContent:
      `
      <div class="container" style="height:${screenHeight_plus23}; padding: 23px;overflow-y: scroll;" id="gallery_page">
      <h2 class="">Projects</h2>
      <div class="search-continer">
        <div class="search-list">
          <div class="">
            <h6>Search By:</h6>
          </div>
          <ul>
            <li>
              <select name="" id="selectOptions" onchange="FilterData(this.event)">
                <option value="Projects" id="projects">All Projects</option>
                <option value="Recent" id="recent"> Recent</option>
                <option value="MostLiked" id="likes">Most Liked</option>
              </select>
            </li>
          </ul>
        </div>
      </div>
      <div class="pagination-section">
        <ul class="pagination">
          <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div>
      <div class="loading" id="loadingIcon">
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
      </div>
      <div class="gallery-container" id="galleryContainer">

      </div>
      <div class="gallery-container" id="galleryLikes">

      </div>
      <div class="gallery-container" id="recentProjects">
      </div>
      <div class="pagination-section" id="bottom-pagination">
        <ul class="pagination">
          <li class="page-item previous" id="previous2"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber2"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div>
    </div>
    </div>`
  })
  console.log('Test');
}
//+++++++++++++++++++added code end +++++++++++++++++++++++++++
// end getcurrentslidenumber++++++++++++++++++++++++++++++++++++

// function getCurrentSlideNumber() {
//   var current_lesson = window.location.href;
//   console.log(current_lesson, 'current_lesson')
//   for (var key in lesson_mappings) {
//     console.log(key, 'key')
//     console.log(current_lesson);
//     console.log(current_lesson.includes(key))
//     if (current_lesson.includes(key)) {
//       var lesson_id = lesson_mappings[key];
//       console.log(lesson_id, 'lesson_id');
//       console.log('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/');
//       firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/').once('value').then(function (snapshot) {
//         data = snapshot.val()
//         console.log("hello", data);
//         var keys = Object.keys(data)
//         var k = keys[Object.keys(data).length - 1]

//         if (data[k].user_code) {
//           console.log(data[k].user_code, 'data[k].user_code');
//           editor.getDoc().setValue(data[k].user_code)
//         }

//         // return snapshot.val();
//       })
//     }
//   }
// }



















var save_pending = false;

var current_avatar = 'robot';
// firebase ++++++++++++++++++++++++++++++
function saveUserData(save_code) {
  // return true;
  var now = new Date().toISOString()
  if (save_pending || save_code) {
    /// /console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase
      .database()
      .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
      .update({
        user_code: editor.value,
        // last_checkpoint: checkpoint_id,
        //  progress_completed: lesson_progress.progress_completed,
        last_updated: timeNow()
      })
    // ////console.log("Saved code and checkpoint #" + checkpoint_id + " " + timeNow());
    save_pending = false
  } else {
    // console.log("Nothing to save: "+timeNow());
  }
}
// firebase end+++++++++++++++++++++++
// Login start here
function loginModal() {
  $('.modal ').remove();
  $(".blocker").css("display",'none')
  $.createDialog({
    modalName: 'login-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: saveLogin,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Type your name and date of birth to login.</h2>' +
      date_form +
      '\t<div id="confirm_actions">' +
      '\t\t<input id="action_button login-button" class="btn btn-encouraging light action" type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })

  // close modal on save
  $('.action').click(function () {
    var first_name = document.getElementById('f_name').value
    var last_name = document.getElementById('l_name').value
    // var nick_name = document.getElementById('n_name').value
    var day = document.getElementById('B-day').value
    var month = document.getElementById('B-month').value
    var year = document.getElementById('B-year').value
    console.log(first_name, last_name, day, month, year)

    if (first_name == '') {
      document.querySelector('#f_name', '').classList.add('error-border')
      return false
    }
    else if (last_name == '') {
      document.querySelector('#l_name', '').classList.add('error-border')
      return false
    } else {

            // store to users storage
        // var ref = firebase.database().ref('/users/' + auth_id)
        var ref = firebase.database().ref('/users/')
        ref.orderByChild("first_name").equalTo(first_name).once('value').then((snapshot)=> {
          data = snapshot.val()
          var matched = false;
          if(data){
            Object.keys(data).forEach(function(k){
              var in_data = data[k];
              Fname = in_data.hasOwnProperty('first_name') ? in_data['first_name'] : '';
              Lname = in_data.hasOwnProperty('last_name') ? in_data['last_name'] : '';
              v_day = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('day') ? in_data.D_O_B['day'] : '' : '';
              v_month = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('month') ? in_data.D_O_B['month'] : '' : '';
              v_year = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('year') ? in_data.D_O_B['year'] : '' : '';
              console.log(Fname, Lname, v_day, v_month, v_year, "From firebase");
              console.log(first_name, last_name, day, month, year, "Inputs");

              if(Fname == first_name && Lname == last_name && v_day == day && v_month == month && v_year == year ){
                 $('#login-modal').modal('hide')
                $('.modal , .modal-backdrop').remove()
                $('.blocker').css('display', 'none');
                successProfileDetails('Login Successfull', 'Welcome back ' + Fname)
                matched = true;
                console.log("Came inside");
                localStorage.setItem('userid', k)
                auth_id = k
                var newurl =
                  window.location.protocol +
                  '//' +
                  window.location.host +
                  window.location.pathname +
                  '?' +
                  auth_id
                console.log('newurl: ' + newurl)
                window.history.pushState({ path: newurl }, '', newurl)
                getCurrentSlideNumber();
                checkUserKey()
                // break;
              }

          });
          }
          

          if(! matched){
            alert('Invalid credentials')
          }
        })

    }

  })
}
function checkUserKey(){
  let userName = document.querySelector('.profile_name')
  var validateAuthId = firebase.database()
  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function (snapshot) {
      data = snapshot.val()
      var keys = Object.keys(data)
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
       if (findKey != auth_id) {
        // registerModal()
      } else{
        validateAuthId.ref('/users/' + auth_id).once('value').then((snapshot)=>{res = snapshot.val()
          // var uKeys = Object.keys(res);
          var uFname = res.first_name;
          var uLname = res.last_name
          userName.innerText ='Hello, '+uFname;

        })
      }
    }
  )
}
// function loginModal() {
//   $('.modal ').remove()
//   $.createDialog({
//     modalName: 'login-modal',
//     popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
//     actionButton: saveLogin,
//     htmlContent:
//       '<div id="confirm_dialog" style="display: block;">' +
//       '\t<h2 id="confirm_title">Please enter your details below to login</h2>' +
//       date_form +
//       '\t<div id="confirm_actions">' +
//       '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Maybe later</button>' +
//       '\t\t<input id="action_button login-button" class="btn btn-primary light action" type="submit" value="Sign In">' +
//       '\t</div>' +
//       '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
//       '</div>'
//   })
//   $('.register-button').click(function () {
//     registerModal()
//   })

//   // close modal on save
//   $('.action').click(function () {
//     var first_name = document.getElementById('f_name').value
//     var last_name = document.getElementById('l_name').value
//     var nick_name = document.getElementById('n_name').value
//     var day = document.getElementById('B-day').value
//     var month = document.getElementById('B-month').value
//     var year = document.getElementById('B-year').value
//     // console.log(first_name, last_name, nick_name, day, month, year)

//     // store to users storage
//     var ref = firebase.database().ref('/users')

//     var updateLoginDetails = {
//       first_name: first_name,
//       last_name: last_name,
//       nick_name: nick_name,
//       D_O_B: { day, month, year }
//     }
//     ref.update({
//       [auth_id]: updateLoginDetails
//     })
//     // users storage end

//     $('#login-modal').modal('hide')
//     $('.modal , .modal-backdrop').remove()
//     $('.blocker').css('display', 'none')
//     // console.log('closed #login-modal')
//   })
// }
//login end here 
// ============= saved userDetails before profile Model =========
function successProfileDetails(title, message) {
  $('.modal ').remove()
  $.createDialog({
    modalName: 'successProfileDetails',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">'+title+'</h2>' +
      '<label>'+message+'</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}
// ============== saved userDetails before profile Model end =========
// CodeMirror HTMLHint Integration
(function (mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

  (function (CodeMirror) {
    "use strict";

    CodeMirror.registerHelper("lint", "html", function (text) {
      var found = [], message;
      if (!window.HTMLHint) return found;
      var messages = HTMLHint.verify(text, ruleSets);
      for (var i = 0; i < messages.length; i++) {
        message = messages[i];
        var startLine = message.line - 1, endLine = message.line - 1, startCol = message.col - 1, endCol = message.col;
        found.push({
          from: CodeMirror.Pos(startLine, startCol),
          to: CodeMirror.Pos(endLine, endCol),
          message: message.message,
          severity: message.type
        });
      }
      return found;
    });
  });

// ruleSets for HTMLLint
var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};

var delay;
// console.log(document.getElementById('myeditor').value);
// Initialize CodeMirror editor
var editor = CodeMirror.fromTextArea(document.getElementById('myeditor'), {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop: true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
});
// console.log(editor);

// Live preview
editor.on('change', function () {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});
function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview = previewFrame.contentDocument || previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();
  save_pending = true;

  //validateCheckPoint();
  validatorLessonChallenges();

	/*if (preg_match('%(<p[^>]*>.*?</p>)%i', $subject, $regs)) {
    $result = $regs[1];
} else {
    $result = "";
}*/
  //console.log($("#editor").text().match('<p[^>]*>.*?</p>'));

  //$("div:match('/[^a-zA-Z0-9]/')")
}
function checkIfTagExists(src, tag, betweenTag) {
  if (!betweenTag) { betweenTag = "(.*?)"; }

  var re = "<" + tag + "[^>]*>\s*" + betweenTag + "\s*<\\/" + tag + ">";


  //	console.log(re);
  //console.log(re2);

  return new RegExp(re).test(src);
}




//Function to return correct Lesson validator for given lesson
function validatorLessonChallenges() {
  let searchParams = new URLSearchParams(window.location.search);
  if (searchParams.has('lesson')) {

    //get lesson number/name if is provided on URL
    lessonName = searchParams.get('lesson');

    //call corresponding validator method
    if (lessonName == "lesson002") {
      validateCheckPointLesson2();
    }
    else if (lessonName == "project1") {
      validateCheckPointProject1();
    }
    else if (lessonName == "p1-t01") {
      validateCheckPointP01_T001_D();
    }
    else if (lessonName == "p1-t02") {
      validateCheckPointP01_T002_D();
    }
    else if (lessonName == "p1-t03") {
      validateCheckPointP01_T003_D();
    }
    else if (lessonName == "p1-t04") {
      validateCheckPointP01_T004_D();
    }
    else if (lessonName == "p2-t01") {
      validateCheckPointP02_T001_D();
    }
    else if (lessonName == "p2-t02") {
      validateCheckPointP02_T002_D();
    }
    else if (lessonName == "p2-t03") {
      validateCheckPointP02_T003_D();
    }
    else if (lessonName == "p2-t04") {
      validateCheckPointP02_T004_D();
    }
    else if (lessonName == "p2-t05") {
      validateCheckPointP02_T005_D();
    }
    else if (lessonName == "p2-t06") {
      validateCheckPointP02_T006_D();
    }
    else if (lessonName == "p3-t01") {
      validateCheckPointP03_T001_D();
    }
    else if (lessonName == "p3-t02") {
      validateCheckPointP03_T002_D();
    }
    else if (lessonName == "p3-t03") {
      validateCheckPointP03_T003_D();
    }
    else if (lessonName == "p3-t04") {
      validateCheckPointP03_T004_D();
    }
    else if (lessonName == "p3-t05") {
      validateCheckPointP03_T005_D();
    }
    else if (lessonName == "p3-t06") {
      validateCheckPointP03_T006_D();
    }
  }
  else {
    //get lesson1(default) validator if lesson is not provided on URL
    validateCheckPoint();
  }
}

/*========== Lesson named P00-T00-D meaning project, taining, and desktop platform ====
  ========== validator functions for each training =====*/

//Function to validate challenges on P01-T001-D

function validateCheckPointP01_T001_D() {
  var checkPoints = {
    16: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
    17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
    23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
    24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
    32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    36: "(<p>|<p [^>]*>)((.|\n)*)\s*2019((.|\n)*)\s*<\/p>"
    // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    // 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    // 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    // 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    // 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    // 318: "^\s*Date\s*\(\s*\)\s*$"
  }
  var hintsForCheckPonts = {
    16: "openning and closing <b>head</b>",
    17: "Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
    23: "In the body section. Hint given as tip on bottom of the slide.",
    24: "Example: <span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span>",
    32: "use below the <span class='html-code'>&lt/h1&gt;</span> tag.",
    36: "Just below the &lt;/h3&gt; tag"
  }
  if (hintsForCheckPonts[currentStep] === undefined) {
    $(".hint-popup").hide();
  }
  if (checkPoints[currentStep] !== undefined) {

    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      saveUserData(true, findIndexOfKeys(checkPoints, currentStep), "P1Training1");
    } else {
      if (!$(".hint-popup-checked").hasClass("checked" + currentStep)) {
        $(".hint-popup-checked").addClass("checked" + currentStep);
        if (!$("#slide" + currentStep + " .check").hasClass('passed')) {
          if (hintsForCheckPonts[currentStep] !== undefined) {
            htmlTagToString = hintsForCheckPonts[currentStep];
            $(".hint-text").html(htmlTagToString);

            $(".hint-popup").show().delay(10000).fadeOut();

            // $(".hint-popup").mouseover(function(){
            //   $(this).stop();
            // });
            // $(".hint-popup").mouseout(function(){
            //   $(this).show().delay(10000).fadeOut();
            // });

            $(".close-hint-popup").click(function () {
              $(".hint-popup").clearQueue();
              $(".hint-popup").fadeOut();
            });
          }
        }
      }

      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }


  var unlockSkills = {
    16: "head",
    17: "body",
    23: "h1-h6",
    36: "p",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P01-T002-D
function validateCheckPointP01_T002_D() {
  var checkPoints = {
    12: "(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>",
    22: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",
    23: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",
    27: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/body>",
    28: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    31: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    37: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    39: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    44: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    // 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    // 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    // 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    // 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    // 318: "^\s*Date\s*\(\s*\)\s*$"
  }

  var hintsForCheckPonts = {
    12: "&lt;style&gt; used for declaring css",
    22: "Selecting element <b>h1</b>",
    23: "css for selected property within { }"
  }
  if (hintsForCheckPonts[currentStep] === undefined) {
    $(".hint-popup").hide();
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true, findIndexOfKeys(checkPoints, currentStep), "P1Training1");
    } else {

      if (!$(".hint-popup-checked").hasClass("checked" + currentStep)) {
        $(".hint-popup-checked").addClass("checked" + currentStep);
        if (!$("#slide" + currentStep + " .check").hasClass('passed')) {
          if (hintsForCheckPonts[currentStep] !== undefined) {
            htmlTagToString = hintsForCheckPonts[currentStep];
            $(".hint-text").html(htmlTagToString);
            $(".hint-popup").show().delay(10000).fadeOut();
            $(".close-hint-popup").click(function () {
              $(".hint-popup").clearQueue();
              $(".hint-popup").fadeOut();
            });
          }
        }
      }

      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P01-T003-D
function validateCheckPointP01_T003_D() {
  var checkPoints = {
    18: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    24: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    25: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    26: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    34: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>((.|\n)*)\s*<\/body>",
    35: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*section((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*lightgrey((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>((.|\n)*)\s*<\/body>",
    39: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*section((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*lightgrey((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>((.|\n)*)\s*(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>((.|\n)*)\s*<\/body>",
    42: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*section((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*lightgrey((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>((.|\n)*)\s*(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>((.|\n)*)\s*<\/body>",
    44: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*section((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*lightgrey((.|\n)*)\s*}((.|\n)*)\s*footer((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>((.|\n)*)\s*(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>((.|\n)*)\s*<\/body>",
    // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    // 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    // 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    // 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    // 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    // 318: "^\s*Date\s*\(\s*\)\s*$"
  }
  var hintsForCheckPonts = {
    18: "remember to open and close it"
  }
  if (hintsForCheckPonts[currentStep] === undefined) {
    $(".hint-popup").hide();
  }
  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      if (!$(".hint-popup-checked").hasClass("checked" + currentStep)) {
        $(".hint-popup-checked").addClass("checked" + currentStep);
        if (!$("#slide" + currentStep + " .check").hasClass('passed')) {
          if (hintsForCheckPonts[currentStep] !== undefined) {
            htmlTagToString = hintsForCheckPonts[currentStep];
            $(".hint-text").html(htmlTagToString);
            $(".hint-popup").show().delay(10000).fadeOut();
            $(".close-hint-popup").click(function () {
              $(".hint-popup").clearQueue();
              $(".hint-popup").fadeOut();
            });
          }
        }
      }

      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "head",
    17: "body",
    23: "h1-h6",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P01-T004-D
function validateCheckPointP01_T004_D() {
  var checkPoints = {
    9: "(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>",
    12: "(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>",
    16: "(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>",
    24: "(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    26: "(<style>|<style [^>]*>)div((.|\n)*)\s*{((.|\n)*)\s*center((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    29: "(<style>|<style [^>]*>)div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    33: "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    36: "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*white((.|\n)*)\s*solid((.|\n)*)\s*2px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    37: "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*45px((.|\n)*)\s*padding((.|\n)*)\s*15px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    39: "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*auto((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    41: "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*400px((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    45: "(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*white((.|\n)*)\s*}((.|\n)*)\s*<\/style>",
    // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    // 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    // 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    // 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    // 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    // 318: "^\s*Date\s*\(\s*\)\s*$"
  }
  var hintsForCheckPonts = {
    9: "The &lt;div&gt; tag defines a division or a section in an HTML document."
  }
  if (hintsForCheckPonts[currentStep] === undefined) {
    $(".hint-popup").hide();
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      if (!$(".hint-popup-checked").hasClass("checked" + currentStep)) {
        $(".hint-popup-checked").addClass("checked" + currentStep);
        if (!$("#slide" + currentStep + " .check").hasClass('passed')) {
          if (hintsForCheckPonts[currentStep] !== undefined) {
            htmlTagToString = hintsForCheckPonts[currentStep];
            $(".hint-text").html(htmlTagToString);
            $(".hint-popup").show().delay(10000).fadeOut();
            $(".close-hint-popup").click(function () {
              $(".hint-popup").clearQueue();
              $(".hint-popup").fadeOut();
            });
          }
        }
      }

      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T001-D
function validateCheckPointP02_T001_D() {
  var checkPoints = {
    12: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",

  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T002-D
function validateCheckPointP02_T002_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T003-D
function validateCheckPointP02_T003_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T004-D
function validateCheckPointP02_T004_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T005-D
function validateCheckPointP02_T005_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T006-D
function validateCheckPointP02_T006_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P03-T001-D
function validateCheckPointP03_T001_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",

  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P03-T002-D
function validateCheckPointP03_T002_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P03-T003-D
function validateCheckPointP03_T003_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P03-T004-D
function validateCheckPointP03_T004_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P03-T005-D
function validateCheckPointP03_T005_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on P02-T006-D
function validateCheckPointP03_T006_D() {
  var checkPoints = {
    100: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

/*========= End of validators ======*/




//Function to validate challenges on project1
function validateCheckPointProject1() {
  var checkPoints = {
    46: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
    ////	48: "(<body>|<body [^>]*>)(\n|(\n\n)|(\n\s)|\n\s\n|\n\s)\s*(.*?)\s*<\/body>"
    47: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
    53: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
    54: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
    62: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    63: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    66: "(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>",
    85: "(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>",
    95: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",
    96: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>",

    100: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/body>",
    101: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    104: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    110: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    112: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    117: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    146: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    152: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    153: "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>((.|\n)*)\s*" +
      "(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
    // 14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    // 19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    // 23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    // 27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    // 219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    // 318: "^\s*Date\s*\(\s*\)\s*$"
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    console.log('Run validation results ' + reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      // $(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      console.log('Run validation using ' + checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $("#slide" + currentStep + " .check").html('Skip this →').removeClass('passed');;
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//Function to validate challenges on lesson2
function validateCheckPointLesson2() {

  var checkPoints = {
    12: "(<h5>|<h5 [^>]*>)\s*(.*?)\s*<\/h5>",
    14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    318: "^\s*Date\s*\(\s*\)\s*$"
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    // console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    // console.log('Run validation results '+reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      //		$(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      // console.log('Run validation using '+checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}


function validateCheckPoint() {

  var checkPoints = {
    12: "(<h1>|<h1 [^>]*>)\s*(.*?)\s*<\/h1>",
    14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
    19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
    23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
    27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
    219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
    318: "^\s*Date\s*\(\s*\)\s*$"
  }

  if (checkPoints[currentStep] !== undefined) {
    // do something
    //console.log('Run validation only on '+currentStep);
    var re = checkPoints[currentStep];
    var reResults = new RegExp(re, "i").test(editor.getValue());
    //console.log('Run validation results '+reResults);
    if (reResults) {
      $("#slide" + currentStep + " .check-icon").html('/');
      $("#slide" + currentStep + " .check").html('I did it →').addClass('passed');
      $("#slide" + currentStep + " .actions li").addClass('correct');
      //			$(".pagination .next").addClass('disabled');
      $(".pagination .next").removeClass('disabled');
      //			console.log('Run validation using '+checkPoints[currentStep]);
      saveUserData(true);
    } else {
      $("#slide" + currentStep + " .check-icon").html('\\');
      $("#slide" + currentStep + " .actions li").removeClass('correct');
      $(".pagination .next").addClass('disabled');
    }
  }

  var unlockSkills = {
    12: "h1-h6",
    15: "p",
    20: "email-input",
    24: "submit-input"
  }

  if (unlockSkills[currentStep] !== undefined) {
    $("#" + unlockSkills[currentStep] + "-skill").addClass('has-skill-true');
    $("#" + unlockSkills[currentStep] + "-skill").removeClass('has-skill-false');
    //console.log('Unlocked skill '+unlockSkills[currentStep]);
    //	console.log("#"+currentStep+"-skill");

    $("#" + unlockSkills[currentStep] + "-skill").attr('data-original-title', $("#" + unlockSkills[currentStep] + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
    //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
    //	  console.log("#"+unlockSkills[currentStep]+"-skill");
    //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
  }
}

//var activeTab = $(".tab-content .tab-content").find(".active");
//var id = activeTab.attr('id');
//console.log('Run validation only on '+currentStep);


setTimeout(updatePreview, 300);

totalSlides = $(".tab-pane-slide").length;
$("#lessonProgressBar").attr("data-valuemax", totalSlides);

var lessonProgressBar = document.getElementById('lessonProgressBar');
var maxNum = lessonProgressBar.dataset.valuemax;
var currentStep = 1;

// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
  if (currentStep = parseInt(url.split('#slide')[1])) {
    $('#tab-slides.nav-tabs a[href="#slide' + currentStep + '"]').tab('show');
    //console.log('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]');
  } else {
    $('#submenu.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
  }

}

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
  if (history.pushState) {
    history.pushState(null, null, e.target.hash);
  } else {
    window.location.hash = e.target.hash; //Polyfill for old browsers
  }
})



function updateProgressBar(e, step) {
  if (!step) {
    currentStep = parseInt($(e.target).data('step'));
  } else {
    currentStep = step;
  }

  //console.log("e: "+currentStep);
  if (!parseInt(currentStep)) {
    currentStep = 1;
  }
  // console.log("e2: "+currentStep);


  var percent = (parseInt(currentStep) / maxNum) * 100;


  $('.progress-bar').css({ width: percent + '%' });
  $('.lessonProgressText').text("Step " + currentStep + " of " + maxNum);

  if (currentStep === 1) {
    $(".pagination .text-right .next").html('Start Slideshow').removeClass('disabled').attr("href", "#slide" + (currentStep + 1));
    $(".pagination .prev").addClass('disabled');
    // $(".pagination .prev").click(function(e) {
    //	e.preventDefault();
    // });
  }
  else if (currentStep === parseInt(maxNum)) {
    //$("#slide"+(currentStep)).append("<a class='btn btn-primary' href='GitHub/codejika/learn/nomzamos-website-01.php?lesson=P01-T001-D/nomzamos-website-01.php?lesson=P01-T001-D' style='top:65%;'>Start next training →</a></div>");

    $(".pagination .next").addClass('disabled');
    $(".pagination .prev").removeClass('disabled').attr("href", "#slide" + (currentStep - 1));
    // $(".pagination .next").click(function(e) {
    //	e.preventDefault();
    // });
  }
  else {
    $(".pagination .next").removeClass('disabled').attr("href", "#slide" + (currentStep + 1));
    $(".pagination .text-right .next").html('Next >').removeClass('disabled').attr("href", "#slide" + (currentStep + 1));
    $(".pagination .prev").removeClass('disabled').attr("href", "#slide" + (currentStep - 1));
  }
  //e.relatedTarget // previous tab

  //validateCheckPoint();
  validatorLessonChallenges();
}

$('a[data-toggle="tab"][role="tab-slides"]').on('shown.bs.tab', updateProgressBar);

function nextTab(elem) {
  var elemFind = $(elem).parent().next().find('a[data-toggle="tab"]')
  if (elemFind) {
    elemFind.click();
  }
}
function prevTab(elem) {
  var $elemFind = $(elem).parent().prev().find('a[data-toggle="tab"]')
  if ($elemFind) {
    $elemFind.click();
  }
}

$('.first').click(function () {

  $('#myWizard a:first').tab('show')

})


function timeNow() {
  return new Date().toISOString();
}

function saveUserData(save_code, checkpoint_id, lesson_id) {
  var now = new Date().toISOString();
  if (save_pending || save_code) {
    //console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + lesson_id + "/user_checkpoint/" + checkpoint_id).update({
      user_code: editor.getValue(),
      //last_checkpoint: checkpoint_id,
      //progress_completed: lesson_progress.progress_completed,
      last_updated: timeNow()
    });
    // console.log("Saved code and checkpoint #" + currentStep + " " + timeNow());
    save_pending = false;
  } else {
    //console.log("Nothing to save: "+timeNow());
  }
}


function initFirebase() {
  // Initialize Firebase
  // var config = {
  //   apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
  //   authDomain: "codejika-2cf17.firebaseapp.com",
  //   databaseURL: "https://codejika-2cf17.firebaseio.com",
  //   projectId: "codejika-2cf17",
  //   storageBucket: "codejika-2cf17.appspot.com",
  //   messagingSenderId: "405485160215"

  // gallery Firebase
  var config = {
    apiKey: 'AIzaSyAw269vPfE3QreRGZDuEisv3wSnfFmFFoY',
    authDomain: 'codejika-staging.firebaseapp.com',
    databaseURL: 'https://cj-staging.firebaseio.com/',
    projectId: 'codejika-staging',
    storageBucket: 'codejika-staging.appspot.com',
    messagingSenderId: '405485160215'
  }

  firebase.initializeApp(config);
}

function progressUpdate() {
  var progressPercentage = checkpoint_count / checkpoint_id;
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).set({
    progressCompleted: progressPercentage
  });
}


function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonProgressPromise");
}

function onStart() {

  editor.setValue(lesson_progress.user_code);

}

$(document).ready(function () {

  $("#menu_page").hide();
  $("#gallery_section").hide();
  hideButton()
  $('#close_button').click(function () {
    $('.blocker .jquery-modal').css('background', 'none')
  })

  initFirebase();
  getCurrentSlideNumber()
  var getGallery = getGalleryData('', '')
  var getLessonProgress = getLessonProgressPromise();
  Promise.all([getLessonProgress, getGallery]).then(function (results) {
    lesson_progress = results[0];

    //if no lesson data save default values to DB
    if (lesson_progress === null) {
      console.log("no lesson progress found");
      lesson_progress = {
        "last_checkpoint": "0",
        "user_code": "",
        "progress_completed": "0"
      }
      //saveUserData(true);
      console.log("default lesson_progress inserted");
    }

    //str = JSON.stringify(lesson_progress, null, 4);
    //console.log("default lesson_progress:\n"+str);

    console.log("promise all is true, now running onStart");
    onStart();
  });

  // console.log('echo1');
  updateProgressBar("lessonProgressBar", currentStep);
  //  console.log('echo2');


  //Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip();

  //Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    
    var $target = $(e.target);

    if ($target.parent().hasClass('disabled')) {
      return false;
    }
  });

  $("#lesson_tab .next").click(function (e) {

    var $active = $('#lesson_tab .nav-tabs li>a.active');
    $active.parent().next().removeClass('disabled');
    nextTab($active);

  });
  $("#lesson_tab .prev").click(function (e) {

    var $active = $('#lesson_tab .nav-tabs li>a.active');
    prevTab($active);

  });

  //  Activate the Tooltips
  // $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
  // html: true,
  // trigger: 'manual'
  // }).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function(){
  //     $('#'+$(this).attr('id')+'.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
  //     });
});

// ============= unRegistered Model =========
function UnRegisteredModal() {
  // console.log("reg");
  $('.modal ').remove()
  $(".blocker").css("display",'none')
  $.createDialog({
    modalName: 'unRegistered-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad',
    actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">OOOOPS!</h2>' +
      '<label>This feature is only available for logged in users.</label>\n\n\n' +
      '<h4>Login Now</h4>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action " type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })
}
// function UnRegisteredModal() {
//   // console.log("reg");
//   $('.modal ').remove()
//   $.createDialog({
//     modalName: 'unRegistered-modal',
//     popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad',
//     actionButton: loginModal,
//     htmlContent:
//       '<div id="confirm_dialog" style="display: block;">' +
//       '\t<h2 id="confirm_title">OOOOPS!</h2>' +
//       '<label>This feature is only available for logged in users.</label>\n\n\n' +
//       '<h4>Login Now</h4>\n' +
//       '\t<div id="confirm_actions">' +
//       '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
//       '\t\t<input id="action_button" class="btn btn-primary light action " type="submit" value="Login">' +
//       '\t</div>' +
//       '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
//       '</div>'
//   })
//   $('.register-button').click(function () {
//     registerModal()
//   })
// }
// ============== unRegistered Model end =========


function homePage() {
  // console.log('home')
  $("#main_page").show();
  $("#gallery_section").hide();
  $("#menu_page").hide();
}


function galleryPage() {
  // console.log('galery')
  $("#main_page").hide();
  $("#menu_page").hide();
  $("#gallery_section").show();
  $(".menu-bg").hide();
    addToGallery();

}

function menuPage() {
  $("#gallery_section").hide();
  //$("#main_page").hide();
  $("#menu_page").show();
}
var loading = document.getElementById('loadingIcon')
loading.style.display = 'none'

var current_page = 0
var page_id_array = ['']
document.getElementById('pageNumber').innerHTML = current_page + 1
// document.getElementById('pageNumber2').innerHTML = current_page + 1

function addToGallery() {
  var userCode, keyString
  var check_if_blank = editor.value.replace(/(\r\n\t|\n|\r\t)/gm, '').trim()
  if (check_if_blank == '') {
    alert('write some code')
    return false
  }

  var validateAuthId = firebase.database()

  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function (snapshot) {
      // loading.style.display = "block";
      data = snapshot.val()
      var keys = Object.keys(data)
      var validateName = keys['first_name']
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
      // console.log(keyString, 'userDetails')
      console.log(auth_id, 'auid')
      if (findKey == auth_id) {
        titleModal()
      } else if (findKey !== auth_id || keys == null) {
        UnRegisteredModal()
        // alert("Your code is added to the gallery");
      }
    }) // end of firebase response validAuth2
}
// function addToGallery() {
//   var userCode, keyString
//   var check_if_blank = editor.getValue().replace(/(\r\n\t|\n|\r\t)/gm, '').trim()
//   if (check_if_blank == '') {
//     alert('write some code')
//     return false
//   }
//   var validateAuthId = firebase.database()

//   validateAuthId
//     .ref('/users/')
//     .once('value')
//     .then(function (snapshot) {
//       // loading.style.display = "block";
//       data = snapshot.val()
//       var keys = Object.keys(data)
//       var validateName = keys['first_name']
//       var findKey = keys.filter(key => key == auth_id)
//       keyString = findKey.toString()
//       // console.log(keyString, 'userDetails')
//       console.log(auth_id, 'auid')
//       if (findKey == auth_id) {
//         titleModal()
//       } else if (findKey !== auth_id || keys == null) {
//         UnRegisteredModal()
//         // alert("Your code is added to the gallery");
//       }
//     }) // end of firebase response validAuth2
// }

var user_HTML
var current_key1
var user_code1
var userCodeHtml2 = ''
var pagesize = 6
var current_page_size = 0
function getGalleryData(start_with_id, filter) {
  firebase_object = firebase
    .database()
    .ref('/gallery/5-min-website')
    .orderByKey()
  if (start_with_id && start_with_id != '') {
    // console.log(start_with_id, 'Here is last fetched key')
    firebase_object.startAt(start_with_id)
  }
  var lastVisible = ''

  firebase_object
    .limitToFirst(pagesize * (current_page + 1))
    .once('value')
    .then(
      function (snapshot) {
        // console.log(pagesize * (current_page + 1), 'hello')
        var data = snapshot.val()
        var keys = Object.keys(data)
        if (keys.length < pagesize * (current_page + 1)) {
          document.getElementById('next2').style.visibility = 'hidden'
        } else {
          document.getElementById('next2').style.visibility = 'visible'
        }

        // console.log(keys, 'Here are the keys ')
        var sorted_array = []
        for (i = 0; i < keys.length; i++) {
          var k = keys[i]
          // dataLikes = data[k].date
          data[k]['key'] = k
          sorted_array.push(data[k])
          // console.log(likeArray,'data&k')
        }
        // console.log(sorted_array, 'before SortArray')
        if (filter && filter != '') {
          sorted_array.sort(function (a, b) {
            if (filter == 'MostLiked') {
              return b.likes - a.likes
            } else if (filter == 'Recent') {
              return new Date(b.date) - new Date(a.date)
            }
            return 0
          })
        }
        // console.log(sorted_array, 'After SortArray')
        current_page_size = sorted_array.length
        for (i = 0; i < sorted_array.length; i++) {
          loading.style.display = 'none'
          var key = sorted_array[i]['key']
          $('#bottom-pagination').css('visibility', 'visible')
          if (i < 6 * current_page) {
            continue
          }

          var current_key1 = key
          // var userCodeURL =
          //   "https://www.codejika.com/preview/5-minute-website?" + current_key;
          var name =
            data[current_key1].first_name + ' ' + data[current_key1].last_name
          var likes = data[current_key1].likes
          user_HTML = escape(data[current_key1].user_code)
          var user_code1 = escape(data[current_key1].user_code)
          var title = data[current_key1].title
          // console.log(user_HTML,'user')
          userCodeHtml2 += `
              <div id = "userKey" >
                  <div class="card-blog" id="cardHTML">
                  <div  class ="priview_blog"><a onclick="openthumbnail('${current_key1}', '${user_HTML}', '${user_code1}')">
                    <h3>${title}</h3>
                    <a onclick="openthumbnail('${current_key1}', '${user_HTML}', '${user_code1}')" id="previewIcon">
                     View
                     <span><i class="fas fa-external-link-square-alt"></i></span>
                    </a>
                  </div>
                  <a  class="HTMLscreen" >
                  <iframe src="data:text/html;charset=utf-8, ${user_code1}"  scrolling="no" id="my_iframe_${current_key1}">
                  </iframe style="background:#fff">
                </a>

                  <div class="detail-blog">
                  <h4>${name}</h4>
                  <div class="d-flex view-section">
                    <div class="likes">
                        <button onclick="likes('${current_key1}')"><span><i class="far fa-thumbs-up"></i></button><small id='my_like_${current_key1}'>${likes}</small></span>
                    </div>
                    <div class="views">
                    </div>
                  </div>
                  </div>
                </div>

              </div>`
          // console.log(userCodeHtml2, "Here is the code");
          lastVisible = current_key1
        }
        document.getElementById('galleryContainer').innerHTML = userCodeHtml2
        // document.getElementById('view_html_code').innerText = user_HTML;

        page_id_array.push(lastVisible)
        return snapshot.val()
      },
      function (error) {
        console.log(error, 'galleryError')
      }
    )

  return firebase_object
}

// likes function
function likes(user_id) {
  // console.log("I am inside likes");

  like_count_ui = document.getElementById('my_like_' + user_id)
  var new_likes = parseInt(like_count_ui.innerHTML) + 1
  var ref = firebase.database().ref('/gallery/5-min-website/' + user_id)

  ref.once('value').then(function (snapshot) {
    // console.log(snapshot.val(), "Here os the snapsho");

    if (snapshot.val() && snapshot.val().likes) {
      new_likes = snapshot.val().likes + 1
    }
    like_count_ui.innerHTML = new_likes
    ref.update({
      likes: new_likes
    })
  })
}

// filter data
function FilterData() {
  $('.modal ').remove()

  var selectedOption = document.getElementById('selectOptions').value
  console.log(selectedOption, 'option')
  userCodeHtml2 = ''
  $('#loadingIcon').css('display', 'block')
  $('.pagination-section').css('visibility', 'visible')
  // console.log('iam in projects')
  var previousPage = document.getElementById('galleryLikes')
  previousPage.style.display = 'none'
  var previousPage2 = document.getElementById('galleryContainer')
  previousPage2.innerHTML = getGalleryData('', selectedOption)
}


// **************** popup modal **********************

// ============= thumbnail Model =========
function viewThumbnail(clicked_key, user_html_code, user_code_output) {
  // $('.modal ').remove()
  $.galleryDialog({
    modalName: 'viewThumbnail-modal',
    htmlContent: `<div >
        <div class="popup-header">
          <a id="viewHTML" class="view-code">VIEW</a>
        </div>
        <iframe src="data:text/html;charset=utf-8, ${user_code_output}" style="width:100%; height: 100vh;overflow:hidden; border: none;" scrolling="yes" id="my_iframe_${clicked_key}">

        </iframe>
      </div>
      `
  })
  $('.view-code').click(function () {
    outputHTML1(clicked_key, user_html_code, user_code_output)
  })
}
function openthumbnail(clicked_key, user_html_code, user_code_output) {
  // console.log(clicked_key, user_html_code, user_code_output, 'Here is my key')
  viewThumbnail(clicked_key, user_html_code, user_code_output)
}
function outputHTML1(clicked_key, user_html_code, user_code_output) {
  user_html_code = unescape(user_html_code)
  $('.modal ').remove()
  $.galleryDialog({
    modalName: 'outputHTML1-modal',
    // popupStyle: "speech-bubble top-align " + current_avatar + " happy",
    // actionButton: resetCurrentProject,
    htmlContent: `

            <div style="height:100vh;overflow:hidden;">
            <div class="popup-header">
              <a class="view-output view1_button" >OUTPUT</a>
            </div>
              <div><xmp id=view_html_code_${clicked_key} class="add-background">${user_html_code}</xmp></div>
            </div>
        `
  })
  $('.view1_button').click(function () {
    // viewThumbnail(clicked_key)
    viewThumbnail(clicked_key, user_html_code, user_code_output)
  })
}

// *************** pagination code ***********
function hideButton() {
  if (current_page == 0) {
    document.getElementById('previous').style.visibility = 'hidden'
  }
}
function go_next() {
  // console.log(page_id_array, 'here is the array')
  current_page += 1
  loading.style.display = 'block'
  document.getElementById('galleryContainer').innerHTML = loading
  $('#bottom-pagination').css('visibility', 'hidden')
  $('#previous2').css('visibility', 'hidden')
  userCodeHtml2 = ''
  getGalleryData(page_id_array[current_page], '')
  document.getElementById('pageNumber').innerHTML = 1 + current_page

  if (current_page != 0) {
    document.getElementById('previous').style.visibility = 'visible'
  }
}

function go_previous() {
  if (current_page != 0) {
    current_page -= 1
    loading.style.display = 'block'
    document.getElementById('galleryContainer').innerHTML = loading
    $('#bottom-pagination').css('visibility', 'hidden')
    userCodeHtml2 = ''
    getGalleryData(page_id_array[current_page], '')
    document.getElementById('pageNumber').innerHTML = 1 + current_page
  }

  // Hide Previous button
  if (current_page == 0) {
    $('#previous').css('visibility', 'hidden')
  }
}


// *************** home page js code *********************
var reseturl=window.location.href;
console.log(reseturl+" outside click")

// localStorage.setItem(true,"flagreset")
// if(resetflag === true && localStorage.getItem("flagReset")===true)
// {
// localStorage.setItem("urlreset",reseturl)
// }
// var lessonsurl = ["http://localhost:9000/learn/coding1-t1.php?lesson=p1-t01",
//                   "http://localhost:9000/learn/coding1-t2.php?lesson=p1-t02",
//                   "http://localhost:9000/learn/coding1-t3.php?lesson=p1-t03",
//                   "http://localhost:9000/learn/coding1-t4.php?lesson=p1-t04"
// ];
// var dirurl = ["http://localhost:9000/learn/coding1-t1",
//               "http://localhost:9000/learn/coding1-t2",
//               "http://localhost:9000/learn/coding1-t3",
//               "http://localhost:9000/learn/coding1-t4"
// ];
// var resetflag=true;
// var dircompleteurl;
// for(var i = dirurl.length; i > 0; i--)
// {
//  if(reseturl !== dirurl[i]){
//    dircompleteurl = dirurl[i].toString();
//    window.location.replace(dircompleteurl +".php");
//    resetflag = flase;
//    break;
// }

// }
// if(resetflag === true)
// { 
//   window.location.replace(reseturl);
// }


$('.reset-profile').click(function () {
  resetCurrentProject()
})
function resetCurrentProject() {
  resetUserData()
  localStorage.setItem(lessson_url + '_' + auth_id + '_active_slide', 0)
  
console.log(reseturl + " when clicked ")

// if(reseturl!== localStorage.getItem("urlreset"))
// {
//   reseturl = localStorage.getItem("urlreset")
// }
  window.location.replace(reseturl)
  //window.location.reload()
}

$('.reset-lesson').click(function () {
  $('.modal ').remove()
  // console.log('Came here to reset the lesson')
  $.createDialog({
    modalName: 'reset-lesson',
    popupStyle: 'speech-bubble ' + current_avatar + ' scared',
    actionButton: resetCurrentProject,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Are you sure you want to reset this lesson?</h2>' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light cancel" datax-dismiss="modal"><a href="#" rel="modal:close">Hmmm, maybe not</a></button>' +
      '\t\t<button id="action_button" class="btn btn-primary light action reset-profile">Yes</button>' +
      '\t</div>' +
      '</div>'
  })
})

function resetUserData() {
  firebase
    .database()
    .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
    .remove()
}

var i
var current_year = new Date().getFullYear()
function pad2(number) {
  return (number < 10 ? '0' : '') + number
}

date_form =
  '<form lpformnum="1" id="email-login"><div ><input class="form-control" id="f_name" placeholder="First name" name="fname" type="text" required><input class="form-control" id="l_name" placeholder="Last name" name="lname" type="text" required=""><input class="form-control" id="n_name" placeholder="Nickname (Username)" name="nickname" type="text" required=""></div><div><label style="    margin: 0;">Date of birth</label></div>\n' +
  '<select name="day" id="B-day" class="custom-select" style="width: 28%">\n' +
  '<option value="" selected></option>\n'
for (i = 1; i <= 31; i++) {
  date_form += '<option value="' + pad2(i) + '">' + pad2(i) + '</option>\n'
}
date_form +=
  '</select>\n' +
  '<select name="month" id="B-month" class="custom-select" style="width: 28%">\n' +
  '<option value="" selected></option>\n' +
  '<option value="01">Jan</option>\n' +
  '<option value="02">Feb</option>\n' +
  '<option value="03">Mar</option>\n' +
  '<option value="04">Apr</option>\n' +
  '<option value="05">May</option>\n' +
  '<option value="06">June</option>\n' +
  '<option value="07">July</option>\n' +
  '<option value="08">Aug</option>\n' +
  '<option value="09">Sept</option>\n' +
  '<option value="10">Oct</option>\n' +
  '<option value="11">Nov</option>\n' +
  '<option value="12">Dec</option>\n' +
  '</select>\n' +
  '<select name="year" id="B-year" class="custom-select" style="width: 38%">\n' +
  '<option value="" selected></option>\n'
for (i = current_year; i >= 1950; i--) {
  date_form += '<option value="' + i + '">' + i + '</option>\n'
}
date_form += '</select>\n'
date_form += '</div><div id="error"></div></form>\n'

$('.login-button').click(function () {
  loginModal()
})

$('.register-button').click(function () {
  registerModal()
})

$('.select_lego').click(function () {
  changeAvatar('lego')
})
$('.select_robot').click(function () {
  changeAvatar('robot')
})
// function loginModal() {
//   $('.modal ').remove()
//   $.createDialog({
//     modalName: 'login-modal',
//     popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
//     actionButton: saveLogin,
//     htmlContent:
//       '<div id="confirm_dialog" style="display: block;">' +
//       '\t<h2 id="confirm_title">Please enter your details below to login</h2>' +
//       date_form +
//       '\t<div id="confirm_actions">' +
//       '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Maybe later</button>' +
//       '\t\t<input id="action_button login-button" class="btn btn-primary light action" type="submit" value="Sign In">' +
//       '\t</div>' +
//       '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
//       '</div>'
//   })
//   $('.register-button').click(function () {
//     registerModal()
//   })

//   // close modal on save
//   $('.action').click(function () {
//     var first_name = document.getElementById('f_name').value
//     var last_name = document.getElementById('l_name').value
//     var nick_name = document.getElementById('n_name').value
//     var day = document.getElementById('B-day').value
//     var month = document.getElementById('B-month').value
//     var year = document.getElementById('B-year').value
//     // console.log(first_name, last_name, nick_name, day, month, year)

//     // store to users storage
//     var ref = firebase.database().ref('/users')

//     var updateLoginDetails = {
//       first_name: first_name,
//       last_name: last_name,
//       nick_name: nick_name,
//       D_O_B: { day, month, year }
//     }
//     ref.update({
//       [auth_id]: updateLoginDetails
//     })
//     // users storage end

//     $('#login-modal').modal('hide')
//     $('.modal , .modal-backdrop').remove()
//     $('.blocker').css('display', 'none')
//     // console.log('closed #login-modal')
//   })
// }
// function registerModal() {
//   // console.log("reg");
//   $('.modal ').remove()
//   $.createDialog({
//     modalName: 'register-modal',
//     popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
//     actionButton: registerNextModal,
//     htmlContent:
//       '<div id="confirm_dialog" style="display: block;">' +
//       '\t<h2 id="confirm_title">Registration is soooo easy. We just need a few details to get started</h2>' +
//       date_form +
//       '\t<div id="confirm_actions">' +
//       '\t\t<button id="close_button" class="btn btn-primary light cancel" >Maybe later</button>' +
//       '\t\t<input id="action_button" class="btn btn-primary light action" type="submit" value="Sign up">' +
//       '\t</div>' +
//       '<p class="sign-up login-button">Already have an account ? <span class="a-link">Sign In</span></p>' +
//       '</div>'
//   })
//   $('.login-button').click(function () {
//     loginModal()
//   })
// }
function registerModal() {
  console.log("register modal click")
  $('.modal ').remove()
  $.createDialog({
    modalName: 'register-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    actionButton: registerNextModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Registration is soooo easy. We just need a few details to get started</h2>' +
      date_form +
      '\t<div id="confirm_actions">' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Sign up">' +
      '\t</div>' +
      '<p class="sign-up login-button">Already have an account ? <span class="a-link">Login</span></p>' +
      '</div>'
  })
  $('.login-button').click(function () {
    loginModal()
  })
}
function registerNextModal() {

  var first_name = document.getElementById('f_name').value
  var last_name = document.getElementById('l_name').value
  var nick_name = document.getElementById('n_name').value
  var day = document.getElementById('B-day').value
  var month = document.getElementById('B-month').value
  var year = document.getElementById('B-year').value
  // console.log(first_name, last_name, nick_name, day, month, year)

  // store to users storage
  // var ref = firebase.database().ref('/users')
  var ref = firebase.database().ref('/users/');
  firebase.database().ref('/users/'+auth_id).once('value').then(function (snapshot) {
      data = snapshot.val()
      console.log("data", data);
      var updateLoginDetails = data;
      if(data){
        var keys = Object.keys(data)
        updateLoginDetails['first_name'] = first_name
        updateLoginDetails['last_name'] = last_name
        updateLoginDetails['nick_name'] = nick_name
        updateLoginDetails['D_O_B'] = { day, month, year }
      }else{
        updateLoginDetails = {
          first_name: first_name,
          last_name: last_name,
          nick_name: nick_name,
          D_O_B: { day, month, year }
        }
      }
      console.log("updateLoginDetails", updateLoginDetails);
      ref.update({
        [auth_id]: updateLoginDetails
      })
      // return snapshot.val();
  })

  function successAnimation() {
    if (active_tab > 0) {
      editor.blur()
      $('.success-animation').removeClass('hide')
  
      setTimeout(() => {
        $('.success-animation').addClass('hide')
        // console.log('animate2')
        // editor.focus()
      }, 1100)
    }
    // console.log('animate1' + active_tab)
  }


if (first_name == '' ) {
  document.querySelector('#f_name', '').classList.add('error-border')
  return false
}
if(last_name == ''){
   document.querySelector('#l_name', '').classList.add('error-border')
  return false
}
if (nick_name == '') {
  document.querySelector('#n_name', '').classList.add('error-border')
  return false
} else {
  var register_addtional =
    '<div id="confirm_dialog" style="display: block;">' +
    '\t<h2 id="confirm_title">Congrats you have successfullly registered.</h2>' +
    '<p style="margin-bottom: 8px;">Please could you add in few more details about yourself so we can stay in touch.</p>' +
    '<form lpformnum="1" id="email-login"><div >\n' +
    '<input class="form-control" id="mobile_number" placeholder="Mobile number" name="mobile" type="text" required="" style="width: 100%">\n' +
    '<input class="form-control" id="email_id" placeholder="Email address" name="email" type="email" required="" style="width: 100%">\n' +
    '<input class="form-control" id="id_number" placeholder="ID number" name="id_number" type="text" required="" style="width: 100%">\n' +
    '<select name="country" id="eCountry" class="custom-select" style="width: 44%">\n' +
    '<option value="" selected>Country</option>\n' +
    '<option value="South Africa">South Africa</option>\n' +
    '<option value="Mozambique">Mozambique</option>\n' +
    '<option value="Nambia">Nambia</option>\n' +
    '<option value="Zambia">Zambia</option>\n' +
    '<option value="Zimbabwe">Zimbabwe</option>\n' +
    '</select>\n' +
    '<input class="form-control school" id="shcoolName" placeholder="Name of School" name="school" type="text" required="" style="width: 54%">\n' +
    '</div><div id="error"></div></form>\n' +
    '\t<div id="confirm_actions">' +
    '\t\t<button id="close_button" class="btn btn-encouraging light cancel" datax-dismiss="modal">Skip</button>' +
    '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Save">' +
    '\t</div>' +
    '</div>'
}

$('#register-modal .modal-body').html(register_addtional)

$('#close_button, .modal-backdrop').click(function () {
  $('#register-modal').modal('hide')
  $('.modal , .modal-backdrop').remove()
  $('.blocker').css('display', 'none')
  // console.log('closed #register-modal')
})
$('previewIcon').click(function () {
  $('.modal , .modal-backdrop').remove()
})

$('.action').click(function () {

  var mobile_number = document.getElementById('mobile_number').value
  var email_id = document.getElementById('email_id').value
  var id_number = document.getElementById('id_number').value
  var country = document.getElementById('eCountry').value
  var school = document.getElementById('shcoolName').value
  // console.log(first_name, last_name, nick_name, day, month, year)

  if (country == '') {
    document.querySelector('#eCountry').classList.add('error-border')
  }
  if (school == '') {
    document.querySelector('#shcoolName').classList.add('error-border')
  }
  else {
    document.querySelector('#eCountry').classList.remove('error-border');
     document.querySelector('#shcoolName').classList.remove('error-border');
    // store to users storage
  var ref = firebase.database().ref('/users/' + auth_id )

  var additionalDetails = {
    mobile_number: mobile_number,
    email_id: email_id,
    id_number: id_number,
    country: country,
    school_name: school
  }
  ref.update({
    details: additionalDetails
  }).then(function (snapshot){
    successProfileDetails('Signup Successfull', 'Welcome Aboard')
  // console.log(additionalDetails, 'Details')
  })
  }

})
}

// function registerNextModal() {
//   // console.log(
//   //   'add code here to save user and then proceed to collect addtional data'
//   // )
//   var first_name = document.getElementById('f_name').value
//   var last_name = document.getElementById('l_name').value
//   var nick_name = document.getElementById('n_name').value
//   // console.log(first_name, last_name, nick_name)

//   if (first_name == '' || last_name == '') {
//     document.querySelector('#f_name', '').classList.add('error-border')
//   }
//   if (nick_name == '') {
//     document.querySelector('#n_name', '').classList.add('error-border')
//   } else {
//     var register_addtional =
//       '<div id="confirm_dialog" style="display: block;">' +
//       '\t<h2 id="confirm_title">Congrats you have successfullly registered.</h2>' +
//       '<p style="margin-bottom: 8px;">Please could you add in few more details about yourself so we can stay in touch.</p>' +
//       '<form lpformnum="1" id="email-login"><div >\n' +
//       '<input class="form-control" placeholder="Mobile number" name="mobile" type="text" required="" style="width: 100%">\n' +
//       '<input class="form-control" placeholder="Email address" name="email" type="text" required="" style="width: 100%">\n' +
//       '<input class="form-control" placeholder="ID number" name="id_number" type="text" required="" style="width: 100%">\n' +
//       '<select name="country" class="custom-select" style="width: 44%">\n' +
//       '<option value="" selected>Country</option>\n' +
//       '<option value="ZA">South Africa</option>\n' +
//       '<option value="MZ">Mozambique</option>\n' +
//       '<option value="NM">Nambia</option>\n' +
//       '<option value="ZM">Zambia</option>\n' +
//       '<option value="ZI">Zimbabwe</option>\n' +
//       '</select>\n' +
//       '<input class="form-control school" placeholder="Name of School" name="school" type="text" required="" style="width: 54%">\n' +
//       '</div><div id="error"></div></form>\n' +
//       '\t<div id="confirm_actions">' +
//       '\t\t<button id="close_button" class="btn btn-primary light cancel" datax-dismiss="modal">Skip</button>' +
//       '\t\t<input id="action_button" class="btn btn-primary light action" type="submit" value="Save">' +
//       '\t</div>' +
//       '</div>'
//   }

//   $('#register-modal .modal-body').html(register_addtional)

//   $('#close_button, .modal-backdrop').click(function () {
//     $('#register-modal').modal('hide')
//     $('.modal , .modal-backdrop').remove()
//     $('.blocker').css('display', 'none')
//     // console.log('closed #register-modal')
//   })
//   $('previewIcon').click(function () {
//     $('.modal , .modal-backdrop').remove()
//   })

//   $('.action').click(function () {
//     $('#login-modal').modal('hide')
//     $('.modal , .modal-backdrop').remove()
//     $('.blocker').css('display', 'none')
//     // console.log('closed #registerNext-modal')
//   })
// }


function changeAvatar(avatar) {
  $('.modal-content')
    .removeClass(current_avatar)
    .addClass(avatar)
  $('.select_robot, .select_lego').toggleClass(
    'option_selected option_unselected'
  )
  current_avatar = avatar
  successAnimation()
}

// ***************** title popup in home page ************
function titleModal() {
  // console.log("reg");
  $('.modal ').remove()
  $.createDialog({
    modalName: 'title-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    actionButton: titleInput,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Awesome</h2>' +
      '<form titleForm="1" id="title-form"><div ><label>What do you want to call your project?<span class="required-mark">*</span></label><input class="form-control" id="project_name" placeholder="Super Code Stretch-man  v1" name="Pname" type="text" required=""><div><label style="    margin: 0;">Do you have any notes? (Optional)</label></div><textarea class="form-control" id="project_notes" placeholder="This project  reminds me of the pet spider I had in 2ndgrade, that.." name="nickname" type="text" cols="4"></textarea></div></form>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-primary light action projectTitle" type="submit" value="Save">' +
      '\t</div>' +
      '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })
  // $('#action_button').click(function () {
  //   savedToGallery()
  // })
}
// ************** title popup end ==============

// ============= unRegistered Model =========
// function UnRegisteredModal() {
//   // console.log("reg");
//   $('.modal ').remove()
//   $.createDialog({
//     modalName: 'unRegistered-modal',
//     popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad',
//     actionButton: loginModal,
//     htmlContent:
//       '<div id="confirm_dialog" style="display: block;">' +
//       '\t<h2 id="confirm_title">OOOOPS!</h2>' +
//       '<label>This feature is only available for logged in users.</label>\n\n\n' +
//       '<h4>Login Now</h4>\n' +
//       '\t<div id="confirm_actions">' +
//       '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
//       '\t\t<input id="action_button" class="btn btn-primary light action " type="submit" value="Login">' +
//       '\t</div>' +
//       '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
//       '</div>'
//   })
//   $('.register-button').click(function () {
//     registerModal()
//   })
// }
// ============== unRegistered Model end =========

// ============= saved your code Model =========
function savedToGallery() {
  $('.modal ').remove()
  $.createDialog({
    modalName: 'projectSaved-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Yeahhhh!</h2>' +
      '<label>Your Project is Saved...</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}
// ***********  save user login details to local storage
function saveLogin() {
  // console.log('logindetails//////')
  var first_name = document.getElementById('f_name').value
  var last_name = document.getElementById('l_name').value
  var nick_name = document.getElementById('n_name').value
  var day = document.getElementById('B-day').value
  var month = document.getElementById('B-month').value
  var year = document.getElementById('B-year').value
  // console.log(first_name, last_name, nick_name, day, month, year)
}



// ***************** submit offline code to the firebase
function selectProjectName() {
  var project_name = $(".project-name :selected").val();
  if (project_name === "project1") {
    $("#submit-project").attr("onclick", "validateProject1()");
  }
  else if (project_name === "project2") {
    $("#submit-project").attr("onclick", "validateProject1()");
  }
  else if (project_name === "project3") {
    $("#submit-project").attr("onclick", "validateProject3()");
  }
  else {
    $("#submit-project").attr("onclick", "validateProject4()");
  }
}
function validateProject4() {
  alert("Project 4 not ready");
}
function validateProject3() {
  alert("Project 3 not ready");
}
function validateProject2() {
  alert("Project 2 not ready");
}


function validateProject1() {
  var check_points_for_training1 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>',
    '(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>'
  ];
  var check_points_for_training2 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>',
    '(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>'
  ];
  var check_points_for_training3 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>',
    '(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>'
  ];
  var check_points_for_training4 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>',
    '(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>'
  ];

  
  var marks_for_training1 = validateHtmlElement( check_points_for_training1 );
  var marks_for_training2 = validateHtmlElement( check_points_for_training2 );
  var marks_for_training3 = validateHtmlElement( check_points_for_training3 );
  var marks_for_training4 = validateHtmlElement( check_points_for_training4 );

  marks_for_training1.project_name = "P1 Training1";
  marks_for_training2.project_name = "P1 Training2";
  marks_for_training3.project_name = "P1 Training3";
  marks_for_training4.project_name = "P1 Training4";

  var overrollMarks = {
    p1_training1:  marks_for_training1,
    p1_training2:  marks_for_training2,
    p1_training3:  marks_for_training3,
    p1_training4:  marks_for_training4 
  }

  console.log(overrollMarks);
}


function validateHtmlElement(checkPoints) {
  if ( ! $.isArray(checkPoints)) {
     throw "needs a array parameter";
  }
  var total_marks = 0;
  var inputCode = $("#offline-code").val();
  var validationPass = false;

  /***** get total marks *****/
  for (var i = 0; i < checkPoints.length; i++) {
    var reResults = new RegExp(checkPoints[i], "i").test(inputCode);
    if (reResults) {
      total_marks++;
    }
  }
  var total_marks_in_percent = (total_marks / checkPoints.length) * 100;

  /***** get tags that have errors or are invalid*****/
  var invalid_tags = [];
  for (var i = 0; i < checkPoints.length; i++) {
    var challengeNumber = i + 1;
    var reResults = new RegExp(checkPoints[i], "i").test(inputCode);
    if (reResults) {
      if ((i + 1) == checkPoints.length) {
        validationPass = true;
      }
    }
    else {
      var openningTag = checkPoints[i].split('|')[0];
      openningTag = openningTag.replace('(', '');

      var results = new RegExp(openningTag).test(inputCode);
      if (results) {
        invalid_tags.push("C" + challengeNumber + ". There seems to be no " + openningTag + " closing tag");
      }
      else {
        invalid_tags.push("C" + challengeNumber + ". There seems to be an error with " + openningTag + " tag or is messsing.");
      }
    }
  }
  var score_for_trainning = total_marks_in_percent ;
  var comments_on_score = total_marks + " of " + checkPoints.length + " key validators triggered."; // Correct (3 of 5 key validators triggered.)
  
  return {
    comments_on_invalid_tags: invalid_tags,
    comments_on_score: comments_on_score,
    total_marks:total_marks,
    total_marks_in_percent: total_marks_in_percent
  }
}



function saveProject(results) {
  var projectName = $(".project-name :selected").val();
  var projectCode = $(".project-code").val();
  if (projectName !== "" && projectCode !== "") {

    initFirebase();

    firebase.database().ref('/offline_version_submitted_code/' + auth_id).update(
      { project: results }
    );

    $(".offline-submition-page").fadeOut();
    $(".thank-you-page").fadeIn();
  }
}

 //++++++++++++++++++++++++ menu popup ++++++++++++++++++++++++\\
 var crossmenu=document.getElementById("#cross-menu");
 //menu popup function
 document.getElementById("#popmenu").addEventListener("click",function() {
   document.querySelector('.menu-bg').style.display="flex";
   document.querySelector('.menu-content').style.display="block";
   

 });

 crossmenu.addEventListener("click",function () {
   document.querySelector('.menu-content').style.display="none";
   document.querySelector('.menu-bg').style.display="none";
   
 });
 
 //++++++++++++++++++++++EnD+++++++++++++++++++++++++++++++++++\\

 