$(function() {
		   
	function reloadCaptcha(){ 
		$("#captchax").attr("src","php/captcha/captcha.php?r=" + Math.random()); 
	}
	
	$('.captcode').click(function(e){
		e.preventDefault();
		reloadCaptcha();
	});	
	
	function swapButton(){
		var txtswap = $(".frm-footer button[type='submit']");
		if (txtswap.text() == txtswap.data("btntext-sending")) {
			txtswap.text(txtswap.data("btntext-original"));
		} else {
			txtswap.data("btntext-original", txtswap.text());
			txtswap.text(txtswap.data("btntext-sending"));
		}
	}
	
	$("#floraforms").validate({
			errorClass: "state-error",
			validClass: "state-success",
			errorElement: "em",
			rules: {
				firstname: {
						required: true,
						minlength: 2
				},
				lastname: {
						required: true
				},				
				emailaddress: {
						required: true,
						email: true
				},
				telephone: {
						required: true
				},			
				captcha:{
					required:true,
					remote:'php/captcha/process.php'
				}				
			},
			messages:{
				firstname: {
						required: 'Enter first name',
						minlength: 'Enter at least 2 characters'
				},
				lastname: {
						required: 'Enter last name'
				},				
				emailaddress: {
						required: 'Enter email address',
						email: 'Enter a VALID email address'
				},
				telephone: {
						required: 'Enter telephone number'
				},				
				captcha:{
						required: 'You must enter the captcha code',
						remote:'Captcha code is incorrect'
				}				
			},
			highlight: function(element, errorClass, validClass) {
					$(element).closest('.field').addClass(errorClass).removeClass(validClass);
			},
			unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.field').removeClass(errorClass).addClass(validClass);
			},
			errorPlacement: function(error, element) {
			   if (element.is(":radio") || element.is(":checkbox")) {
						element.closest('.option-group').after(error);
			   } else {
						error.insertAfter(element.parent());
			   }
			},	
			submitHandler:function(form) {
				$(form).ajaxSubmit({
						target:'.response',			   
						beforeSubmit:function(){
							swapButton();
							$('.frm-footer').addClass('elemprogress');
						},
						error:function(){
							swapButton();
							$('.frm-footer').removeClass('elemprogress');
						},
						 success:function(){
								swapButton();
								$('.frm-footer').removeClass('elemprogress');
								$('.alert-success').show().delay(10000).fadeOut();
								$('.field').removeClass("state-error, state-success");
								if( $('.alert-error').length == 0){
									$('#floraforms').resetForm();
									reloadCaptcha();
								}
						 }
				  });
			}		
	});					

});	